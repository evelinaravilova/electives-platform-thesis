from rest_framework import serializers
from rest_framework.generics import get_object_or_404

from auth_module.serializers import UserSerializer
from institute_module.models import Group, StudentProfile, TeacherProfile, DeaneryProfile, RatingType, Institute, \
    StudentRating, Program
from request_module.tasks import send_student_rating, prepare_send_rating_data


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = '__all__'


class StudentProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    group = GroupSerializer()

    class Meta:
        model = StudentProfile
        fields = ['user', 'group', 'email_extra']


class TeacherProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = TeacherProfile
        fields = ['user', 'institute']
        depth = 2


class DeaneryProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = DeaneryProfile
        fields = '__all__'
        depth = 2


class RatingTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = RatingType
        fields = '__all__'


class InstituteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Institute
        fields = '__all__'


class ProgramSerializer(serializers.ModelSerializer):
    class Meta:
        model = Program
        fields = '__all__'


class StudentRatingSerializer(serializers.ModelSerializer):
    student = serializers.IntegerField(source='student.user.id')

    def create(self, validated_data):
        user_id = validated_data['student']['user']['id']
        rating_type = validated_data['rating_type']
        score = validated_data['score']

        student = get_object_or_404(StudentProfile, user_id=user_id)
        obj, created = StudentRating.objects.update_or_create(
            student=student,
            rating_type=rating_type,
            defaults={
                'score': score
            })
        print(obj, created)
        send_student_rating.delay(*prepare_send_rating_data(obj, self.context['wave_id'], self.context['course_id']))
        return obj

    class Meta:
        model = StudentRating
        fields = ('student', 'rating_type', 'score')
