from django.contrib import admin
from .models import *


class StudentProfileAdmin(admin.ModelAdmin):
    search_fields = ['user__last_name', 'user__first_name', 'user__middle_name']


admin.site.register(Group)
admin.site.register(Institute)
admin.site.register(Program)
admin.site.register(StudentProfile, StudentProfileAdmin)
admin.site.register(TeacherProfile)
admin.site.register(DeaneryProfile)
admin.site.register(RatingType)
admin.site.register(StudentRating)
