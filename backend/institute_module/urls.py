from rest_framework import routers

from .views import ProfileViewSet, InstituteViewSet, StudentRatingViewSet, ProgramViewSet

router = routers.DefaultRouter()
router.register(r'profiles', ProfileViewSet, basename="profiles")
router.register(r'institutes', InstituteViewSet, basename="institutes")
router.register(r'programs', ProgramViewSet, basename="programs")
router.register(r'student_ratings', StudentRatingViewSet, basename="student_ratings")


urlpatterns = [

]
urlpatterns += router.urls
