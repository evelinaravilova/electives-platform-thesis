from django.apps import AppConfig


class InstituteModuleConfig(AppConfig):
    name = 'institute_module'
