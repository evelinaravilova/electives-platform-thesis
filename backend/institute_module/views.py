from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, status
from rest_framework.response import Response

from auth_module.permissions import IsDeanery
from electives_platform.enums import STUDENT, TEACHER, DEANERY
from electives_platform.utils import ElectivesAPIException, method_permission_classes
from institute_module.models import Institute, StudentRating, DeaneryProfile, Program
from institute_module.serializers import StudentProfileSerializer, TeacherProfileSerializer, DeaneryProfileSerializer, \
    InstituteSerializer, StudentRatingSerializer, ProgramSerializer


class ProfileViewSet(viewsets.ReadOnlyModelViewSet):

    def get_queryset(self):
        serializer_model = self.get_serializer_class().Meta.model
        return serializer_model.objects.all()

    def get_serializer_class(self):
        role = self.request.query_params.get('role')
        if role == STUDENT:
            return StudentProfileSerializer
        elif role == TEACHER:
            return TeacherProfileSerializer
        elif role == DEANERY:
            return DeaneryProfileSerializer
        return StudentProfileSerializer


class InstituteViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Institute.objects.all()
    serializer_class = InstituteSerializer


class ProgramViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Program.objects.all()
    serializer_class = ProgramSerializer

    @method_permission_classes((IsDeanery,))
    def list(self, request, **kwargs):
        deanery = get_object_or_404(DeaneryProfile, user=request.user)
        programs = self.queryset.filter(institute=deanery.institute)
        serializer = self.serializer_class(programs, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class StudentRatingViewSet(viewsets.ModelViewSet):
    queryset = StudentRating.objects.all()
    serializer_class = StudentRatingSerializer

    def create(self, request, **kwargs):
        user = request.user
        ratings = request.data['ratings']
        wave_id = request.data['wave_id']
        course_id = request.data['course_id']
        print(ratings)

        serializer = self.serializer_class(data=ratings, context={'wave_id': wave_id, 'course_id': course_id}, many=True)
        try:
            serializer.is_valid(raise_exception=True)
        except Http404:
            raise ElectivesAPIException(
                detail="Неверные данные",
                status_code=status.HTTP_400_BAD_REQUEST,
            )
        serializer.save()
        return Response(data={}, status=status.HTTP_200_OK)
