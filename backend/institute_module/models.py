from django.db import models

from auth_module.models import User
from electives_platform import settings
from electives_platform.enums import DEGREES, RATING_MODE


class Institute(models.Model):
    name = models.CharField(max_length=100, unique=True, verbose_name="Название")
    short_name = models.CharField(max_length=30, null=True, verbose_name="Короткое название")
    key_number = models.SmallIntegerField(default=100, null=True, blank=True, verbose_name="Код института")

    class Meta:
        verbose_name_plural = "Институты"
        verbose_name = "Институт"

    def __str__(self):
        return "{}".format(self.name)


class Program(models.Model):
    name = models.CharField(max_length=100, unique=True, verbose_name="Название направления")
    institute = models.ForeignKey(Institute, on_delete=models.SET_NULL, null=True, verbose_name="Институт")

    class Meta:
        verbose_name_plural = "Направления обучения"
        verbose_name = "Направление обучения"

    def __str__(self):
        return "{}".format(self.name)


class Group(models.Model):
    group_number = models.CharField(max_length=15, unique=True, verbose_name="Номер группы")
    start_year = models.SmallIntegerField(default=2000)
    end_year = models.SmallIntegerField(default=2000)
    current_course = models.SmallIntegerField(default=1, verbose_name="Номер курса")
    program = models.ForeignKey(Program, on_delete=models.SET_NULL, null=True, verbose_name="Направление обучения")
    degree = models.CharField(max_length=8, choices=DEGREES, default='bachelor', verbose_name="Степень")

    def __str__(self):
        return "{}".format(self.group_number)

    class Meta:
        verbose_name_plural = "Студенческие группы"
        verbose_name = "Студенческая группа"


class StudentProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE,
                                unique=True)
    group = models.ForeignKey(Group, on_delete=models.SET_NULL, null=True,
                              verbose_name="Группа")
    ratings = models.ManyToManyField(through='institute_module.StudentRating', to='institute_module.RatingType')
    email_extra = models.EmailField(blank=True, null=True)

    def __str__(self):
        if self.group:
            return f"{str(self.user)} - {self.group}"
        return f"{str(self.user)}"

    class Meta:
        verbose_name_plural = "Профили студентов"
        verbose_name = "Профиль студента"


class TeacherProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE,
                                unique=True)
    institute = models.ForeignKey(Institute, on_delete=models.SET_NULL, null=True, verbose_name="Институт")
    position = models.CharField(max_length=1000, blank=True, null=True, verbose_name="Должность")
    academic_title = models.CharField(max_length=1000, blank=True, null=True, verbose_name="Академические звания")
    office = models.CharField(max_length=200, blank=True, null=True, verbose_name="Рабочий адрес")

    class Meta:
        verbose_name_plural = "Профили преподавателей"
        verbose_name = "Профиль преподавателя"

    def __str__(self):
        if self.institute:
            return f"{str(self.user)} - {self.institute}"
        return f"{str(self.user)}"


class DeaneryProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE,
                                unique=True)
    institute = models.ForeignKey(Institute, on_delete=models.SET_NULL, null=True, verbose_name="Институт")

    class Meta:
        verbose_name_plural = "Профили сотрудников деканата"
        verbose_name = "Профиль сотрудника деканата"

    def __str__(self):
        if self.institute:
            return f"{str(self.user)} - {self.institute}"
        return f"{str(self.user)}"


class RatingType(models.Model):
    key_name = models.CharField(max_length=100, unique=True, blank=True, verbose_name="Название ключа")
    mode = models.CharField(max_length=10, choices=RATING_MODE, default='score', verbose_name="Вид рейтинга")

    class Meta:
        verbose_name_plural = "Типы рейтинга"
        verbose_name = "Тип рейтинга"

    def __str__(self):
        return "{}".format(self.key_name)


class StudentRating(models.Model):
    score = models.FloatField(default=0.0, verbose_name="Значение")
    rating_type = models.ForeignKey(RatingType, on_delete=models.SET_NULL, null=True, verbose_name="Тип рейтинга")
    student = models.ForeignKey(StudentProfile, on_delete=models.CASCADE, verbose_name="Студент")

    class Meta:
        verbose_name_plural = "Рейтинги студента"
        verbose_name = "Рейтинг студента"

    def __str__(self):
        return f"{str(self.student)} {str(self.rating_type)}"
