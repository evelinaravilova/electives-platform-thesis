from django.apps import AppConfig


class CourseModuleConfig(AppConfig):
    name = 'course_module'
