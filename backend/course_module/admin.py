from django.contrib import admin

from .models import *

admin.site.register(CourseType)
admin.site.register(CourseBlock)
admin.site.register(CourseTag)
admin.site.register(YoutubeVideo)
admin.site.register(Course)
