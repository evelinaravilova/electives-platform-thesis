from rest_framework import serializers

from course_module.models import CourseBlock, Course, CourseTag
from institute_module.models import StudentProfile
from institute_module.serializers import TeacherProfileSerializer, RatingTypeSerializer
from request_module.models import CourseRequest
from request_module.serializers import CourseRequestSerializer, CourseRequestResultSerializer


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseTag
        fields = '__all__'


class CourseSerializer(serializers.ModelSerializer):
    head = TeacherProfileSerializer(many=True, required=False, allow_null=True)
    rating_type = RatingTypeSerializer(required=False, allow_null=True)
    course_tag = TagSerializer(many=True, required=False, allow_null=True)

    class Meta:
        model = Course
        exclude = ('program', 'block')


class CourseStudentSerializer(serializers.ModelSerializer):
    head = TeacherProfileSerializer(many=True, required=False, allow_null=True)
    rating_type = RatingTypeSerializer(required=False, allow_null=True)
    course_tag = TagSerializer(many=True, required=False, allow_null=True)
    request = serializers.SerializerMethodField(required=False)
    requests_count = serializers.SerializerMethodField(required=False, allow_null=True)
    youtube_videos = serializers.SlugRelatedField(
        source="youtubevideo_set",
        many=True,
        read_only=True,
        slug_field='youtube_id',
    )

    def get_request(self, obj):
        student = StudentProfile.objects.get(user=self.context['user'])
        try:
            request = CourseRequest.objects.get(course=obj, student=student, wave=self.context['wave'])
        except CourseRequest.DoesNotExist:
            request = None
        if request:
            return CourseRequestSerializer(request).data
        return None

    def get_requests_count(self, obj):
        wave = self.context['wave']
        if wave.show_requests_count:
            return CourseRequest.objects.filter(course=obj, wave=wave).count()

    class Meta:
        model = Course
        exclude = ('program', 'block', 'active',)


class CourseBlockSerializer(serializers.ModelSerializer):
    type = serializers.CharField(source='type.key_name')

    class Meta:
        model = CourseBlock
        fields = '__all__'


class CourseBlockStudentSerializer(serializers.ModelSerializer):
    type = serializers.CharField(source='type.key_name')
    requests_count = serializers.SerializerMethodField(required=False, allow_null=True)

    def get_requests_count(self, obj):
        return CourseRequest.objects.filter(course__block=obj, wave=self.context['wave'], student__user=self.context['user']).count()

    class Meta:
        model = CourseBlock
        fields = '__all__'


class CourseWaveSerializer(serializers.ModelSerializer):
    rating_type = RatingTypeSerializer(required=False, allow_null=True)
    block = CourseBlockSerializer()
    requests_count = serializers.SerializerMethodField(required=False, allow_null=True)

    def get_requests_count(self, obj):
        return CourseRequest.objects.filter(course=obj, wave=self.context['wave']).count()

    class Meta:
        model = Course
        fields = ('id', 'name', 'block', 'rating_type', 'students_count', 'requests_count')


class CourseBlockRequestsSerializer(serializers.ModelSerializer):
    type = serializers.CharField(source='type.key_name')
    requests = serializers.SerializerMethodField()

    def get_requests(self, obj):
        requests = CourseRequest.objects.filter(course__block=obj, wave=self.context['wave'], student=self.context['student'])
        return CourseRequestResultSerializer(requests, many=True).data

    class Meta:
        model = CourseBlock
        fields = '__all__'

