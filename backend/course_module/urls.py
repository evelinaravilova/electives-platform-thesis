from rest_framework import routers

from .views import CourseBlockViewSet, CourseViewSet

router = routers.DefaultRouter()
router.register(r'course_blocks', CourseBlockViewSet, basename="course_blocks")
router.register(r'courses', CourseViewSet, basename="courses")

urlpatterns = [
]

urlpatterns += router.urls
