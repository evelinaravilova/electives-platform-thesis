from rest_framework.response import Response
from rest_framework import status
from rest_framework.viewsets import ModelViewSet

from auth_module.utils import is_student
from course_module.models import CourseBlock, Course
from course_module.serializers import CourseBlockSerializer, CourseSerializer, CourseStudentSerializer, CourseBlockStudentSerializer
from wave_module.fsm import WaveNumberState, WaveContext
from wave_module.student_queries import get_student_current_wave


class CourseBlockViewSet(ModelViewSet):
    queryset = CourseBlock.objects.all()
    serializer_class = CourseBlockSerializer

    def get_queryset(self):
        user = self.request.user
        if is_student(user):
            current_wave = get_student_current_wave(user)
            return current_wave.wave_bunch.course_blocks

    def list(self, request, **kwargs):
        user = request.user
        if is_student(user):
            queryset = self.get_queryset()
            context = {'user': user}
            current_wave = get_student_current_wave(request.user)
            if current_wave:
                context['wave'] = current_wave
            serializer = CourseBlockStudentSerializer(queryset, context=context, many=True)
            return Response(data=serializer.data, status=status.HTTP_200_OK)

        queryset = self.queryset.filter()
        serializer = self.serializer_class(queryset, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class CourseViewSet(ModelViewSet):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer

    def get_serializer_class(self):
        if is_student(self.request.user):
            return CourseStudentSerializer
        return self.serializer_class

    def list(self, request, **kwargs):
        user = request.user
        block_id = request.query_params.get('block_id')
        context = {'user': user}
        current_wave = get_student_current_wave(user)
        data = {}

        if current_wave:
            context['wave'] = current_wave
            # show_courses_context = ShowCoursesContext(get_show_courses_strategy(current_wave))
            # course_list = show_courses_context.get_courses_to_show(current_wave, user, block_id)

            course_list = Course.objects.filter(block_id=block_id)

            if current_wave:
                wave_context = WaveContext(WaveNumberState())
                wave_context.request_course_script(course_list, current_wave, user, block_id)
                if wave_context.obj:
                    course_list = wave_context.obj
            serializer = self.get_serializer_class()(course_list, context=context, many=True)
            data = serializer.data
        return Response(data=data, status=status.HTTP_200_OK)

    # def create(self, request):
    #     pass

    # def update(self, request, pk=None):
    #     pass
    #
    # def partial_update(self, request, pk=None):
    #     pass
    #
    # def destroy(self, request, pk=None):
    #     pass
