from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

from electives_platform.enums import DEGREES, TAG_TYPE
from institute_module.models import TeacherProfile, Program, RatingType


class CourseType(models.Model):
    key_name = models.CharField(max_length=100, unique=True, verbose_name="Название ключа")

    class Meta:
        verbose_name_plural = "Типы курса"
        verbose_name = "Тип курса"

    def __str__(self):
        return f"{self.key_name} курс"


class CourseBlock(models.Model):
    semester = models.SmallIntegerField(default=1, verbose_name="Семестр",
                                        validators=[MinValueValidator(1), MaxValueValidator(8)])
    degree = models.CharField(max_length=8, choices=DEGREES, default='bachelor', verbose_name="Степень")
    type = models.ForeignKey(CourseType, on_delete=models.SET_NULL, verbose_name='Тип', null=True)

    class Meta:
        verbose_name_plural = "Блоки курсов"
        verbose_name = "Блок курсов"

    def __str__(self):
        return f"{self.type.key_name} блок {self.semester} сем."


class CourseTag(models.Model):
    name = models.CharField(max_length=25, unique=True, verbose_name="Тег")
    type = models.CharField(max_length=5, choices=TAG_TYPE, default='skill', verbose_name="Тип тега")

    class Meta:
        verbose_name = "Тег"
        verbose_name_plural = "Теги"

    def __str__(self):
        return f"{self.type} - {self.name}"


class Course(models.Model):
    name = models.CharField(max_length=150, unique=True, verbose_name="Название курса")
    requirements = models.TextField(blank=True, verbose_name="Требования")
    description = models.TextField(blank=True, verbose_name="Описание")
    logo = models.URLField(max_length=100, blank=True, verbose_name="Логотип")
    head = models.ManyToManyField(TeacherProfile, blank=True, verbose_name="Преподаватель курса")
    rating_type = models.ForeignKey(RatingType, on_delete=models.SET_NULL, null=True, verbose_name="Тип рейтинга")
    program = models.ForeignKey(Program, on_delete=models.SET_NULL, null=True, verbose_name="Направление")
    block = models.ForeignKey(CourseBlock, on_delete=models.SET_NULL, null=True, verbose_name="Блок")
    students_count = models.SmallIntegerField(null=True, blank=True, verbose_name="Число студентов на запись")
    active = models.BooleanField(default=True, verbose_name="Активный")
    course_tag = models.ManyToManyField(CourseTag, blank=True, verbose_name="Теги курса")

    class Meta:
        verbose_name = "Курс по выбору"
        verbose_name_plural = "Курсы по выбору"

    def __str__(self):
        return self.name


class YoutubeVideo(models.Model):
    youtube_id = models.CharField(max_length=50, unique=True, verbose_name="ID видео на Youtube")
    course = models.ForeignKey(Course, on_delete=models.CASCADE, null=True, verbose_name="Курс")

    class Meta:
        verbose_name = "Видео на Youtube"
        verbose_name_plural = "Видео на Youtube"

    def __str__(self):
        return f"{str(self.course)} - {self.youtube_id}"
