from django.urls import path
from rest_framework import routers

from auth_module.views import AuthView

router = routers.DefaultRouter()
urlpatterns = [
    path("get_token", AuthView.as_view(), name="auth_view"),
]
urlpatterns += router.urls
