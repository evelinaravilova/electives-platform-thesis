from django.db import models
from django.contrib.auth.models import AbstractUser

from electives_platform.enums import ROLES


class User(AbstractUser):
    middle_name = models.CharField(max_length=20, null=True, blank=True, verbose_name="Отчество")
    role = models.CharField(max_length=10, choices=ROLES, default='student', verbose_name='Роль')
    photo = models.URLField(blank=True, null=True)

    def __str__(self):
        return "{} {} {}".format(self.last_name, self.first_name, self.middle_name)

    class Meta:
        verbose_name_plural = "Пользователи"
        verbose_name = "Пользователь"

    @property
    def full_name(self):
        return str(self)

