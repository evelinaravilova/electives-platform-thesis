from rest_framework import serializers

from .models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'middle_name', 'last_name', 'photo', 'email', 'role']
        depth = 1


class AuthSerializer(serializers.Serializer):
    token = serializers.CharField()
    user = UserSerializer()

    class Meta:
        fields = ['token', 'user']
