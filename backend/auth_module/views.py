# coding=utf-8
import requests
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token

from .models import User
from .serializers import UserSerializer, AuthSerializer
from institute_module.models import Institute, Group, Program, StudentProfile, TeacherProfile
import os


class AuthView(APIView):
    permission_classes = ()

    def post(self, request):
        # код авторизации, используемый для запроса на токен доступа на платформу хостинга и поддержки проектов
        auth_code = request.data.get('code')
        print(auth_code)
        # получение токена
        access_token = fetch_access_token(auth_code)
        print(access_token)
        # получение информации о пользователе КФУ
        user_data = get_kpfu_user(access_token)
        print(user_data)
        # создание пользователя в базе данных, если его нет, или редактирование
        user = get_or_create_user(request, user_data)
        # добавление токена доступа в базу данных
        create_token_db(user, access_token)
        # создание профиля в базе данных, если его нет
        create_profile(user, user_data)

        response_data = AuthSerializer({'token': access_token, 'user': user}).data

        return Response(response_data, status=status.HTTP_200_OK)


def fetch_access_token(auth_code):
    print(os.getenv("FRONTEND_URL", "."))
    response = requests.post('https://core.uenv.ru/oauth/token/',
                             data={
                                 "client_id": os.getenv("CLIENT_ID"),
                                 "client_secret": os.getenv("CLIENT_SECRET"),
                                 "grant_type": "authorization_code",
                                 "code": auth_code,
                                 "redirect_uri": os.getenv("FRONTEND_URL", ".") + "/callback"
                             },
                             headers={"Content-Type": "application/x-www-form-urlencoded"})
    print(response.json())
    return response.json()["access_token"]


def get_kpfu_user(access_token):
    return requests.get('https://core.uenv.ru/api/v1.0/profile/',
                        headers={"Authorization": "Bearer " + access_token}).json()


def get_or_create_user(request, response):
    middle_name = response["middle_name"]
    username = response["email"].split("@")[0]
    if middle_name:
        try:
            middle_name, username = middle_name.split(" ")
        except ValueError:
            pass
    user, created = User.objects.update_or_create(
        email=response["email"],
        defaults={
            'username': username,
            'email': response["email"],
            'first_name': response["first_name"],
            'last_name': response["last_name"],
            'middle_name': middle_name,
            'photo': response["photo_url"]
        }
    )
    if created:
        user.set_password("123")
        user.role = response["role"]
        user.save()

    return user


def create_token_db(user, access_token):
    Token.objects.filter(user=user).delete()
    Token.objects.create(user=user, key=access_token)


def create_profile(user, response):
    institute, created = Institute.objects.get_or_create(name=response['institute']['title'])
    program = Program.objects.filter(institute=institute).first()
    print(institute)
    print(program)
    if user.role == 'student':
        user_group = response['student_group']
        degree = 'bachelor' if user_group['end_year'] - user_group['start_year'] >= 4 else 'magister'
        student_group, created = Group.objects.get_or_create(
            group_number=user_group['title'],
            defaults=dict(
                program=program,
                start_year=user_group['start_year'],
                end_year=user_group['end_year'],
                current_course=user_group['current_course'],
                degree=degree
            )
        )
        if not created:
            student_group.current_course = user_group['current_course']
            student_group.save()
        student, created = StudentProfile.objects.get_or_create(user_id=user.id,
                                                                defaults=dict(user=user, group=student_group))
        print(student)
    elif user.role == 'teacher':
        teacher, created = TeacherProfile.objects.get_or_create(user_id=user.id, defaults=dict(
            user=user,
            institute=institute
        ))
        print(teacher)


def get_academic_performance(access_token):
    return requests.get('https://core.uenv.ru/api/v1.0/kpfu_stud_api_gateway/academic_performance/',
                        headers={"Authorization": "Bearer " + access_token}).json()


# @api_view(('GET',))
# @renderer_classes((JSONRenderer,))
# def get_admin_key(request):
#     # получение сотрудника
#     user_id = request.GET.get('id', None)
#     user = User.objects.get(id=user_id)
#     # генерация ключа
#     new_key = random_string()
#     # создание ключа в базе данных
#     key = AdminPanelKey.objects.create(user=user, access_key=new_key)
#     return Response({"key": key.access_key}, status=status.HTTP_200_OK)
#
#
# def redirect_admin(request):
#     # получение параметров запроса
#     user_id = request.GET.get('id', None)
#     key = request.GET.get('key', None)
#     # получение объектов из базы данных. Если ключ не найден, вернуть 404
#     user = User.objects.get(id=user_id)
#     key = get_object_or_404(AdminPanelKey, user=user, access_key=key)
#     # если прошло меньше 30 секунд, авторизовать на сервере и удалить ключ
#     if time.time() - key.created_at.timestamp() < 30:
#         key.delete()
#         login(request, user)
#     return redirect('/admin/')
#
#
# def random_string():
#     return ''.join([random.choice(string.ascii_letters + string.digits) for n in range(20)])
