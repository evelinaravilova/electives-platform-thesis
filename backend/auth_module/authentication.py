from datetime import datetime, timedelta
import pytz
from rest_framework import exceptions
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token


class ExpiringTokenAuthentication(TokenAuthentication):
    def authenticate_credentials(self, key):
        try:
            token = Token.objects.get(key=key)
        except Token.DoesNotExist:
            raise exceptions.AuthenticationFailed('Неверный токен')

        if not token.user.is_active:
            raise exceptions.AuthenticationFailed('Пользователь не активен')

        # This is required for the time comparison
        utc_now = datetime.utcnow()
        utc_now = utc_now.replace(tzinfo=pytz.utc)

        if token.created < utc_now - timedelta(hours=10):
            raise exceptions.AuthenticationFailed('Ваша сессия истекла, войдите снова')

        return token.user, token
