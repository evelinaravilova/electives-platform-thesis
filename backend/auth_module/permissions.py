from rest_framework import permissions

from auth_module.utils import is_student, is_teacher, is_deanery


class IsStudent(permissions.BasePermission):

    def has_permission(self, request, view):
        return is_student(request.user)


class IsTeacher(permissions.BasePermission):

    def has_permission(self, request, view):
        return is_teacher(request.user)


class IsDeanery(permissions.BasePermission):

    def has_permission(self, request, view):
        return is_deanery(request.user)

