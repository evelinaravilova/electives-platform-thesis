from electives_platform.enums import STUDENT, TEACHER, DEANERY


def is_student(user):
    return user.role == STUDENT


def is_teacher(user):
    return user.role == TEACHER


def is_deanery(user):
    return user.role == DEANERY
