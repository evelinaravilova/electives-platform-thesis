from django.utils import timezone
from electives_platform.enums import DEGREES, WAVE_BUNCH_STATUS, REQUEST_STATUS


def get_degree_name(degree):
    return next((y for x, y in DEGREES if x == degree), None)


def get_status_name(status):
    return next((y for x, y in REQUEST_STATUS if x == status), None)


def get_wave_bunch_status(wave):
    now = timezone.now()
    if wave.start_date and now < wave.start_date:
        return WAVE_BUNCH_STATUS[0][0]
    elif wave.start_date and wave.end_date and wave.start_date <= now < wave.end_date:
        return WAVE_BUNCH_STATUS[1][0]
    elif wave.end_date and now > wave.end_date:
        return WAVE_BUNCH_STATUS[2][0]
    else:
        return WAVE_BUNCH_STATUS[0][0]


def get_wave_status_for_teacher(wave):
    now = timezone.now()
    if wave.auto_distribution_date:
        if wave.end_date and wave.end_date < now and wave.auto_distribution_date < now:
            return WAVE_BUNCH_STATUS[2][0]
        elif wave.end_date and wave.end_date < now < wave.auto_distribution_date:
            return WAVE_BUNCH_STATUS[1][0]
        elif wave.end_date and wave.end_date > now:
            return WAVE_BUNCH_STATUS[0][0]
        else:
            return WAVE_BUNCH_STATUS[0][0]
    else:
        if wave.end_date and wave.end_date < now:
            return WAVE_BUNCH_STATUS[2][0]
        else:
            return WAVE_BUNCH_STATUS[0][0]


def is_wave_opened_for_student(wave):
    now = timezone.now()
    return wave.end_date > now > wave.start_date

