from django.utils import timezone
from rest_framework import serializers

from course_module.models import Course
from course_module.serializers import CourseWaveSerializer, CourseBlockSerializer, CourseBlockRequestsSerializer
from electives_platform.enums import WAVE_BUNCH_STATUS
from institute_module.serializers import InstituteSerializer, ProgramSerializer
from google_sheets.requests_spreadsheets import create_spreadsheet
from wave_module.utils import get_wave_status_for_teacher, get_wave_bunch_status
from .models import *


class WaveSerializer(serializers.ModelSerializer):
    rating_range = serializers.SerializerMethodField(allow_null=True)

    # wave_bunch_id = serializers.IntegerField(source="wave_bunch.id")

    def get_rating_range(self, obj):
        result = None
        if hasattr(obj, "rating_interval"):
            data = obj.rating_interval
            result = (data.lower, data.upper)
        return result

    class Meta:
        model = Wave
        exclude = ('rating_interval',)

    def create(self, validated_data):
        wave = super().create(validated_data)
        wave.requests_spreadsheet = create_spreadsheet(wave)
        wave.save()
        return wave


class WaveBunchSerializer(serializers.ModelSerializer):
    programs = ProgramSerializer(read_only=True, many=True)
    semesters = serializers.SerializerMethodField(read_only=True)
    status = serializers.SerializerMethodField(read_only=True)
    programs_id = serializers.SlugRelatedField(
        source="programs",
        queryset=Program.objects.all(),
        many=True,
        write_only=True,
        slug_field='id',
    )

    # waves = WaveSerializer(source="wave_set", many=True)

    def get_semesters(self, obj):
        return set(obj.course_blocks.values_list('semester', flat=True))

    def get_status(self, obj):
        return get_wave_bunch_status(obj)

    class Meta:
        model = WaveBunch
        fields = ('id', 'degree', 'current_course', 'programs', 'start_date',
                  'end_date', 'semesters', 'status', 'course_blocks', 'programs_id')


class WaveBunchDetailSerializer(WaveBunchSerializer):
    waves = WaveSerializer(source="wave_set", many=True)
    course_blocks = CourseBlockSerializer(many=True)

    class Meta:
        model = WaveBunch
        fields = WaveBunchSerializer.Meta.fields + ('waves',)


class WaveStudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wave
        exclude = ('wave_number', 'rating_interval', 'type', 'wave_bunch', 'requests_spreadsheet',
                   'auto_distribution_date')


class WaveTeacherSerializer(serializers.ModelSerializer):
    courses = serializers.SerializerMethodField(allow_null=True)
    programs = serializers.SlugRelatedField(
        source="wave_bunch.programs",
        read_only=True,
        many=True,
        slug_field='name',
    )
    degree = serializers.CharField(source="wave_bunch.degree")
    current_course = serializers.CharField(source="wave_bunch.current_course")
    status = serializers.SerializerMethodField(read_only=True)

    def get_courses(self, obj):
        course_blocks = self.context['course_blocks']
        courses = []
        for block_id in obj.wave_bunch.course_blocks.all().values_list('id', flat=True):
            if block_id in course_blocks:
                courses = courses + course_blocks[block_id]
        return CourseWaveSerializer(courses, context={'wave': obj}, many=True).data

    def get_status(self, obj):
        return get_wave_status_for_teacher(obj)

    class Meta:
        model = Wave
        fields = ('id', 'wave_number', 'start_date', 'end_date', 'auto_distribution_date', 'courses', 'programs',
                  'degree', 'status', 'current_course', 'use_rating')


class WaveFieldsSerializer(WaveSerializer):
    class Meta:
        model = Wave
        exclude = WaveSerializer.Meta.exclude + ('wave_bunch', )


class WaveResultsSerializer(serializers.ModelSerializer):
    course_blocks = serializers.SerializerMethodField()

    def get_course_blocks(self, obj):
        return CourseBlockRequestsSerializer(
            obj.wave_bunch.course_blocks,
            many=True,
            context={'wave': obj, 'student': self.context['student']}
        ).data

    class Meta:
        model = Wave
        fields = ('id', 'course_blocks', 'wave_number', 'start_date', 'end_date', )
