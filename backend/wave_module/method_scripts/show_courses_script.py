from institute_module.models import StudentRating
from wave_module.method_strategies.show_courses_strategy import UsePreviousStrategy


def invalid_operation_exception(x):
    raise Exception("Invalid operation")


def perform_courses_operation(operation, *operation_args):

    ops = {
        "use_previous_requests": UsePreviousStrategy().get_courses_to_show,
    }

    operation_function = ops.get(operation, invalid_operation_exception)

    return operation_function(*operation_args)
