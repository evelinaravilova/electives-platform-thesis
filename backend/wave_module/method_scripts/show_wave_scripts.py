from institute_module.models import StudentRating


def in_rating_interval(wave, student):
    print("run rating interval")

    try:
        student_rating = StudentRating.objects.get(rating_type__key_name='Средний балл', student=student)
    except StudentRating.DoesNotExist:
        student_rating = None
    if student_rating:
        if wave.rating_interval.lower <= student_rating.score < wave.rating_interval.upper:
            return wave
        else:
            return None
    return wave


def can_participate(wave, student):
    print("run can participate")
    return wave


def invalid_operation_exception(x):
    raise Exception("Invalid operation")


def perform_wave_operation(operation, *operation_args):

    ops = {
        "rating_interval": in_rating_interval,
        "can_participate": can_participate,
    }

    operation_function = ops.get(operation, invalid_operation_exception)

    return operation_function(*operation_args)
