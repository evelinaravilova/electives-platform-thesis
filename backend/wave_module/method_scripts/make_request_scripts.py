from wave_module.method_strategies.make_request_strategies import TapkiStrategy, PriorityStrategy


def invalid_operation_exception(x):
    raise Exception("Invalid operation")


def perform_request_operation(operation, *operation_args):

    ops = {
        "without_rating": TapkiStrategy().make_request,
        "auto_accept": TapkiStrategy().make_request,
        "priority": PriorityStrategy().make_request,
    }

    operation_function = ops.get(operation, invalid_operation_exception)

    return operation_function(*operation_args)
