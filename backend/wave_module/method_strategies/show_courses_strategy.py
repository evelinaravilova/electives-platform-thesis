from __future__ import annotations
from abc import ABC, abstractmethod

from course_module.models import Course
from electives_platform.enums import REQUEST_STATUS
from request_module.models import CourseRequest


class ShowCoursesContext:

    def __init__(self, strategy: Strategy) -> None:
        self._strategy = strategy

    @property
    def strategy(self) -> Strategy:
        return self._strategy

    @strategy.setter
    def strategy(self, strategy: Strategy) -> None:
        self._strategy = strategy

    def get_courses_to_show(self, current_wave, user, block_id, *args):
        courses = Course.objects.filter(block_id=block_id)

        if current_wave:
            courses = self._strategy.get_courses_to_show(self, courses, current_wave, user, block_id)
        return courses


class Strategy(ABC):

    @abstractmethod
    def get_courses_to_show(self, courses, *args):
        pass


class CommonStrategy(Strategy):
    def get_courses_to_show(self, courses, *args):
        return courses


class UsePreviousStrategy(Strategy):

    def get_courses_to_show(self, courses, *args):
        wave = args[0]
        user = args[1]
        block_id = args[2]
        previous_requests = CourseRequest.objects.filter(wave__wave_number=wave.wave_number - 1, student__user=user,
                                                         course__block_id=block_id, status=REQUEST_STATUS[1][0])
        print(previous_requests)
        courses = previous_requests.values_list('course', flat=True)
        return Course.objects.filter(pk__in=list(courses))


def get_show_courses_strategy(current_wave):
    if current_wave.use_previous_requests:
        return UsePreviousStrategy
    return CommonStrategy

# if __name__ == "__main__":
# Клиентский код выбирает конкретную стратегию и передаёт её в контекст.
# Клиент должен знать о различиях между стратегиями, чтобы сделать
# правильный выбор.

# context = ShowCoursesContext(get_show_courses_strategy())
# print("Client: Strategy is set to normal sorting.")
# context.get_courses_to_show()
# print()
