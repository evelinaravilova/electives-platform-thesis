from __future__ import annotations
from abc import ABC, abstractmethod

from rest_framework import status

from electives_platform.enums import REQUEST_STATUS
from electives_platform.utils import ElectivesAPIException
from request_module.models import CourseRequest


class MakeRequestContext:

    def __init__(self, strategy: Strategy) -> None:
        self._strategy = strategy

    @property
    def strategy(self) -> Strategy:
        return self._strategy

    @strategy.setter
    def strategy(self, strategy: Strategy) -> None:
        self._strategy = strategy

    def make_request(self, current_wave, student, course, *args):
        course_request = CourseRequest(course=course, student=student, wave=current_wave)

        if current_wave:
            student_course_requests_count = CourseRequest.objects.filter(
                course__block=course.block,
                student=student,
                wave=current_wave
            ).count()

            if student_course_requests_count < current_wave.course_count:
                course_request = self._strategy.make_request(course_request, course, current_wave, *args)
            else:
                raise ElectivesAPIException(
                    detail="Превышено количество заявок в блоке",
                    status_code=status.HTTP_400_BAD_REQUEST,
                )
            course_request.save()
        return course_request


class Strategy(ABC):

    @abstractmethod
    def make_request(self, course_request, *args):
        pass


class CommonStrategy(Strategy):
    def make_request(self, course_request, *args):
        return course_request


class TapkiStrategy(Strategy):
    """ кто первый записался """

    def make_request(self, course_request, *args):
        course, wave = args[0], args[1]
        students_course_requests_count = CourseRequest.objects.filter(
            course=course,
            wave=wave).count()
        if students_course_requests_count < course.students_count:
            course_request.status = REQUEST_STATUS[1][0]
            return course_request
        else:
            raise ElectivesAPIException(
                detail="Превышена квота на курс",
                status_code=status.HTTP_400_BAD_REQUEST,
            )


class PriorityStrategy(Strategy):
    def make_request(self, course_request, *args):
        priority = args[2]
        if priority:
            course_request.priority = priority
        return course_request


def get_make_request_strategy(current_wave):
    if current_wave.course_count == 1 and not current_wave.use_rating or current_wave.auto_accept:
        return TapkiStrategy
    if current_wave.has_priority:
        return PriorityStrategy
    return CommonStrategy


# if __name__ == "__main__":
    # Клиентский код выбирает конкретную стратегию и передаёт её в контекст.
    # Клиент должен знать о различиях между стратегиями, чтобы сделать
    # правильный выбор.

    # context = MakeRequestContext(ConcreteStrategyA())
    # print("Client: Strategy is set to normal sorting.")
    # context.do_some_business_logic()
    # print()
    #
    # print("Client: Strategy is set to reverse sorting.")
    # context.strategy = ConcreteStrategyB()
    # context.do_some_business_logic()
