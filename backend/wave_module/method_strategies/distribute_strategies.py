from __future__ import annotations
from abc import ABC, abstractmethod

from course_module.models import Course
from electives_platform.enums import REQUEST_STATUS
from institute_module.models import StudentRating
from request_module.models import CourseRequest

from wave_module.models import Wave


class DistributeContext:

    def __init__(self, strategy: Strategy) -> None:
        self._strategy = strategy

    @property
    def strategy(self) -> Strategy:
        return self._strategy

    @strategy.setter
    def strategy(self, strategy: Strategy) -> None:
        self._strategy = strategy

    def distribute(self, current_wave, *args):
        blocks = current_wave.wave_bunch.course_blocks.all()
        course_requests = {}
        for block in blocks:
            courses = block.course_set.all()
            for course in courses:
                # if course.id == 9:
                student_requests = CourseRequest.objects.filter(course=course, wave=current_wave)
                course_requests[course.id] = self._strategy.distribute(self, student_requests, course)
        return course_requests


class Strategy(ABC):
    @abstractmethod
    def distribute(self, student_requests, *args):
        pass


class RatingStrategy(Strategy):
    def distribute(self, student_requests, *args):
        requests_students = student_requests.values_list('student', flat=True)
        course = args[0]
        print(course)
        accepted_students = StudentRating.objects\
            .filter(rating_type=course.rating_type, student_id__in=requests_students)\
            .order_by('-score')[:course.students_count]\
            .values_list('student', flat=True)[:course.students_count]
        for request in student_requests:
            request.status = REQUEST_STATUS[1][0] if request.student.id in accepted_students else REQUEST_STATUS[2][0]
            request.save()
        print(student_requests)
        return student_requests



def get_distribute_strategy(current_wave):
    if current_wave.use_rating:
        return RatingStrategy
    return Strategy


# current_wave = Wave.objects.get(pk=32)
#
# distribute_context = DistributeContext(get_distribute_strategy(current_wave))
# distribute_context.distribute(current_wave)
# print()
