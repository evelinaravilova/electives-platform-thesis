from datetime import datetime
from auth_module.utils import is_student
from institute_module.models import StudentProfile
from request_module.models import CourseRequest
from wave_module.fsm import WaveContext, WaveNumberState
from wave_module.models import Wave
from wave_module.serializers import WaveResultsSerializer


def get_student_current_wave(user):
    if is_student(user):
        student = StudentProfile.objects.get(user=user)
        now = datetime.now()
        try:
            current_wave = Wave.objects.get(wave_bunch__current_course=student.group.current_course,
                                            wave_bunch__programs=student.group.program,
                                            start_date__lte=now,
                                            end_date__gte=now)
            context = WaveContext(WaveNumberState())
            context.request_wave_script(current_wave, student)
            current_wave = context.obj

        except Wave.DoesNotExist:
            current_wave = None

        return current_wave
    return None


def get_student_results(student):
    student_requests = CourseRequest.objects.filter(student=student)
    student_waves_dict = {}
    for student_request in student_requests:
        if student_request.wave:
            wave_course_number = student_request.wave.wave_bunch.current_course
            if wave_course_number not in student_waves_dict:
                student_waves_dict[wave_course_number] = []
            else:
                if student_request.wave not in student_waves_dict[wave_course_number]:
                    student_waves_dict[wave_course_number].append(student_request.wave)

    for key in student_waves_dict:
        student_waves_dict[key].sort(key=lambda x: x.end_date)
        student_waves_dict[key] = WaveResultsSerializer(
            student_waves_dict[key],
            context={'student': student},
            many=True
        ).data
    return student_waves_dict

