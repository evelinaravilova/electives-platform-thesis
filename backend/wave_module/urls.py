from django.urls import path
from rest_framework import routers

from .views import WaveViewSet, DistributionViewSet, CreateWaveView

router = routers.DefaultRouter()
router.register(r'waves', WaveViewSet, basename="waves")
router.register(r'distributions', DistributionViewSet, basename="distributions")

urlpatterns = [
    path("wavefields", CreateWaveView.as_view(), name="create_wave"),
]

urlpatterns += router.urls
