from django.apps import AppConfig


class WaveModuleConfig(AppConfig):
    name = 'wave_module'
