from django.contrib.postgres.validators import RangeMinValueValidator, RangeMaxValueValidator
from django.db import models
from django.contrib.postgres.fields import IntegerRangeField
from psycopg2.extras import NumericRange

from course_module.models import CourseBlock
from electives_platform.enums import DEGREES, WAVE_TYPE
from institute_module.models import Institute, Program


class WaveBunch(models.Model):
    degree = models.CharField(max_length=8, choices=DEGREES, default='bachelor', verbose_name="Степень студентов")
    current_course = models.SmallIntegerField(default=1, verbose_name="Номер курса студентов")
    programs = models.ManyToManyField(Program, verbose_name="Направление")
    start_date = models.DateTimeField(verbose_name="Начало распределения", null=True, blank=True)
    end_date = models.DateTimeField(verbose_name="Конец распределения", null=True, blank=True)
    course_blocks = models.ManyToManyField(CourseBlock, verbose_name="Блоки курсов")

    class Meta:
        verbose_name_plural = "Связки волн"
        verbose_name = "Связка волн"

    def __str__(self):
        return f"Связка для {self.current_course} курса {self.degree}"


def get_default_rating_range():
    return NumericRange(0, 100)


class Wave(models.Model):
    wave_number = models.SmallIntegerField(default=1, verbose_name="Порядковый номер блока")
    start_date = models.DateTimeField(verbose_name="Начало волны")
    end_date = models.DateTimeField(verbose_name="Конец волны")
    auto_distribution_date = models.DateTimeField(blank=True, null=True,
                                                  verbose_name="Дата автоматического распределения")
    type = models.CharField(max_length=9, choices=WAVE_TYPE, default='selection', verbose_name="Тип волны")
    use_rating = models.BooleanField(default=False, verbose_name="Рейтинг")
    course_count = models.SmallIntegerField(default=1, verbose_name="Количество курсов на выбор")
    has_priority = models.BooleanField(default=False, verbose_name="Приоритет курсов")
    rating_interval = IntegerRangeField(default=get_default_rating_range,
                                        blank=True,
                                        null=True,
                                        validators=[
                                            RangeMinValueValidator(0),
                                            RangeMaxValueValidator(100)
                                        ],
                                        verbose_name="Интервал рейтинга по 100-бальной шкале тех, кто участвует")
    auto_accept = models.BooleanField(default=False, verbose_name="Автоматический прием")
    forbid_overflow = models.BooleanField(default=False, verbose_name="Запретить подавать на заполненный курс")
    can_participate = models.BooleanField(default=False, verbose_name="Можно ли участвовать тем, кто уже выбрал")
    use_previous_requests = models.BooleanField(default=False, verbose_name="Выбор из тех курсов, куда прошел")
    show_requests_count = models.BooleanField(default=False, verbose_name="Показывать количество заявок на курс")
    show_result = models.BooleanField(default=False, verbose_name="Показать результаты студенту")
    wave_bunch = models.ForeignKey(WaveBunch, on_delete=models.CASCADE, verbose_name="Связка волн")
    requests_spreadsheet = models.URLField(max_length=500, blank=True, null=True,
                                           verbose_name="Ссылка на google таблицу с заявками")
    auto_distribution_task_id = models.CharField(max_length=100, null=True, blank=True,
                                                 verbose_name="ID таска на распределение")

    class Meta:
        verbose_name_plural = "Волны"
        verbose_name = "Волна"

    def __str__(self):
        return f"{self.wave_bunch.current_course} курс {self.wave_bunch.degree}: {self.wave_number} волна"

