from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from auth_module.permissions import IsDeanery
from auth_module.utils import is_student, is_teacher
from course_module.models import Course
from electives_platform.celery import app
from electives_platform.enums import WAVE_TYPE
from electives_platform.utils import ElectivesAPIException, method_permission_classes
from institute_module.models import DeaneryProfile, StudentProfile
from wave_module.fsm import WaveContext, state_classes
from wave_module.models import Wave, WaveBunch
from wave_module.serializers import WaveSerializer, WaveStudentSerializer, WaveBunchSerializer, \
    WaveBunchDetailSerializer, WaveFieldsSerializer, WaveTeacherSerializer
from request_module.tasks import wave_distribute
from wave_module.student_queries import get_student_current_wave, get_student_results
from electives_platform.settings import CELERY_BROKER_URL


class WaveViewSet(ModelViewSet):
    queryset = Wave.objects.all()
    serializer_class = WaveSerializer

    @action(detail=False)
    def current_wave(self, request):
        user = request.user

        if is_student(user):
            current_wave = get_student_current_wave(user)
            if current_wave:
                data = WaveStudentSerializer(current_wave).data
            else:
                data = None
            return Response(data=data, status=status.HTTP_200_OK)

    @action(detail=False)
    def wave_courses(self, request):
        user = request.user
        if is_teacher(user):
            courses = Course.objects.filter(head__user__id__exact=user.id)
            course_blocks = dict()
            for course in courses:
                if course.block_id in course_blocks:
                    course_blocks[course.block_id].append(course)
                else:
                    course_blocks[course.block_id] = [course]
            course_blocks_keys = course_blocks.keys()
            waves = Wave.objects.filter(wave_bunch__course_blocks__in=course_blocks_keys,
                                        type=WAVE_TYPE[0][0]).distinct()

            serializer = WaveTeacherSerializer(waves, context={'user': user, 'course_blocks': course_blocks}, many=True)
            return Response(data=serializer.data, status=status.HTTP_200_OK)

    @action(detail=False)
    def available_wave_numbers(self, request):
        wave_bunch_id = request.query_params.get('wave_bunch_id')
        existing_numbers = Wave.objects \
            .filter(wave_bunch__id=int(wave_bunch_id)) \
            .values_list('wave_number', flat=True)
        new_numbers = [i for i in range(1, 10) if i not in existing_numbers]

        return Response(data={'wave_numbers': new_numbers}, status=status.HTTP_200_OK)

    @action(detail=False)
    def wave_results(self, request):
        student = get_object_or_404(StudentProfile, user=request.user)
        return Response(data={'current_course': student.group.current_course, 'results': get_student_results(student)},
                        status=status.HTTP_200_OK)


class DistributionViewSet(ModelViewSet):
    queryset = WaveBunch.objects.all()
    serializer_class = WaveBunchSerializer

    def get_serializer_class(self):
        if self.action == 'list' or self.action == 'create':
            return WaveBunchSerializer
        elif self.action == 'retrieve':
            return WaveBunchDetailSerializer

    @method_permission_classes((IsDeanery,))
    def list(self, request, **kwargs):
        deanery = get_object_or_404(DeaneryProfile, user=request.user)
        wave_bunches = self.queryset.filter(programs__institute=deanery.institute).distinct()
        serializer = self.get_serializer_class()(wave_bunches, many=True)
        print(CELERY_BROKER_URL)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


@receiver(post_save, sender=Wave, dispatch_uid="update_wave_bunch")
def update_wave_bunch(sender, instance, **kwargs):
    if instance.wave_number == 1:
        instance.wave_bunch.start_date = instance.start_date
        instance.wave_bunch.save()
    else:
        waves_count = Wave.objects.filter(wave_bunch=instance.wave_bunch).count()
        if instance.wave_number == waves_count:
            instance.wave_bunch.end_date = instance.end_date
            instance.wave_bunch.save()


@receiver(pre_save, sender=Wave, dispatch_uid="update_wave_auto_distribution")
def update_wave_auto_distribution(sender, instance, **kwargs):
    if instance.id is not None:
        previous = Wave.objects.get(id=instance.id)
        if previous.auto_distribution_task_id:
            app.control.revoke(previous.auto_distribution_task_id)
    if instance.auto_distribution_date:
        result = wave_distribute.apply_async((instance.id,), eta=instance.auto_distribution_date)
        instance.auto_distribution_task_id = result.id


class CreateWaveView(APIView):

    def get(self, request):
        pass

    def post(self, request):
        serializer = WaveFieldsSerializer(data=request.data['wave'])
        try:
            serializer.is_valid(raise_exception=True)
        except Http404:
            raise ElectivesAPIException(
                detail="Неверные данные",
                status_code=status.HTTP_400_BAD_REQUEST,
            )
        validated_data = serializer.validated_data

        context = WaveContext(state_classes[request.data['state']]())
        fields = context.request_next_fields(validated_data)

        return Response(data=fields, status=status.HTTP_200_OK)
