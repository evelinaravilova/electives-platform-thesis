from __future__ import annotations
from abc import ABC, abstractmethod

from django.db.models import QuerySet

from course_module.models import Course
from electives_platform.enums import WAVE_TYPE
from request_module.models import CourseRequest
from wave_module.method_scripts.make_request_scripts import perform_request_operation
from wave_module.method_scripts.show_courses_script import perform_courses_operation
from wave_module.method_scripts.show_wave_scripts import perform_wave_operation
from wave_module.models import Wave


class WaveContext:
    _state = None

    def __init__(self, state: State, obj=None) -> None:
        self.transition_to(state)
        self.obj = obj

    def transition_to(self, state: State):
        print(f"Context: Transition to {type(state).__name__}")
        self._state = state
        self._state.context = self

    def request_next_fields(self, wave):
        return self._state.get_next_fields(wave)

    def request_wave_script(self, wave, student):
        return self._state.run_wave_script(wave, student)

    def request_make_request_script(self, course_request, course, wave, priority=None):
        return self._state.run_make_request_script(course_request, course, wave, priority)

    def request_course_script(self, courses, wave, user, block_id):
        return self._state.run_course_script(courses, wave, user, block_id)


class State(ABC):

    @property
    def context(self) -> WaveContext:
        return self._context

    @context.setter
    def context(self, context: WaveContext) -> None:
        self._context = context

    @abstractmethod
    def get_next_fields(self, wave) -> []:
        pass

    @abstractmethod
    def run_wave_script(self, wave, student) -> Wave or None:
        pass

    @abstractmethod
    def run_make_request_script(self, course_request, course, wave, priority=None) -> CourseRequest or None:
        pass

    @abstractmethod
    def run_course_script(self, courses, wave, user, block_id) -> 'QuerySet[Course]':
        pass


def is_selection(t):
    return t == WAVE_TYPE[0][0]


class WaveTypeState(State):
    def get_next_fields(self, wave):
        if wave['type'] == WAVE_TYPE[0][0]:
            return [['start_date', 'end_date', 'forbid_overflow', 'show_requests_count'], []]
        else:
            return [['start_date'], ['end_date', 'auto_distribution_date', 'forbid_overflow', 'show_requests_count',
                                     'rating_range', 'use_previous_requests']]

    def run_wave_script(self, wave, student):
        pass

    def run_make_request_script(self, *args):
        self.context.transition_to(AutoAcceptState())
        self.context.request_make_request_script(*args)

    def run_course_script(self, courses, wave, user, block_id):
        pass


class WaveNumberState(State):
    def transition(self, wave):
        if wave.wave_number == 1:
            self.context.transition_to(RatingState())
        else:
            self.context.transition_to(CanParticipateState())

    def get_next_fields(self, wave):
        if wave['wave_number'] == 1:
            return [[], ['can_participate', 'use_previous_requests']]
        else:
            return [['can_participate', 'use_previous_requests'], []]

    def run_wave_script(self, wave, student):
        self.transition(wave)
        self.context.request_wave_script(wave, student)

    def run_make_request_script(self, *args):
        self.transition(args[2])
        self.context.request_make_request_script(*args)

    def run_course_script(self, *args):
        self.transition(args[1])
        self.context.request_course_script(*args)


class RatingState(State):
    def get_next_fields(self, wave):
        if wave['use_rating']:
            return [['auto_distribution_date', 'rating_range']
                    if is_selection(wave['type']) else ['course_count'], []]
        else:
            return [[], ['rating_range', 'auto_distribution_date']]

    def run_wave_script(self, wave, student):
        if wave.use_rating:
            self.context.transition_to(RatingIntervalState())
            self.context.request_wave_script(wave, student)

    def run_make_request_script(self, course_request, course, wave, priority=None):
        if not wave.use_rating:
            self.context.obj = perform_request_operation("without_rating", course_request, course, wave, priority)
        self.context.transition_to(RatingIntervalState())
        self.context.request_make_request_script(course_request, course, wave, priority)

    def run_course_script(self, *args):
        self.context.transition_to(RatingIntervalState())
        self.context.request_course_script(*args)


class CourseCountState(State):
    def get_next_fields(self, wave):
        if wave['course_count'] > 1:
            return [['has_priority'] if is_selection(wave['type']) and wave['wave_number'] > 1
                    else ['has_priority'], []]
        else:
            return [[], ['has_priority']]

    def run_wave_script(self, wave, student):
        self.context.transition_to(PriorityState())
        self.context.request_wave_script(wave, student)

    def run_make_request_script(self, *args):
        self.context.transition_to(PriorityState())
        self.context.request_make_request_script(*args)

    def run_course_script(self, *args):
        self.context.transition_to(PriorityState())
        self.context.request_course_script(*args)


class RatingIntervalState(State):
    def get_next_fields(self, wave):
        pass

    def run_wave_script(self, wave, student):
        self.context.obj = perform_wave_operation("rating_interval", wave, student)
        if self.context.obj:
            self.context.transition_to(CourseCountState())
            self.context.request_wave_script(wave, student)

    def run_make_request_script(self, *args):
        self.context.transition_to(CourseCountState())
        self.context.request_make_request_script(*args)

    def run_course_script(self, *args):
        self.context.transition_to(CourseCountState())
        self.context.request_course_script(*args)


class CanParticipateState(State):
    def get_next_fields(self, wave):
        pass

    def run_wave_script(self, wave, student):
        self.context.obj = perform_wave_operation("can_participate", wave, student)
        if self.context.obj:
            self.context.transition_to(RatingState())
            self.context.request_wave_script(wave, student)

    def run_make_request_script(self, *args):
        self.context.transition_to(RatingState())
        self.context.request_make_request_script(*args)

    def run_course_script(self, *args):
        self.context.transition_to(RatingState())
        self.context.request_course_script(*args)


class UsePreviousRequestsState(State):
    def get_next_fields(self, wave):
        pass

    def run_wave_script(self, wave, student):
        pass

    def run_make_request_script(self, *args):
        pass

    def run_course_script(self, courses, wave, user, block_id):
        if wave.use_previous_requests:
            self.context.obj = perform_courses_operation("use_previous_requests", courses, wave, user, block_id)


class PriorityState(State):
    def transition(self, wave):
        if wave.wave_number > 1:
            self.context.transition_to(UsePreviousRequestsState())

    def get_next_fields(self, wave):
        pass

    def run_wave_script(self, wave, student):
        self.transition(wave)
        self.context.request_wave_script(wave, student)

    def run_make_request_script(self, *args):
        self.transition(args[2])
        self.context.request_make_request_script(*args)

    def run_course_script(self, *args):
        self.transition(args[1])
        self.context.request_course_script(*args)


class AutoAcceptState(State):
    def get_next_fields(self, wave):
        pass

    def run_wave_script(self, wave, student):
        pass

    def run_make_request_script(self, course_request, course, wave, priority=None):
        if wave.auto_accept:
            self.context.obj = perform_request_operation("auto_accept", course_request, course, wave, priority)
        self.context.transition_to(WaveNumberState())
        self.context.request_make_request_script(course_request, course, wave, priority)

    def run_course_script(self, *args):
        self.context.transition_to(WaveNumberState())
        self.context.request_course_script(*args)


state_classes = {'type': WaveTypeState,
                 'wave_number': WaveNumberState,
                 'use_rating': RatingState,
                 'course_count': CourseCountState}

# Клиентский код.
# context = WaveContext(WaveNumberState())
# context.request(wave)
