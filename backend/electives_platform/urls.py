from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

# admin.site.site_url = 'http://course-assignment.a.uenv.ru'
admin.site.site_header = 'Курсы по выбору'
admin.site.site_title = 'Создание курсов по выбору и наборов'
admin.site.index_title = 'Создание курсов по выбору и наборов'

urlpatterns = [
    path('jet/', include('jet.urls', 'jet')),
    path('admin/', admin.site.urls),
    path('auth/', include('auth_module.urls')),
    path('institute/', include('institute_module.urls')),
    path('wave_module/', include('wave_module.urls')),
    path('course_module/', include('course_module.urls')),
    path('request_module/', include('request_module.urls'))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
