from rest_framework import status
from rest_framework.exceptions import ErrorDetail, APIException
from rest_framework.renderers import JSONRenderer
from rest_framework.views import exception_handler


class CustomRenderer(JSONRenderer):

    def render(self, data, accepted_media_type=None, renderer_context=None):
        response_data = {'message': 'ok', 'data': data, 'success': True}

        print(data)
        if isinstance(data, dict):
            detail = data.get('detail', None)
            if detail:
                response_data = {'message': detail, 'data': [], 'success': False}
        response = super(CustomRenderer, self).render(
            response_data, accepted_media_type, renderer_context)
        return response


class ElectivesAPIException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = 'Unknown error'
    default_code = 'error'

    def __init__(self, status_code=None, detail=None, code=None):
        super(ElectivesAPIException, self).__init__(detail, code)
        if status_code:
            self.status_code = status_code


def method_permission_classes(classes):
    def decorator(func):
        def decorated_func(self, *args, **kwargs):
            self.permission_classes = classes
            # this call is needed for request permissions
            self.check_permissions(self.request)
            return func(self, *args, **kwargs)
        return decorated_func
    return decorator



