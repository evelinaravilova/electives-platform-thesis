ENROLLMENT_STATUS = (
    ("opened", 'Набор открыт'),
    ("closed", 'Набор закрыт')
)

DEGREES = (
    ("bachelor", 'Бакалавриат'),
    ("magister", 'Магистратура'),
)

REQUEST_STATUS = (
    ("submitted", 'На рассмотрении'),
    ("accepted", 'Принята'),
    ("rejected", 'Отклонена'),
)

STUDENT = "student"
TEACHER = "teacher"
DEANERY = "admin"

ROLES = (
    (STUDENT, "Студент"),
    (TEACHER, "Преподаватель"),
    (DEANERY, "Сотрудник"),
    ("superadmin", "Администратор")
)

RATING_MODE = (
    ("score", "Балл"),
    ("interview", "Собеседование")
)

TAG_TYPE = (
    ("lab", "Лаборатория"),
    ("skill", "Навык")
)

WAVE_TYPE = (
    ("selection", "Выбор"),
    ("algorithm", "Алгоритм")
)

WAVE_BUNCH_STATUS = (
    ("waiting", "Ожидание"),
    ("active", "Активно"),
    ("finished", "Завершено")
)
