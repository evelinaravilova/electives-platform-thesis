from googleapiclient.discovery import build
from google.oauth2 import service_account

SCOPES = ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive']
SERVICE_ACCOUNT_FILE = r'./electives_platform/google_sheets/service.json'

creds = service_account.Credentials.from_service_account_file(
        SERVICE_ACCOUNT_FILE, scopes=SCOPES)

SAMPLE_SPREADSHEET_ID = '1VrF7ihQJ9wF3J-CrWCVoPOn6JLsFIWzJbaFbfWrlUng'

sheets_service = build('sheets', 'v4', credentials=creds)

drive_service = build('drive', 'v3', credentials=creds)
