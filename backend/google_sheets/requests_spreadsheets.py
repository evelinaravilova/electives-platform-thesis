from google_sheets.config import sheets_service, drive_service
from wave_module.utils import get_degree_name

NAME_COLUMNS_COUNT = 3


def create_spreadsheet(wave_data):
    # wave_data = Wave.objects.all().first()
    header_row = [{"userEnteredValue": {"stringValue": "ФИО"}},
                  {"userEnteredValue": {"stringValue": "Дата подачи"}},
                  {"userEnteredValue": {"stringValue": "Статус"}}]

    header_row_columns_count = NAME_COLUMNS_COUNT

    if wave_data.use_rating:
        header_row.append({"userEnteredValue": {"stringValue": "Рейтинг"}})
        header_row_columns_count += 1

    course_blocks = wave_data.wave_bunch.course_blocks.all()
    sheets = []
    for course_block in course_blocks:
        courses = course_block.course_set.all()
        for course in courses:
            sheet_title = str(course)
            sheets.append(
                {
                    "properties": {
                        "title": sheet_title,
                        "sheetType": "GRID",
                        "gridProperties": {"columnCount": header_row_columns_count}
                    },
                    "data": [{
                        "startRow": 0,
                        "startColumn": 0,
                        "rowData": [{"values": header_row}]
                    }]
                })
    spreadsheet = {
        'properties': {
            'title': f'Заявки {wave_data.wave_bunch.current_course} курса ' +
                     f'{get_degree_name(wave_data.wave_bunch.degree)} - волна {wave_data.wave_number}',
            'locale': 'ru_RU'
        },
        'sheets': sheets
    }
    spreadsheet = sheets_service.spreadsheets().create(
        body=spreadsheet, fields='spreadsheetId'
    ).execute()
    spreadsheet_id = spreadsheet.get('spreadsheetId')
    spreadsheet = sheets_service.spreadsheets().get(spreadsheetId=spreadsheet_id).execute()
    sheet_ids = [sheet['properties']['sheetId'] for sheet in spreadsheet.get('sheets')]
    updated_sheets = []

    for i, sheet_id in enumerate(sheet_ids):
        updated_sheets.append({"addProtectedRange": {
            "protectedRange": {
                "range": {
                    "sheetId": sheet_id,
                    "startRowIndex": 0,
                    "endRowIndex": 1,
                    "startColumnIndex": 0,
                    "endColumnIndex": header_row_columns_count - 1,
                },
                "editors": {"users": []}
            }
        }
        })
        updated_sheets.append({"updateDimensionProperties": {
            "range": {
                "sheetId": sheet_id,
                "dimension": "COLUMNS",
                "startIndex": 0,
                "endIndex": header_row_columns_count
            },
            "properties": {
                "pixelSize": 300
            },
            "fields": "pixelSize"
        }})
        updated_sheets.append({"updateBorders": {
            "range": {
                "sheetId": sheet_id,
                "startRowIndex": 0,
                "endRowIndex": 1,
                "startColumnIndex": 0,
                "endColumnIndex": header_row_columns_count
            },
            "bottom": {
                "style": "SOLID",
            }
        }})

    sheets_service.spreadsheets().batchUpdate(
        spreadsheetId=spreadsheet_id, body={
            "requests": updated_sheets}
    ).execute()

    drive_service.permissions().create(
        fileId=spreadsheet_id,
        body={'type': 'user', 'role': 'writer', 'emailAddress': 'evelinaravilova@gmail.com'},
        fields='id').execute()

    # delete_spreadsheet(spreadsheet_id)

    return spreadsheet.get('spreadsheetUrl')


def __deprecated__create_spreadsheet(wave_data):
    # wave_data = Wave.objects.all().first()
    header_row = [{"userEnteredValue": {"stringValue": "Юзернейм КФУ"}}, {"userEnteredValue": {"stringValue": "ФИО"}}]
    header_row_columns_count = NAME_COLUMNS_COUNT + wave_data.course_count
    for c in range(wave_data.course_count):
        header_row.append({"userEnteredValue": {"stringValue": "Курс " + str(c + 1)}})
    course_blocks = wave_data.course_blocks.all()
    sheets = []
    for course_block in course_blocks:
        sheet_title = str(course_block)
        sheets.append(
            {
                "properties": {
                    "title": sheet_title,
                    "sheetType": "GRID",
                    "gridProperties": {"columnCount": header_row_columns_count}
                },
                "data": [{
                    "startRow": 0,
                    "startColumn": 0,
                    "rowData": [{"values": header_row}]
                }]
            })
    spreadsheet = {
        'properties': {
            'title': f'Заявки {wave_data.wave_bunch.current_course} курса ' +
                     f'{get_degree_name(wave_data.wave_bunch.degree)} - волна {wave_data.wave_number}',
            'locale': 'ru_RU'
        },
        'sheets': sheets
    }
    spreadsheet = sheets_service.spreadsheets().create(
        body=spreadsheet, fields='spreadsheetId'
    ).execute()
    spreadsheet_id = spreadsheet.get('spreadsheetId')
    spreadsheet = sheets_service.spreadsheets().get(spreadsheetId=spreadsheet_id).execute()
    sheet_ids = [sheet['properties']['sheetId'] for sheet in spreadsheet.get('sheets')]
    updated_sheets = []

    for i, sheet_id in enumerate(sheet_ids):
        updated_sheets.append({"addProtectedRange": {
            "protectedRange": {
                "range": {
                    "sheetId": sheet_id,
                    "startRowIndex": 0,
                    "endRowIndex": 1,
                    "startColumnIndex": 0,
                    "endColumnIndex": header_row_columns_count - 1,
                },
                "editors": {"users": []}
            }
        }
        })
        courses = course_blocks[i].course_set
        cell_values = []
        for course in courses.iterator():
            cell_values.append({"userEnteredValue": course.name})
        updated_sheets.append({"setDataValidation": {
            "range": {
                "sheetId": sheet_id,
                "startRowIndex": 1,
                "endRowIndex": 100,
                "startColumnIndex": 2,
                "endColumnIndex": header_row_columns_count
            },
            "rule": {
                "condition": {
                    "type": 'ONE_OF_LIST',
                    "values": cell_values,
                },
                "showCustomUi": True,
                "strict": True
            }
        }})
        updated_sheets.append({"updateDimensionProperties": {
            "range": {
                "sheetId": sheet_id,
                "dimension": "COLUMNS",
                "startIndex": 0,
                "endIndex": header_row_columns_count
            },
            "properties": {
                "pixelSize": 300
            },
            "fields": "pixelSize"
        }})

    sheets_service.spreadsheets().batchUpdate(
        spreadsheetId=spreadsheet_id, body={
            "requests": updated_sheets}
    ).execute()

    drive_service.permissions().create(
        fileId=spreadsheet_id,
        body={'type': 'user', 'role': 'writer', 'emailAddress': 'evelinaravilova@gmail.com'},
        fields='id').execute()

    # delete_spreadsheet(spreadsheet_id)

    return spreadsheet.get('spreadsheetUrl')


def get_student_row_index(rows, student_name):
    row_index = 0
    for row in rows:
        if row[0] == student_name:
            break
        row_index += 1
    return row_index


def write_student_request(spreadsheet_id, student_request_by_course):
    course = student_request_by_course[0]
    result = sheets_service.spreadsheets().values().get(spreadsheetId=spreadsheet_id, range=course).execute()
    columns_count = len(result['values'][0])
    row_index = get_student_row_index(result['values'], student_request_by_course[1])
    print(row_index)

    updated_row = student_request_by_course[1:]

    if updated_row[1] == '' and updated_row[2] == '':
        updated_row[0] = ''

    print(updated_row)

    result = sheets_service.spreadsheets().values().update(
        spreadsheetId=spreadsheet_id, valueInputOption="USER_ENTERED", body={
            "values": [
                updated_row
            ]
        }, range=f"{course}!A{row_index+1}:{chr(64 + columns_count)}{row_index+1}").execute()
    print(result)


def __deprecated__write_student_request(spreadsheet_id, updated_student_row, course_block_name, username):
    result = sheets_service.spreadsheets().values().get(spreadsheetId=spreadsheet_id, range=course_block_name).execute()
    columns_count = len(result['values'][0])
    row_index = 0
    for row in result['values']:
        if row[0] == username:
            break
        row_index += 1
    print(row_index)

    result = sheets_service.spreadsheets().values().update(
        spreadsheetId=spreadsheet_id, valueInputOption="USER_ENTERED", body={
            "values": [
                updated_student_row
            ]
        }, range=f"{course_block_name}!A{row_index+1}:{chr(64 + columns_count)}{row_index+1}").execute()
    print(result)


def write_student_rating(spreadsheet_id, student_rating_by_course):
    course = student_rating_by_course[0]
    result = sheets_service.spreadsheets().values().get(spreadsheetId=spreadsheet_id, range=course).execute()
    columns_count = len(result['values'][0])
    row_index = get_student_row_index(result['values'], student_rating_by_course[1])
    print(row_index)

    updated_row = result['values'][row_index]
    updated_row.append(student_rating_by_course[2])

    print(updated_row)

    result = sheets_service.spreadsheets().values().update(
        spreadsheetId=spreadsheet_id, valueInputOption="USER_ENTERED", body={
            "values": [
                updated_row
            ]
        }, range=f"{course}!A{row_index+1}:{chr(64 + columns_count)}{row_index+1}").execute()
    print(result)


def write_student_request_status(spreadsheet_id, course, student_requests_by_course):
    print(course)
    print(student_requests_by_course)
    result = sheets_service.spreadsheets().values().get(spreadsheetId=spreadsheet_id, range=course).execute()
    columns_count = len(result['values'][0])

    result = sheets_service.spreadsheets().values().update(
        spreadsheetId=spreadsheet_id, valueInputOption="USER_ENTERED", body={
            "values": student_requests_by_course
        }, range=f"{course}!A2:{chr(64 + columns_count)}{len(student_requests_by_course)+1}").execute()
    print(result)


# print(create_spreadsheet(None))
