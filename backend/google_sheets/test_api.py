from google_sheets.config import sheets_service, drive_service


def read_data(spreadsheetId, cell_range):
    result = sheets_service.spreadsheets().values().get(spreadsheetId=spreadsheetId, range=cell_range).execute()
    print(result)
    return result


def write_data(body, spreadsheetId, cell_range):
    result = sheets_service.spreadsheets().values().update(
         spreadsheetId=spreadsheetId, range=cell_range,
         valueInputOption="USER_ENTERED", body=body).execute()
    print('{0} cells updated.'.format(result.get('updatedCells')))
    return result


def create_spreadsheet(body):
    spreadsheet = sheets_service.spreadsheets().create(
        body=body, fields='spreadsheetId'
    ).execute()
    print('Spreadsheet ID: {0}'.format(spreadsheet.get('spreadsheetId')))
    return spreadsheet


def access_spreadsheet(spreadsheetId):
    access = drive_service.permissions().create(
        fileId=spreadsheetId,
        body={'type': 'user', 'role': 'writer', 'emailAddress': 'evelinaravilova@gmail.com'},
        fields='id').execute()
    return access


def delete_spreadsheet(spreadsheetId):
    drive_service.files().delete(fileId=spreadsheetId).execute()


if __name__ == '__main__':
    spreadsheet = {
        'properties': {
            'title': "Робот таблица"
        }
    }

    # spreadsheetId = create_spreadsheet(spreadsheet).get('spreadsheetId')

    # access_spreadsheet(spreadsheetId)

    # delete_spreadsheet(spreadsheetId)

    spreadsheetId = '1SNp4H2MDNz0oYp7i22zucmAQLxTAUwLl8MgOEsLEk_k'

    values = [
        [
            'Привет', 'я робот'
        ],
    ]
    body = {
        'values': values
    }

    cell_range = 'Sheet1!A8:B8'

    write_data(body, spreadsheetId, cell_range)

    read_data(spreadsheetId, cell_range)



