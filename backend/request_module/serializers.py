from rest_framework import serializers

from auth_module.serializers import UserSerializer
from course_module.models import Course
from institute_module.models import StudentRating
from institute_module.serializers import StudentProfileSerializer, RatingTypeSerializer
from request_module.models import CourseRequest


class CourseRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseRequest
        fields = ['id', 'status', 'priority']


# для препода
class CourseRequestListSerializer(serializers.ModelSerializer):
    user = UserSerializer(source='student.user')
    group = serializers.CharField(source='student.group.group_number')
    rating = serializers.SerializerMethodField(allow_null=True, required=False)

    def get_rating(self, obj):
        try:
            student_rating = StudentRating.objects.get(rating_type=self.context['rating_type'],
                                                       student=obj.student).score
        except StudentRating.DoesNotExist:
            student_rating = None
        return student_rating

    class Meta:
        model = CourseRequest
        fields = ['id', 'user', 'group', 'rating']


class CourseWithRequestsSerializer(serializers.ModelSerializer):
    requests = serializers.SerializerMethodField(allow_null=True)
    rating_type = RatingTypeSerializer(required=False, allow_null=True)

    def get_requests(self, obj):
        course_requests = CourseRequest.objects.filter(course=obj, wave_id=self.context['wave_id'])
        return CourseRequestListSerializer(course_requests, context=self.context, many=True).data

    class Meta:
        model = Course
        fields = ['id', 'name', 'students_count', 'requests', 'rating_type']


class CourseRequestResultSerializer(serializers.ModelSerializer):
    course = serializers.CharField(source='course.name')

    class Meta:
        model = CourseRequest
        fields = ['id', 'status', 'course']
