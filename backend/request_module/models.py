from django.db import models

from course_module.models import Course
from electives_platform.enums import REQUEST_STATUS
from institute_module.models import StudentProfile
from wave_module.models import Wave


class CourseRequest(models.Model):
    status = models.CharField(max_length=9, choices=REQUEST_STATUS, default="submitted", verbose_name="Статус заявки")
    priority = models.SmallIntegerField(null=True, blank=True, verbose_name="Приоритет")
    message = models.TextField(blank=True, verbose_name="Замечание")
    date_of_request = models.DateTimeField(auto_now_add=True, verbose_name="Дата подачи заявки")
    student = models.ForeignKey(StudentProfile, on_delete=models.CASCADE, verbose_name="Студент")
    course = models.ForeignKey(Course, on_delete=models.CASCADE, verbose_name="Курс")
    wave = models.ForeignKey(Wave, on_delete=models.SET_NULL, null=True, default=None, verbose_name="Волна")

    class Meta:
        verbose_name_plural = "Заявки на курсы по выбору"
        verbose_name = "Заявка на курс по выбору"

    def __str__(self):
        return f"{str(self.student)}, {str(self.date_of_request.date())}, статус: {self.status}, курс {str(self.course)}, волна {str(self.wave)}"
