from django.contrib import admin

from .models import *


class CourseRequestAdmin(admin.ModelAdmin):
    autocomplete_fields = ['student']


admin.site.register(CourseRequest, CourseRequestAdmin)
