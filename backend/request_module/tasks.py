import datetime

from celery import shared_task
from django.utils import timezone

from course_module.models import Course
from electives_platform.celery import app
from institute_module.models import StudentRating
from request_module.models import CourseRequest
from google_sheets.requests_spreadsheets import write_student_request, write_student_rating, \
    write_student_request_status
from wave_module.method_strategies.distribute_strategies import DistributeContext, get_distribute_strategy
from wave_module.models import Wave
from wave_module.utils import get_status_name


def prepare_send_request_data(student, wave, course, course_request=None):
    if course_request:
        student_request_by_course = [
            course.name,
            str(student.user),
            timezone.localtime(course_request.date_of_request).strftime("%d.%m.%Y %H:%M"),
            get_status_name(course_request.status)
        ]
    else:
        student_request_by_course = [course.name, str(student.user), '', '']

    spreadsheet_id = wave.requests_spreadsheet.split('/')[5]
    print(spreadsheet_id)

    return spreadsheet_id, student_request_by_course


def prepare_send_rating_data(rating, wave_id, course_id):
    course = Course.objects.get(pk=course_id)
    spreadsheet_id = Wave.objects.get(pk=wave_id).requests_spreadsheet.split('/')[5]

    return spreadsheet_id, [str(course), str(rating.student.user), rating.score]


def prepare_send_course_distribution_data(course, requests, wave):
    student_requests_by_course = []

    for request in requests:
        try:
            rating_score = StudentRating.objects.get(student=request.student, rating_type=course.rating_type).score
        except StudentRating.DoesNotExist:
            rating_score = 0
        student_requests_by_course.append([
            str(request.student.user),
            timezone.localtime(request.date_of_request).strftime("%d.%m.%Y %H:%M"),
            get_status_name(request.status),
            rating_score
        ])

    student_requests_by_course.sort(key=lambda x: x[3], reverse=True)

    spreadsheet_id = wave.requests_spreadsheet.split('/')[5]

    return spreadsheet_id, course, student_requests_by_course


@app.task
def send_student_request(*args):
    write_student_request(*args)


@app.task
def send_student_rating(*args):
    write_student_rating(*args)


@app.task
def send_course_distribution(*args):
    write_student_request_status(*args)


@app.task
def wave_distribute(wave_id):
    wave = Wave.objects.get(id=wave_id)
    distribute_context = DistributeContext(get_distribute_strategy(wave))
    course_requests = distribute_context.distribute(wave)
    for course_id in course_requests:
        course = Course.objects.get(pk=course_id)
        if wave.requests_spreadsheet:
            send_course_distribution(*prepare_send_course_distribution_data(course, course_requests[course_id], wave))

#
# user_id = '582ee32a5b9c861c87dc297e'
# tag = 'new_tag'
# started_at = datetime.datetime(2021, 5, 20, 11, 40, 0)


# wave_distribute.apply_async((wave), eta=started_at)
