# Generated by Django 3.1.7 on 2021-04-21 09:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('request_module', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='courserequest',
            name='priority',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Приоритет'),
        ),
    ]
