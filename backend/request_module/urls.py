from django.urls import path

from rest_framework import routers

from .views import RequestViewSet, RequestAnalyticsView


router = routers.DefaultRouter()
router.register(r'requests', RequestViewSet, basename="requests")

urlpatterns = [
    path("analytics", RequestAnalyticsView.as_view(), name="analytics"),
]

urlpatterns += router.urls
