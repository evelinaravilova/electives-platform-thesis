import numpy as np


def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i+n]


def resample(arr, new_length):
    chunk_size = round(len(arr)/new_length)
    return [np.mean(chunk) for chunk in chunks(arr, chunk_size)]


def get_average_array(arrs):
    result = [0 for i in range(len(arrs[0]))]
    for a in arrs:
        for i in range(len(a)):
            if i < len(result):
                result[i] += a[i]
    average_result = [i/len(arrs) for i in result]
    return average_result


def scale_linear(arr, max, min, high=100, low=0):
    return np.interp(arr, (min, max), (low, high))
