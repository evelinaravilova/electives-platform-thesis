from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from auth_module.permissions import IsTeacher, IsStudent, IsDeanery
from course_module.models import Course
from electives_platform.utils import ElectivesAPIException, method_permission_classes
from institute_module.models import StudentProfile, StudentRating, RatingType
from request_module.models import CourseRequest
from request_module.serializers import CourseRequestSerializer, CourseWithRequestsSerializer
from request_module.tasks import send_student_request, prepare_send_request_data
from request_module.utils import resample, get_average_array, scale_linear
from wave_module.fsm import WaveContext, WaveTypeState
from wave_module.models import Wave, WaveBunch
from wave_module.utils import get_wave_status_for_teacher, is_wave_opened_for_student


class RequestViewSet(ModelViewSet):
    queryset = CourseRequest.objects.all()
    serializer_class = CourseRequestSerializer

    @method_permission_classes((IsStudent,))
    def create(self, request, **kwargs):
        user = request.user
        course_id = request.data["course_id"]
        wave_id = request.data["wave_id"]
        priority = request.data.get("priority", None)
        current_wave = get_object_or_404(Wave, pk=wave_id)
        if is_wave_opened_for_student(current_wave):
            course = Course.objects.get(id=course_id)
            student = StudentProfile.objects.get(user=user)

            course_request = CourseRequest(course=course, student=student, wave=current_wave)

            if current_wave:
                student_course_requests_count = CourseRequest.objects.filter(
                    course__block=course.block,
                    student=student,
                    wave=current_wave
                ).count()

                if student_course_requests_count < current_wave.course_count:
                    context = WaveContext(WaveTypeState())
                    context.request_make_request_script(course_request, course, current_wave, priority)
                    if context.obj:
                        course_request = context.obj
                else:
                    raise ElectivesAPIException(
                        detail="Превышено количество заявок в блоке",
                        status_code=status.HTTP_400_BAD_REQUEST,
                    )
                course_request.save()

            # make_request_strategy = get_make_request_strategy(current_wave)
            # print(make_request_strategy)
            # context = MakeRequestContext(make_request_strategy())
            # course_request = context.make_request(current_wave, student, course, priority)

            if current_wave.requests_spreadsheet:
                send_student_request.delay(*prepare_send_request_data(student, current_wave, course, course_request))
        else:
            raise ElectivesAPIException(
                detail="Выбор курсов недоступен",
                status_code=status.HTTP_400_BAD_REQUEST,
            )

        return Response(data=self.serializer_class(course_request).data, status=status.HTTP_200_OK)

    @method_permission_classes((IsStudent,))
    def destroy(self, request, pk=None, **kwargs):
        course_id = request.query_params.get("course_id")
        wave_id = request.query_params.get("wave_id")
        user = request.user
        student = StudentProfile.objects.get(user=user)
        course = Course.objects.get(id=course_id)
        current_wave = get_object_or_404(Wave, pk=wave_id)

        if is_wave_opened_for_student(current_wave):
            super().destroy(request, pk)
            if current_wave.requests_spreadsheet:
                send_student_request.delay(*prepare_send_request_data(student, current_wave, course))
        else:
            raise ElectivesAPIException(
                detail="Выбор курсов недоступен",
                status_code=status.HTTP_400_BAD_REQUEST,
            )
        return Response(data={}, status=status.HTTP_200_OK)

    @method_permission_classes((IsTeacher,))
    def list(self, request, **kwargs):
        user = request.user
        course_id = request.query_params.get("course_id")
        wave_id = request.query_params.get("wave_id")
        if course_id and wave_id:
            wave = get_object_or_404(Wave, pk=wave_id)
            course = get_object_or_404(Course, pk=course_id)
            course_heads = course.head.all().values_list('user', flat=True)
            if user.id not in course_heads:
                raise ElectivesAPIException(
                    detail="Нет доступа к данному курсу",
                    status_code=status.HTTP_400_BAD_REQUEST,
                )
            serializer = CourseWithRequestsSerializer(course,
                                                      context={'wave_id': wave_id, 'rating_type': course.rating_type})
            return Response(data={
                'wave_id': wave.id,
                'auto_distribution_date': wave.auto_distribution_date,
                'use_rating': wave.use_rating,
                'status': get_wave_status_for_teacher(wave),
                'course': serializer.data
            }, status=status.HTTP_200_OK)
        else:
            raise ElectivesAPIException(
                detail="Неверный запрос",
                status_code=status.HTTP_400_BAD_REQUEST,
            )


class RequestAnalyticsView(APIView):
    permission_classes = (IsAuthenticated, IsDeanery)

    def get(self, request):
        block_id = request.query_params.get('block_id')
        year = request.query_params.get('year')

        # Популярность курсов

        courses = Course.objects.filter(block_id=block_id)
        course_names = courses.values_list('name', flat=True)
        values = []

        for course in courses:
            if year:
                requests = CourseRequest.objects.filter(wave__end_date__year=year, course=course)
            else:
                requests = CourseRequest.objects.filter(course=course)
            requests_count = requests.count()
            values.append(requests_count / course.students_count)

        norm_param = 10
        waves_values = []

        course_dict = dict((el, 0) for el in course_names)
        course_dict_count = dict((el, 0) for el in course_names)

        wave_bunches = WaveBunch.objects.filter(course_blocks__id__exact=block_id)
        for wave_bunch in wave_bunches:
            for wave in wave_bunch.wave_set.all():

                # Интенсивность подачи заявок

                if year:
                    requests = CourseRequest.objects.filter(wave=wave, wave__end_date__year=year,
                                                            course__block_id=block_id) \
                        .order_by('date_of_request')
                else:
                    requests = CourseRequest.objects.filter(wave=wave, course__block_id=block_id) \
                        .order_by('date_of_request')
                wave_days_count = int((wave.end_date - wave.start_date).total_seconds() // (60 * 60))
                if wave_days_count >= 100:
                    wave_values = [0 for i in range(wave_days_count)]
                    for r in requests:
                        i = int((r.date_of_request - wave.start_date).total_seconds() // (60 * 60))
                        if i < len(wave_values):
                            if wave_values[i]:
                                wave_values[i] += 1
                            else:
                                wave_values[i] = 1
                    waves_values.append(resample(wave_values, norm_param))

                # Проходной балл

                for course_name in course_names:
                    if year:
                        students = CourseRequest.objects.filter(wave=wave, wave__end_date__year=year,
                                                                course__name=course_name, status='accepted')\
                            .values_list('student_id', flat=True)
                    else:
                        students = CourseRequest.objects.filter(wave=wave, course__name=course_name, status='accepted')\
                            .values_list('student_id', flat=True)
                    student_ratings = StudentRating.objects\
                        .filter(student__id__in=students, rating_type__key_name='Средний балл')\
                        .order_by('score')
                    min_student_rating = student_ratings.first()
                    if min_student_rating:
                        course_dict[course_name] += min_student_rating.score
                        course_dict_count[course_name] += 1

        ratings_values = []
        for key in course_dict:
            if course_dict_count[key]:
                ratings_values.append(course_dict[key] / course_dict_count[key])
            else:
                ratings_values.append(0)

        return Response(data={
            'intensity': {'days': list(range(1, len(waves_values[0]) + 1)), 'values': get_average_array(waves_values)},
            'popularity': {'courses': course_names, 'values': values},
            'min_accept_score': {'courses': course_names, 'values': ratings_values}
        }, status=status.HTTP_200_OK)
