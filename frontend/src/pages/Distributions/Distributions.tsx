import React, { FC, useEffect } from 'react';

import { PageLayout } from '../../shared';
import { Aside } from '../../components/Aside';
import { useSelector } from 'react-redux';
import * as selectors from '../../redux-store/selectors';
import { UserRole } from '../../consts/enums';
import { useHistory } from 'react-router';
import { DistributionList } from '../../components/DistributionList';
import { DistributionsContent, StyledHeader } from './styles';
import { Layout } from 'antd';
import { HeaderTitle } from '../Distribution/styles';

export const Distributions: FC = () => {
    const history = useHistory();
    const user = useSelector(selectors.user.user);

    useEffect(() => {
        if (user.role !== UserRole.DEANERY) history.push('/');
    }, [user.role, history]);

    return (
        <PageLayout>
            <Aside />
            <Layout>
                <StyledHeader className="site-layout-sub-header-background" >
                    <HeaderTitle level={4}>Распределения</HeaderTitle>
                </StyledHeader>
                <DistributionsContent>
                    <DistributionList />
                </DistributionsContent>
            </Layout>
        </PageLayout>
    );
};
