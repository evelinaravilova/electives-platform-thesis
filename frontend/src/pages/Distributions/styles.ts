import styled from 'styled-components';
import { Content, Header } from 'antd/es/layout/layout';

export const DistributionsContent = styled(Content)`
    padding: 20px;
`;

export const StyledHeader = styled(Header)`
    padding: 18px;
    display: flex;
    justify-content: flex-end;
    position: relative;
`;
