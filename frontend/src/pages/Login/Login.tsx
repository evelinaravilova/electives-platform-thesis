import React, { FC, useEffect } from 'react';
import { Button, message } from 'antd';
import { PageLayout } from '../../shared';
import Title from 'antd/es/typography/Title';
import { LoginContent } from './styles';
import { useLocation } from 'react-router';

export const Login: FC = () => {
    const location = useLocation();

    const loginUrl = `https://core.uenv.ru/oauth/authorize?response_type=code&client_id=${process.env.REACT_APP_CLIENT_ID}&redirect_uri=${process.env.REACT_APP_FRONTEND_DOMAIN}/callback`;

    useEffect(() => {
        const messageText = new URLSearchParams(location.search).get('message') ?? '';
        if (messageText) {
            message.error(decodeURIComponent(messageText), 5);
        }
    }, [location.search]);

    return (
        <PageLayout>
            <LoginContent>
                <Title>Вход</Title>
                <p>Вам нужно перейти на страницу авторизации КФУ</p>
                <Button type="primary" ghost href={loginUrl}>
                    Перейти
                </Button>
            </LoginContent>
        </PageLayout>
    );
};
