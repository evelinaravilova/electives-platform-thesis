import styled from 'styled-components';
import { Content } from 'antd/es/layout/layout';
import { Alert } from 'antd';

export const LoginContent = styled(Content)`
    margin: 18% auto;
    text-align: center;
`;

export const ErrorMessage = styled(Alert)`
    display: inline-block;
`;
