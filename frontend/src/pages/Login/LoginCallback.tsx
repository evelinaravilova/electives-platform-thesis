import React, { FC, useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { ErrorMessage, LoginContent } from './styles';
import { useHistory } from 'react-router';
import { Spin } from 'antd';
import { authorize } from '../../api/auth';
import { useDispatch } from 'react-redux';
import { setCurrentUser } from '../../redux-store/userReducer/slice';
import { UserRole } from '../../consts/enums';
import { Routes } from '../../consts';

export const LoginCallback: FC = () => {
    const location = useLocation();
    const history = useHistory();
    const dispatch = useDispatch();

    const [error, setError] = useState<string>('');

    useEffect(() => {
        const code = new URLSearchParams(location.search).get('code') ?? '';
        authorize(code).then(res => {
            if (res.success) {
                dispatch(setCurrentUser(res.data));
                switch (res.data.user.role) {
                    case UserRole.STUDENT:
                        history.push(Routes.COURSES);
                        break;
                    case UserRole.TEACHER:
                        history.push(Routes.COURSES);
                        break;
                    case UserRole.DEANERY:
                        history.push(Routes.DISTRIBUTIONS);
                        break;
                    default:
                        break;
                }
            } else setError(res.message!);
        });
    }, [history, location.search, dispatch]);

    return (
        <LoginContent>
            {!error ? (
                <Spin tip="Загрузка..." size="large" />
            ) : (
                <ErrorMessage message="Возникла ошибка при авторизации" description={error} type="error" showIcon />
            )}
        </LoginContent>
    );
};
