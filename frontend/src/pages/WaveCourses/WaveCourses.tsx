import React, { FC, useEffect, useState } from 'react';

import { PageLayout } from '../../shared';
import { Aside } from '../../components/Aside';
import { useSelector } from 'react-redux';
import * as selectors from '../../redux-store/selectors';
import { UserRole } from '../../consts/enums';
import { useHistory } from 'react-router';
import { DistributionsContent, StyledHeader } from './styles';
import { Layout } from 'antd';
import { HeaderTitle } from '../Distribution/styles';
import { InfoMessage } from '../../components/InfoMessage';
import { fetchWaveCourses } from '../../api/wave';
import { WaveCoursesTable } from '../../components/WaveCoursesTable';
import { WaveBody } from '../../api/wave/types';

export const WaveCourses: FC = () => {
    const history = useHistory();
    const user = useSelector(selectors.user.user);

    const [waveCourses, setWaveCourses] = useState<WaveBody[]>([]);
    const [loading, setLoading] = useState<boolean>(true);

    useEffect(() => {
        if (user.role !== UserRole.TEACHER) history.push('/');
        fetchWaveCourses().then(result => {
            if (result.success) {
                setWaveCourses(result.data);
            }
        });
        setLoading(false);
    }, [user.role, history]);

    return (
        <PageLayout>
            <Aside />
            <Layout>
                <StyledHeader className="site-layout-sub-header-background">
                    <HeaderTitle level={4}>Волны</HeaderTitle>
                </StyledHeader>
                <DistributionsContent>
                    <InfoMessage
                        info={
                            <span>
                                Пожалуйста, введите результаты собеседований до{' '}
                                <b>даты автоматического распределения</b> в каждой волне
                                <br />
                                Доступ к вводу результатов открывается после окончания выбора у студентов
                            </span>
                        }
                    /><br/>
                    <WaveCoursesTable waveCourses={waveCourses} loading={loading}/>
                </DistributionsContent>
            </Layout>
        </PageLayout>
    );
};
