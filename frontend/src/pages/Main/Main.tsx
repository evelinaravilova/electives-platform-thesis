import React, { FC, useEffect } from 'react';
import { useHistory } from 'react-router';
import { useSelector } from 'react-redux';
import { UserRole } from '../../consts/enums';
import { Routes } from '../../consts';
import * as selectors from '../../redux-store/selectors';

export const Main: FC = () => {
    const history = useHistory();
    const user = useSelector(selectors.user.user);

    useEffect(() => {
        if (user) {
            switch (user.role) {
                case UserRole.STUDENT:
                    history.push(Routes.COURSES);
                    break;
                case UserRole.TEACHER:
                    history.push(Routes.WAVE_COURSES);
                    break;
                case UserRole.DEANERY:
                    history.push(Routes.DISTRIBUTIONS);
                    break;
                default:
                    break;
            }
        }
    }, [history, user]);

    return <div></div>;
};
