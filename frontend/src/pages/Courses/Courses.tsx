import React, { FC, useContext, useEffect, useMemo } from 'react';

import { PageLayout } from '../../shared';
import { Aside } from '../../components/Aside';
import { Content } from 'antd/es/layout/layout';
import { InfoMessage } from '../../components/InfoMessage';
import { Col, Row } from 'antd';
import { PaddedRow, RowContainer } from '../../shared/GridCell';
import { RightDetails } from '../../components/RightDetails';
import { CourseBlockList } from '../../components/CourseBlock';
import { CourseList } from '../../components/CourseList';
import { CourseDetails } from '../../components/CourseDetails';
import WaveContext from '../../components/WaveProvider/WaveContext';
import { useSelector } from 'react-redux';
import * as selectors from '../../redux-store/selectors';
import { UserRole } from '../../consts/enums';
import { useHistory } from 'react-router';
import moment from 'moment';

export const Courses: FC = () => {
    const wave = useContext(WaveContext);
    const history = useHistory();
    const user = useSelector(selectors.user.user);
    
    /*const [width, setWidth] = useState<number>(window.innerWidth);
    
    useEffect(() => {
        function handleWindowSizeChange() {
            setWidth(window.innerWidth);
        }
        window.addEventListener('resize', handleWindowSizeChange);
        return () => {
            window.removeEventListener('resize', handleWindowSizeChange);
        }
    }, []);

    const isMobile: boolean = (width <= 768);*/

    useEffect(() => {
        if (user.role !== UserRole.STUDENT) history.push("/");
    }, [user.role, history]);

    const info = useMemo<string>(() => {
        if (wave) {
            return `Выберите курсы до ${moment(wave.end_date).format('LLL')}`;
        }
        return 'На текущий момент распределение не проводится'
    }, [wave]);

    return (
        <PageLayout>
            <Aside/>
            <Content>
                <Row>
                    <Col xxl={wave ? 14 : 24} xl={wave ? 14 : 24} xs={10} sm={10}>
                        <RowContainer>
                            <PaddedRow>
                                <InfoMessage info={info}/>
                            </PaddedRow>

                            <PaddedRow>{wave && <CourseBlockList/>}</PaddedRow>
                            <PaddedRow>{wave && <CourseList/>}</PaddedRow>
                        </RowContainer>
                    </Col>
                    {wave && (
                        <Col xxl={10} xl={10} xs={14} sm={14}>
                            <RightDetails>
                                <CourseDetails
                                    waveId={wave.id}
                                    showRequestCount={wave.show_requests_count}
                                    useRating={wave.use_rating}
                                />
                            </RightDetails>
                        </Col>
                    )}
                </Row>
            </Content>
        </PageLayout>
    );
};
