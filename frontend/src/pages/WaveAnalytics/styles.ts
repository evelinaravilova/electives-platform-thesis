import styled from 'styled-components';
import { Card, Col, Row } from 'antd';
import { Content } from 'antd/es/layout/layout';

export const AnalyticsFormWrapper = styled.div`
    display: flex;
    margin-bottom: 10px;

    > div {
        margin-right: 10px;
    }
`;

export const ChartCol = styled(Col)`
    padding: 1%;
`;

export const ChartRow = styled(Row)`
    padding: 1%;
`;

export const ChartCard = styled(Card)`
    width: 100%;
    
    .ant-card-head {
        border-bottom: none;
    }
`;

export const AnalyticsContent = styled(Content)`
    padding: 20px;
    overflow-y: auto;
    height: 90vh;
`;
