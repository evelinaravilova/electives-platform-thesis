import React, { FC, useCallback, useEffect, useMemo, useState } from 'react';

import { PageLayout } from '../../shared';
import { Aside } from '../../components/Aside';
import { useDispatch, useSelector } from 'react-redux';
import * as selectors from '../../redux-store/selectors';
import { DegreeName, UserRole } from '../../consts/enums';
import { useHistory } from 'react-router';
import { Button, DatePicker, Form, Layout, Row, Select } from 'antd';
import { HeaderTitle } from '../Distribution/styles';
import { CoursePopularityChart } from '../../components/Analytics';
import { StyledHeader } from '../Distributions/styles';
import { AnalyticsContent, AnalyticsFormWrapper, ChartCard, ChartCol, ChartRow } from './styles';
import { loadCourseBlocks } from '../../redux-store/courseReducer/thunks';
import { fetchPopularCourses } from '../../api/request';
import { Moment } from 'moment';
import { PopularCourses } from '../../api/request/types';
import { RequestsIntensityChart } from '../../components/Analytics/RequestsIntensityChart';
import { RatingChart } from '../../components/Analytics/RatingChart';

export const WaveAnalytics: FC = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const user = useSelector(selectors.user.user);

    const courseBlocks = useSelector(selectors.courseBlock.blocks);
    const courseBlocksLoading = useSelector(selectors.courseBlock.loading);

    const [popularityChartData, setPopularityChartData] = useState<PopularCourses | null>(null);

    useEffect(() => {
        if (user.role !== UserRole.DEANERY) history.push('/');
        loadCourseBlocks(dispatch);
    }, [user.role, history, dispatch]);

    const options = useMemo(
        () =>
            courseBlocks.map((courseBlock) => ({
                value: courseBlock.id.toString(),
                label: `${courseBlock.type} блок - ${courseBlock.semester} семестр - ${DegreeName[courseBlock.degree]}`,
            })),
        [courseBlocks],
    );

    const onFormFinish = useCallback(async (values: { block_id: number; year?: Moment }) => {
        const result = await fetchPopularCourses(values.block_id, values.year?.year());
        if (result.success) {
            setPopularityChartData(result.data);
        }
    }, []);

    return (
        <PageLayout>
            <Aside />
            <Layout>
                <StyledHeader className="site-layout-sub-header-background">
                    <HeaderTitle level={4}>Аналитика</HeaderTitle>
                </StyledHeader>
                <AnalyticsContent>
                    <ChartRow>
                        <Form onFinish={onFormFinish}>
                            <AnalyticsFormWrapper>
                                <Form.Item
                                    label="Блок курсов"
                                    name="block_id"
                                    rules={[{ required: true, message: 'Пожалуйста, выберите блок курсов' }]}
                                >
                                    <Select
                                        placeholder="Выберите блок курсов"
                                        loading={courseBlocksLoading}
                                        options={options}
                                        style={{ width: 300 }}
                                    />
                                </Form.Item>

                                <Form.Item label="Год" name="year">
                                    <DatePicker picker="year" />
                                </Form.Item>

                                <Button htmlType="submit">Показать</Button>
                            </AnalyticsFormWrapper>
                        </Form>
                    </ChartRow>
                    <ChartRow>
                        <ChartCard title="Интенсивность подачи заявок">
                            {popularityChartData && <RequestsIntensityChart data={popularityChartData.intensity} />}
                        </ChartCard>
                    </ChartRow>
                    <Row>
                        <ChartCol span={10}>
                            <ChartCard title="Популярность курсов">
                                {popularityChartData && <CoursePopularityChart data={popularityChartData.popularity} />}
                            </ChartCard>
                        </ChartCol>
                        <ChartCol span={14}>
                            <ChartCard title="Проходной балл">
                                {popularityChartData && <RatingChart data={popularityChartData.min_accept_score} />}
                            </ChartCard>
                        </ChartCol>
                    </Row>
                </AnalyticsContent>
            </Layout>
        </PageLayout>
    );
};
