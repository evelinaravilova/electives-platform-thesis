import React, { FC, useEffect, useMemo, useState } from 'react';
import { useHistory, useRouteMatch } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import * as selectors from '../../redux-store/selectors';
import { DegreeName, UserRole } from '../../consts/enums';
import { PageLayout } from '../../shared';
import { Aside } from '../../components/Aside';
import { Button, Col, Divider, Layout, Popover } from 'antd';
import { StyledHeader } from '../Distributions/styles';
import Title from 'antd/es/typography/Title';
import { loadDistribution } from '../../redux-store/waveReducer/thunks';
import { DistributionStatusLabel } from '../../shared/DistributionStatusLabel';
import { DistributionContent, EditButton, ExpandCourseBlocksButton, FullRow, HeaderTitle } from './styles';
import { WaveBunch } from '../../components/WaveBunch';
import { ConfigurationRightSlider } from '../../components/ConfigurationRightSlider';

export const Distribution: FC = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { distribution_id: distributionId } = useRouteMatch<{ distribution_id: string }>().params;
    const user = useSelector(selectors.user.user);
    const waveBunch = useSelector(selectors.waveBunch.waveBunch);
    const [configVisible, setConfigVisible] = useState(false);

    useEffect(() => {
        if (user.role !== UserRole.DEANERY) history.push('/');
        loadDistribution(dispatch, Number(distributionId));
    }, [user.role, history, distributionId, dispatch]);

    const headerTitle = useMemo(
        () =>
            waveBunch.id !== 0 ? (
                <HeaderTitle level={4}>
                    Распределение{' '}
                    {`${waveBunch.semesters.length ? 'на ' + waveBunch.semesters.join(', ') + ' сем.' : ''}
                            для ${DegreeName[waveBunch.degree].slice(0, 3).toLowerCase()}. 
                            ${waveBunch.current_course} к.`}{' '}
                    <Popover
                        placement="bottomRight"
                        content={
                            <div>
                                <Title level={5}>Блоки курсов</Title>
                                {waveBunch.course_blocks.map((courseBlock) => (
                                    <div key={courseBlock.id}>
                                        {courseBlock.type} {courseBlock.semester} семестр
                                    </div>
                                ))}
                                <Divider/>
                                <Title level={5}>Направления</Title>
                                {waveBunch.programs.map((program) => (
                                    <div key={program.id}>
                                        {program.name}
                                    </div>
                                ))}
                            </div>
                        }
                    >
                        <ExpandCourseBlocksButton />
                    </Popover>
                    <DistributionStatusLabel status={waveBunch.status} />
                </HeaderTitle>
            ) : (
                <Title level={4}>Распределение ...</Title>
            ),
        [waveBunch],
    );

    return (
        <PageLayout>
            <Aside />
            <Layout>
                <StyledHeader className="site-layout-sub-header-background">
                    {headerTitle}
                    <EditButton>Редактировать</EditButton>
                    <Button danger>Удалить</Button>
                </StyledHeader>
                <DistributionContent style={{ maxHeight: window.innerHeight }}>
                    <FullRow>
                        <Col span={configVisible ? 16 : 24}>
                            <WaveBunch
                                waves={waveBunch.waves}
                                configVisible={configVisible}
                                setConfigVisible={setConfigVisible}
                            />
                        </Col>
                        <ConfigurationRightSlider visible={configVisible} />
                    </FullRow>
                </DistributionContent>
            </Layout>
        </PageLayout>
    );
};
