import styled from 'styled-components';
import Title from 'antd/es/typography/Title';
import { Content } from 'antd/es/layout/layout';
import { Button, Row } from 'antd';
import { DownCircleOutlined } from '@ant-design/icons/lib';

export const HeaderTitle = styled(Title)`
    position: absolute;
    left: 20px;
`;

export const DistributionContent = styled(Content)`
    display: flex;
    position: relative;
`;

export const EditButton = styled(Button)`
    margin-right: 10px;
`;

export const FullRow = styled(Row)`
    width: 100%; 
    height: 100%;
`;


export const ExpandCourseBlocksButton = styled(DownCircleOutlined)`
    font-size: 16px;
    color: #8e8d8d;
    cursor: pointer;
    margin: 0 14px 0 8px;
    
    &:hover {
        color: #7d7d7d;
    }
`;

