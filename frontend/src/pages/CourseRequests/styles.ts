import styled from 'styled-components';
import { Content } from 'antd/es/layout/layout';

export const CourseRequestsContent = styled(Content)`
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 20px;
`;
