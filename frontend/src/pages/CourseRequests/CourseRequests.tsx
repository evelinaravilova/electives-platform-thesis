import React, { FC, useEffect, useMemo, useState } from 'react';

import { PageLayout } from '../../shared';
import { Aside } from '../../components/Aside';
import { useSelector } from 'react-redux';
import * as selectors from '../../redux-store/selectors';
import { DistributionStatus, RatingMode, UserRole } from '../../consts/enums';
import { useHistory } from 'react-router';
import { Layout } from 'antd';
import { HeaderTitle } from '../Distribution/styles';
import { InfoMessage } from '../../components/InfoMessage';
import { StyledHeader } from '../Distributions/styles';
import { DistributionStatusLabel } from '../../shared/DistributionStatusLabel';
import { fetchCourseRequests } from '../../api/request';
import { Routes } from '../../consts';
import { CourseRequests as CourseRequestsType } from '../../api/request/types';
import { CourseRequestsForm } from '../../components/CourseRequestsForm';
import { CourseRequestsContent } from './styles';
import moment from 'moment';

export const CourseRequests: FC = () => {
    const history = useHistory();
    const user = useSelector(selectors.user.user);

    const [courseRequests, setCourseRequests] = useState<CourseRequestsType | null>(null);
    const [loading, setLoading] = useState<boolean>(true);

    useEffect(() => {
        if (user.role !== UserRole.TEACHER) history.push('/');
        const params = new URLSearchParams(history.location.search);
        const courseId = params.get('course_id') ?? '';
        const waveId = params.get('wave_id') ?? '';
        if (courseId && waveId) {
            fetchCourseRequests(Number(waveId), Number(courseId)).then((result) => {
                if (result.success) {
                    setCourseRequests(result.data);
                }
            });
            setLoading(false);
        } else history.push(Routes.WAVE_COURSES);
    }, [user.role, history]);

    const headerTitle = useMemo(
        () =>
            courseRequests ? (
                <HeaderTitle level={4}>
                    Заявки на курс "{courseRequests.course.name}"
                    {courseRequests.course.students_count ? `, квота ${courseRequests.course.students_count}` : ''}
                    &nbsp;&nbsp;
                    <DistributionStatusLabel status={courseRequests.status} />
                </HeaderTitle>
            ) : (
                <HeaderTitle level={4}>Заявки ...</HeaderTitle>
            ),
        [courseRequests],
    );

    return (
        <PageLayout>
            <Aside />
            <Layout>
                <StyledHeader className="site-layout-sub-header-background">{headerTitle}</StyledHeader>
                <CourseRequestsContent>
                    {courseRequests && courseRequests.use_rating && courseRequests.course.rating_type.mode === RatingMode.Interview && <InfoMessage
                        info={
                            courseRequests.status === DistributionStatus.Active ? <span>
                                Пожалуйста, проранжируйте список до{' '}
                                <b>{moment(courseRequests.auto_distribution_date).format('LLL')}</b>
                                <br/>
                                Значения могут быть только уникальными
                            </span> : <span>Доступ к редактированию закрыт</span>
                        }
                        style={{ maxWidth: 1000 }}
                    />}<br/>
                    <CourseRequestsForm
                        requests={courseRequests ? courseRequests.course.requests : []}
                        loading={loading}
                        autoDistributionDate={courseRequests ? courseRequests.auto_distribution_date : ''}
                        ratingType={courseRequests?.course.rating_type}
                        waveId={courseRequests?.wave_id}
                        courseId={courseRequests?.course.id}
                        useRating={courseRequests?.use_rating}
                    />
                </CourseRequestsContent>
            </Layout>
        </PageLayout>
    );
};
