import styled from 'styled-components';
import { Row } from 'antd';

export const WaveResultListContainer = styled(Row)`
    overflow-y: auto;
    width: 100%;
    display: flex;
    flex-direction: column;
    @media only screen and (min-device-width: 1000px) {
        padding-left: 15%;
    }
    @media only screen and (max-device-width: 1000px) {
        padding-left: 2%;
    }
`;