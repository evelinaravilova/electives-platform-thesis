import React, { FC, useEffect, useMemo, useState } from 'react';
import { Aside } from '../../components/Aside';
import { Content } from 'antd/es/layout/layout';
import { PageLayout } from '../../shared';
import { Menu } from 'antd';
import { fetchWaveResults } from '../../api/wave';
import { WavesResult } from '../../api/wave/types';
import { UserRole } from '../../consts/enums';
import { useHistory } from 'react-router';
import { useSelector } from 'react-redux';
import * as selectors from '../../redux-store/selectors';
import { WaveResultList } from '../../components/WaveResults';
import { WaveResultListContainer } from './styles';

export const StudentResults: FC = () => {
    const history = useHistory();
    const user = useSelector(selectors.user.user);

    const [activeOption, setActiveOption] = useState('0');
    const [wavesResults, setWavesResults] = useState<WavesResult | null>(null);
    const [loading, setLoading] = useState<boolean>(true);

    useEffect(() => {
        if (user.role !== UserRole.STUDENT) history.push('/');
        fetchWaveResults().then((result) => {
            if (result.success) {
                setWavesResults(result.data);
                const currentCourse = result.data.current_course.toString();
                const courses = Object.keys(result.data.results);
                if (courses.includes(currentCourse)) {
                    setActiveOption(currentCourse);
                } else setActiveOption(courses[0]);
            }
        });
        setLoading(false);
    }, [history, user.role]);

    const courseMenuItems = useMemo(
        () =>
            wavesResults &&
            Object.keys(wavesResults.results).map((course_number) => (
                <Menu.Item key={course_number}>{course_number} курс</Menu.Item>
            )),
        [wavesResults],
    );

    const handleCourseClick = ({ key }: any) => {
        setActiveOption(key);
    };

    return (
        <PageLayout>
            <Aside />
            <Content>
                <Menu onClick={handleCourseClick} selectedKeys={[activeOption]} mode="horizontal">
                    {courseMenuItems}
                </Menu>
                <WaveResultListContainer>
                    {wavesResults && (
                        <WaveResultList
                            wavesResults={
                                wavesResults.results[(activeOption as unknown) as keyof typeof wavesResults.results]!
                            }
                        />
                    )}
                </WaveResultListContainer>
            </Content>
        </PageLayout>
    );
};
