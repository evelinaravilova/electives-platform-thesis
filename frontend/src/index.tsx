import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import 'antd/dist/antd.css';
import './index.css';
import { theme } from './styles';
import { ThemeProvider } from 'styled-components';
import App from './App';
import store from './redux-store/store';
import { GlobalStyle } from './styles/globalStyle';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

ReactDOM.render(
    <>
        <DndProvider backend={HTML5Backend}>
            <Provider store={store}>
                <ThemeProvider theme={theme}>
                    <App />
                    <GlobalStyle />
                </ThemeProvider>
            </Provider>
        </DndProvider>
    </>,
    document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
