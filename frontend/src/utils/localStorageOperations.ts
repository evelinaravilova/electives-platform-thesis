import { JwtNames } from '../consts';
import { CurrentUser } from '../api/auth/types';

export const addJwt = (accessToken: string): void => {
    localStorage.setItem(JwtNames.ACCESS, accessToken);
};

export const addCurrentUser = (data: CurrentUser): void => {
    localStorage.setItem(JwtNames.CURRENT_USER, JSON.stringify(data));
};

export const removeJwt = (): void => {
    localStorage.removeItem(JwtNames.ACCESS);
};

export const removeCurrentUser = (): void => {
    localStorage.removeItem(JwtNames.CURRENT_USER);
};

export const getCurrentUser = (): CurrentUser | null => {
    const userString = localStorage.getItem(JwtNames.CURRENT_USER);
    if (userString) return JSON.parse(userString);
    return null;
};

export const getJwt = (): string => {
    return localStorage.getItem(JwtNames.ACCESS) ?? '';
};
