import { ErrorResponseData, ResponseData } from '../api/types';
import { AxiosResponse } from 'axios';
import { message } from 'antd';
import { removeCurrentUser, removeJwt } from './localStorageOperations';
import { Routes } from '../consts';

export async function doRequest<TData>(
    request: () => Promise<AxiosResponse<any>>,
): Promise<ResponseData<TData>> {
    try {
        const result = await request();

        if (result.status === 204) return { success: true, code: 204, data: {} as TData };

        return result.data;
    } catch (error) {
        const data = error.response?.data;
        if (isUnauthorized(error.response?.status)) {
            redirectToLogin(data?.message);
            message.error(data?.message ?? 'Неизвестная ошибка', 5);
            return { success: false, data: null } as ErrorResponseData;
        } else {
            message.error(data?.message ?? 'Неизвестная ошибка', 5);
            return data as ErrorResponseData;
        }
    }
}

export const isUnauthorized = (status: number): boolean => status === 401;

export const redirectToLogin = (message: string): void => {
    removeJwt();
    removeCurrentUser();
    window.location.replace(Routes.LOGIN + message ? `?message=${encodeURIComponent(message)}` : '');
};
