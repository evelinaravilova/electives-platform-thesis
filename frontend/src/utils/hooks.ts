import { useRef } from 'react';

// eslint-disable-next-line @typescript-eslint/ban-types
export const useComponentWillMount = (func: Function): void => {
    const willMount = useRef(true);

    if (willMount.current) func();

    willMount.current = false
};
