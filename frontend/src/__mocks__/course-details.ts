export const mockData = {
    id: 1,
    name: 'Введение в веб-программирование Django',
    student_quota: 20,
    hours: 118,
    description: `- Учимся кодить на Python
- Создаем веб-приложения с помощью Django
- Затрагиваем JavaScript
- Развиваемся как разработчики в целом: работа в команде, понимания жизненного цикла разработки приложения, контроль версий, тестирование приложений.
-egwegergrg
-rrgergerg
-rgergerg`,
    requirements:
        'Предварительных требований нет, но если будет большое количество желающих, будет проведен отбор в виде решения пары задач.',
    teachers: [
        {
            id: 1,
            first_name: 'Михаил',
            middle_name: 'Михайлович',
            last_name: 'Абрамский',
        },
        {
            id: 2,
            first_name: 'Михаил',
            middle_name: 'Михайлович',
            last_name: 'Абрамский',
        },
        {
            id: 3,
            first_name: 'Михаил',
            middle_name: 'Михайлович',
            last_name: 'Абрамский',
        },
        {
            id: 4,
            first_name: 'Михаил',
            middle_name: 'Михайлович',
            last_name: 'Абрамский',
        },
    ],
    cover: "",
    tags: [
        { name: 'Python', type: 'skill' },
        { name: 'Django', type: 'skill' },
        { name: 'WebLab', type: 'lab' },
        { name: 'Smart Education Lab', type: 'lab' },
    ],
    requested: false,
};
