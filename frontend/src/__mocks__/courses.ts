export const mockData = [
    {
        id: 1,
        name: 'Введение в веб-программирование Django',
        teacher: 'Абрамский Михаил Михайлович',
        status: 0,
        cover: '',
        priority: 1,
    },
    {
        id: 2,
        name: 'Разработка бизнес-решений на платформе .NET',
        teacher: 'Абрамский Михаил Михайлович',
        status: 0,
        cover: '',
        priority: 2,
    },
    {
        id: 3,
        name: 'Введение в разработку корпоративных приложений Java (Java Lab)',
        teacher: 'Абрамский Михаил Михайлович',
        status: 1,
        cover: '',
        priority: 3,
    },
    {
        id: 4,
        name: 'Введение в разработку корпоративных приложений Java (Java Lab)',
        teacher: 'Абрамский Михаил Михайлович',
        status: null,
        cover: '',
    },
    {
        id: 5,
        name: 'Введение в разработку корпоративных приложений Java (Java Lab)',
        teacher: 'Абрамский Михаил Михайлович',
        status: null,
        cover: '',
    },
    {
        id: 6,
        name: 'Введение в разработку корпоративных приложений Java (Java Lab)',
        teacher: 'Абрамский Михаил Михайлович',
        status: null,
        cover: '',
    },
    /*  {
          id: 7,
          name: 'Введение в разработку корпоративных приложений Java (Java Lab)',
          teacher: 'Абрамский Михаил Михайлович',
          status: null,
          cover: cover1,
      },
      {
          id: 8,
          name: 'Введение в разработку корпоративных приложений Java (Java Lab)',
          teacher: 'Абрамский Михаил Михайлович',
          status: null,
          cover: cover1,
      },
      {
          id: 9,
          name: 'Введение в разработку корпоративных приложений Java (Java Lab)',
          teacher: 'Абрамский Михаил Михайлович',
          status: null,
          cover: cover1,
      },*/
];
