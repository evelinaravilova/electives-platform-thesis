export const mockData = [
    {
        id: 1,
        type: 'Технологический',
        semester: 2,
        course_count: 3,
        request_count: 2,
    },
    {
        id: 2,
        type: 'Технологический',
        semester: 3,
        course_count: 3,
        request_count: 1,
    },
    {
        id: 3,
        type: 'Научный',
        semester: 2,
        course_count: 3,
        request_count: 2,
    },
    {
        id: 4,
        type: 'Научный',
        semester: 3,
        course_count: 3,
        request_count: 0,
    },
    {
        id: 5,
        type: 'Гуманитарный',
        semester: 2,
        course_count: 3,
        request_count: 3,
    },
];
