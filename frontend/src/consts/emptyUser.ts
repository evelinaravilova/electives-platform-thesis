import { CurrentUser } from '../api/auth/types';

export const emptyUser: CurrentUser = {
    id: 0,
    first_name: '',
    last_name: '',
    middle_name: '',
    email: '',
    role: '',
    photo: '',
};
