export enum Routes {
    ROOT = '/',
    LOGIN = '/login',
    CALLBACK = '/callback',
    COURSES = '/courses',
    DISTRIBUTIONS = '/distributions',
    DISTRIBUTION = `/distributions/:distribution_id(\\d+)`,
    WAVE_COURSES = `/wave_courses`,
    COURSE_REQUESTS = '/course_requests',
    STUDENT_RESULTS = '/results',
    ANALYTICS = '/analytics',
}
