import { WaveType } from './enums';

export const emptyWave = {
    wave_number: 0,
    start_date: '1970-01-01T00:00:00Z',
    end_date: '1970-01-01T00:00:00Z',
    type: WaveType.Selection,
    use_rating: false,
    rating_range: [0, 100],
    course_count: 1,
    has_priority: false,
    can_participate: false,
    use_previous_requests: false,
    forbid_overflow: false,
    show_requests_count: false,
    show_result: false,
    auto_distribution_date: '1970-01-01T00:00:00Z',
    auto_accept: false,
};
