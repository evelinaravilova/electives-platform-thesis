export enum JwtNames {
    ACCESS = 'electives-platform/access-token',
    CURRENT_USER = 'electives-platform/currentUser',
}
