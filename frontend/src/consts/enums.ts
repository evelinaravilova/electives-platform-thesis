export enum UserRole {
    STUDENT = 'student',
    TEACHER = 'teacher',
    DEANERY = 'admin'
}

export enum RatingMode {
    Score = 'score',
    Interview = 'interview',
}

export enum RatingModeName {
    "score" = "Балл",
    "interview" = "Собеседование"
}

export enum TagType {
    Skill = 'skill',
    Lab = 'lab',
}

export enum Degree {
    Bachelor = 'bachelor',
    Master = 'magister',
}

export enum DegreeName {
    'bachelor' = 'Бакалавриат',
    'magister' = 'Магистратура',
}

export enum RequestStatus {
    Submitted = 'submitted',
    Accepted = 'accepted',
    Rejected = 'rejected'
}

export enum RequestStatusName {
    'submitted' = 'На рассмотрении',
    'accepted' = 'Принята',
    'rejected' = 'Отклонена'
}

export enum DistributionStatus {
    Waiting = 'waiting',
    Active = 'active',
    Finished = 'finished',
}

export enum DistributionStatusName {
    'waiting' = 'Ожидание',
    'active' = 'Активно',
    'finished' = 'Завершено',
}

export enum WaveType {
    Selection = 'selection',
    Algorithm = 'algorithm'
}

export enum WaveTypeName {
    'selection' = 'Выбор',
    'algorithm' = 'Алгоритм'
}

