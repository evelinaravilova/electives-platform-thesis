import { Degree, DistributionStatus } from './enums';

export const emptyWaveBunch = {
    id: 0,
    current_course: 0,
    degree: Degree.Bachelor,
    start_date: null,
    end_date: null,
    programs: [],
    semesters: [],
    status: DistributionStatus.Waiting,
    waves: [],
    course_blocks: [],
};
