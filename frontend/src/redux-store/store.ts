import { configureStore } from '@reduxjs/toolkit';

import { rootReducer } from '.';

const store = configureStore({
    reducer: rootReducer,
    devTools: process.env.NODE_ENV !== 'production',
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    middleware: (getDefaultMiddleware: any) => getDefaultMiddleware(),
});

export default store;
