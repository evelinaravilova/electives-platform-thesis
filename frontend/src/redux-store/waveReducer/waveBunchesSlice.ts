import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { WaveBunch } from '../../api/wave/types';

export type WaveBunchState = {
    loading: boolean;
    waveBunches: WaveBunch[];
};

const waveBunchInitialState: WaveBunchState = {
    loading: false,
    waveBunches: [],
};

export const waveBunchesSlice = createSlice({
    name: 'waveBunches',
    initialState: waveBunchInitialState,
    reducers: {
        setWaveBunchesInProgress: (state, action: PayloadAction<boolean>) => {
            state.loading = action.payload;
        },
        setWaveBunches: (state, action: PayloadAction<WaveBunch[]>) => {
            state.waveBunches = action.payload;
        },
    },
});

export const { setWaveBunchesInProgress, setWaveBunches } = waveBunchesSlice.actions;
export default waveBunchesSlice.reducer;
