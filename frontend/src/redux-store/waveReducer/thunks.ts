import { Dispatch } from '@reduxjs/toolkit';
import { setWaveBunches, setWaveBunchesInProgress } from './waveBunchesSlice';
import { setWaveBunch, setWaveBunchInProgress } from './waveBunchSlice';
import { fetchDistribution, fetchDistributions } from '../../api/wave';

export async function loadDistributions(dispatch: Dispatch): Promise<void> {
    dispatch(setWaveBunchesInProgress(true));
    try {
        const response = await fetchDistributions();
        if (response.success) {
            dispatch(setWaveBunches(response.data));
        }
    } finally {
        dispatch(setWaveBunchesInProgress(false));
    }
}

export async function loadDistribution(dispatch: Dispatch, id: number): Promise<void> {
    dispatch(setWaveBunchInProgress(true));
    try {
        const response = await fetchDistribution(id);
        if (response.success) {
            dispatch(setWaveBunch(response.data));
        }
    } finally {
        dispatch(setWaveBunchInProgress(false));
    }
}
