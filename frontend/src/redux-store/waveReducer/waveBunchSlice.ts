import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { WaveBunchDetail } from '../../api/wave/types';
import { emptyWaveBunch } from '../../consts/emptyWaveBunch';

export type WaveBunchState = {
    loading: boolean;
    waveBunch: WaveBunchDetail;
    activeWave: number;
};

const waveBunchInitialState: WaveBunchState = {
    loading: false,
    waveBunch: emptyWaveBunch,
    activeWave: 0,
};

export const waveBunchSlice = createSlice({
    name: 'waveBunch',
    initialState: waveBunchInitialState,
    reducers: {
        setWaveBunchInProgress: (state, action: PayloadAction<boolean>) => {
            state.loading = action.payload;
        },
        setWaveBunch: (state, action: PayloadAction<WaveBunchDetail>) => {
            const payload = action.payload.waves;
            if (payload.length) {
                const hasActiveId = payload.some((wave) => wave.id === state.activeWave);

                if (state.waveBunch.waves.length === 0 || !hasActiveId)
                    state.activeWave = payload[0].id;
            }
            state.waveBunch = action.payload;
        },
        setActiveWave: (state, action: PayloadAction<number>) => {
            state.activeWave = action.payload;
        },
    },
});

export const { setWaveBunchInProgress, setWaveBunch, setActiveWave } = waveBunchSlice.actions;
export default waveBunchSlice.reducer;
