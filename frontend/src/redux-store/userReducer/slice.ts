import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { getCurrentUser, getJwt } from '../../utils';
import { CurrentUser } from '../../api/auth/types';
import { emptyUser } from '../../consts/emptyUser';

const token = getJwt();
const user = getCurrentUser();

export type UserInfoState = {
    user: CurrentUser;
    accessToken: string;
    isAuthenticated: boolean;
};

export const userInitialState: UserInfoState = {
    user: user || emptyUser,
    accessToken: token || '',
    isAuthenticated: !!token,
};


export const userSlice = createSlice({
    name: 'user',
    initialState: userInitialState,
    reducers: {
        setCurrentUser: (state, action: PayloadAction<{ token: string, user: CurrentUser}>) => {
            state.user = action.payload.user;
            state.accessToken = action.payload.token;
            state.isAuthenticated = true;
        },
    },
});

export const { setCurrentUser } = userSlice.actions;
export default userSlice.reducer
