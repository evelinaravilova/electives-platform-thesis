import { RootState } from './RootState';
import { CurrentUser } from '../api/auth/types';
import { Course, CourseBlock } from '../api/courses/types';
import { WaveBunch, WaveBunchDetail } from '../api/wave/types';

export const user = {
    user: (state: RootState): CurrentUser => state.userReducer.user,
    isAuthenticated: (state: RootState): boolean => state.userReducer.isAuthenticated,
};

export const courseBlock = {
    blocks: (state: RootState): CourseBlock[] => state.courseBlockReducer.courseBlocks,
    activeBlock: (state: RootState): number => state.courseBlockReducer.activeBlock,
    loading: (state: RootState): boolean => state.courseBlockReducer.loading,
};

export const course = {
    courses: (state: RootState): Course[] => state.courseReducer.courses,
    activeCourse: (state: RootState): number => state.courseReducer.activeCourse,
    loading: (state: RootState): boolean => state.courseReducer.loading,
};

export const waveBunches = {
    waveBunches: (state: RootState): WaveBunch[] => state.waveBunchesReducer.waveBunches,
    loading: (state: RootState): boolean => state.waveBunchesReducer.loading,
};

export const waveBunch = {
    waveBunch: (state: RootState): WaveBunchDetail => state.waveBunchReducer.waveBunch,
    loading: (state: RootState): boolean => state.waveBunchReducer.loading,
    activeWave: (state: RootState): number => state.waveBunchReducer.activeWave,
};
