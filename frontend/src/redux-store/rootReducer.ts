import { combineReducers } from '@reduxjs/toolkit';

import userReducer from './userReducer/slice';
import courseBlockReducer from './courseReducer/courseBlockSlice';
import courseReducer from './courseReducer/courseSlice';
import waveBunchesReducer from './waveReducer/waveBunchesSlice';
import waveBunchReducer from './waveReducer/waveBunchSlice';

export const rootReducer = combineReducers({
    userReducer,
    courseBlockReducer,
    courseReducer,
    waveBunchesReducer,
    waveBunchReducer,
});
