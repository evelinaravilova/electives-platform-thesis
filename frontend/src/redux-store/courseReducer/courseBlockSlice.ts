import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { CourseBlock } from '../../api/courses/types';

export type CourseBlockState = {
    courseBlocks: CourseBlock[];
    activeBlock: number;
    loading: boolean;
};

export const courseBlockInitialState: CourseBlockState = {
    courseBlocks: [],
    activeBlock: 0,
    loading: false,
};


export const courseBlockSlice = createSlice({
    name: 'courseBlock',
    initialState: courseBlockInitialState,
    reducers: {
        setCourseBlocksInProgress: (state, action: PayloadAction<boolean>) => {
            state.loading = action.payload;
        },
        setCourseBlocks: (state, action: PayloadAction<CourseBlock[]>) => {
            const payload = action.payload;
            if (state.courseBlocks.length === 0 && payload.length)
                state.activeBlock = payload[0].id;
            state.courseBlocks = payload;
        },
        setActiveBlock: (state, action: PayloadAction<number>) => {
            state.activeBlock = action.payload;
        },
        incrementBlockRequestsCount: (state) => {
            const i = state.courseBlocks.findIndex((courseBlock) => courseBlock.id === state.activeBlock);
            state.courseBlocks[i].requests_count++;
        },
        decrementBlockRequestsCount: (state) => {
            const i = state.courseBlocks.findIndex((courseBlock) => courseBlock.id === state.activeBlock);
            state.courseBlocks[i].requests_count--;
        },
    },
});

export const { setCourseBlocks, setActiveBlock, setCourseBlocksInProgress, incrementBlockRequestsCount, decrementBlockRequestsCount } = courseBlockSlice.actions;
export default courseBlockSlice.reducer;
