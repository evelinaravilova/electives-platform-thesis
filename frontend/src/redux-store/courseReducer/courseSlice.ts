import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Course } from '../../api/courses/types';

export type CourseState = {
    loading: boolean;
    courses: Course[];
    activeCourse: number;
};

export const courseBlockInitialState: CourseState = {
    loading: false,
    courses: [],
    activeCourse: 0,
};


export const courseSlice = createSlice({
    name: 'courseBlock',
    initialState: courseBlockInitialState,
    reducers: {
        setCoursesInProgress: (state, action: PayloadAction<boolean>) => {
            state.loading = action.payload;
        },
        setCourses: (state, action: PayloadAction<Course[]>) => {
            const payload = action.payload;
            if (payload.length) {
                const hasActiveId = payload.some((course) => course.id === state.activeCourse);

                if (state.courses.length === 0 || !hasActiveId)
                    state.activeCourse = payload[0].id;
            }
            state.courses = payload;
        },
        setActiveCourse: (state, action: PayloadAction<number>) => {
            state.activeCourse = action.payload;
        },
    },
});

export const { setCourses, setActiveCourse, setCoursesInProgress } = courseSlice.actions;
export default courseSlice.reducer;
