import { Dispatch } from '@reduxjs/toolkit';
import { setCourseBlocksInProgress, setCourseBlocks } from './courseBlockSlice';
import { setCoursesInProgress, setCourses } from './courseSlice';
import { fetchCourseBlocks, fetchCourses } from '../../api/courses';

export async function loadCourseBlocks(dispatch: Dispatch): Promise<void> {
    dispatch(setCourseBlocksInProgress(true));
    try {
        const response = await fetchCourseBlocks();
        if (response.success) {
            dispatch(setCourseBlocks(response.data));
        }
    } finally {
        dispatch(setCourseBlocksInProgress(false));
    }
}

export async function loadCourses(dispatch: Dispatch, blockId: number): Promise<void> {
    dispatch(setCoursesInProgress(true));
    try {
        const response = await fetchCourses(blockId);
        if (response.success) {
            dispatch(setCourses(response.data));
        }
    } finally {
        dispatch(setCoursesInProgress(false));
    }
}
