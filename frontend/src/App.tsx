import React, { FC } from 'react';
import { BrowserRouter as Router, Switch, Redirect, Route } from 'react-router-dom';

import { Routes as R } from './consts';
import { PrivateRoute } from './components/CustomRoute';
import { Login } from './pages/Login';
import { Courses } from './pages/Courses';
import { LoginCallback } from './pages/Login/LoginCallback';
import { WaveProvider } from './components/WaveProvider/WaveProvider';
import { Distributions } from './pages/Distributions';
import { Main } from './pages/Main';
import { Distribution } from './pages/Distribution';
import { WaveCourses } from './pages/WaveCourses';
import { CourseRequests } from './pages/CourseRequests';
import { StudentResults } from './pages/StudentResults';
import { WaveAnalytics } from './pages/WaveAnalytics';

const App: FC = () => {

    return (
        <Router>
            <Switch>
                <Route exact path={R.LOGIN} component={Login} />
                <Route path={R.CALLBACK} component={LoginCallback}/>
                <PrivateRoute exact path={R.ROOT} component={Main} />
                <PrivateRoute exact path={R.DISTRIBUTIONS} component={Distributions} />
                <PrivateRoute path={R.DISTRIBUTION} component={Distribution} />
                <PrivateRoute path={R.WAVE_COURSES} component={WaveCourses} />
                <PrivateRoute path={R.COURSE_REQUESTS} component={CourseRequests} />
                <PrivateRoute path={R.ANALYTICS} component={WaveAnalytics} />
                <WaveProvider>
                    <PrivateRoute exact path={R.COURSES} component={Courses} />
                    <PrivateRoute exact path={R.STUDENT_RESULTS} component={StudentResults} />
                </WaveProvider>
                <Redirect to={R.LOGIN} />
            </Switch>
        </Router>
    );
};

export default App;
