import { TeacherProfile } from '../institute/types';
import { Degree, RatingMode, TagType } from '../../consts/enums';
import { StudentRequest } from '../request/types';

export type CourseBlock = {
    id: number;
    type: string;
    semester: number;
    degree: Degree;
    requests_count: number;
};

export type RatingType = {
    id: number;
    key_name: string;
    mode: RatingMode;
};

export type CourseTag = {
    id: number;
    name: string;
    type: TagType;
};

export type Course = {
    id: number;
    name: string;
    description: string;
    requirements: number;
    students_count: number;
    logo: string;
    head: TeacherProfile[];
    rating_type: RatingType;
    course_tag: CourseTag[];
    request: StudentRequest | null;
    requests_count: number | null;
    youtube_videos: string[];
};
