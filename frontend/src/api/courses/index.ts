import { ResponseData } from '../types';
import { doRequest } from '../../utils/requester';
import { apiClient } from '../apiClient';
import { Course, CourseBlock } from './types';

export async function fetchCourseBlocks(): Promise<ResponseData<CourseBlock[]>> {
    return doRequest<CourseBlock[]>(() => apiClient.get(`/course_module/course_blocks/`));
}

export async function fetchCourses(block_id: number): Promise<ResponseData<Course[]>> {
    return doRequest<Course[]>(() => apiClient.get(`/course_module/courses/`, { params: { block_id } }));
}
