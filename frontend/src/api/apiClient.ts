import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';

class APIClient {
    private readonly instance: AxiosInstance;

    constructor() {
        const token = localStorage.getItem('electives-platform/access-token');
        this.instance = axios.create(
            token
                ? {
                      baseURL: process.env.REACT_APP_BACKEND_DOMAIN,
                      headers: {
                          Authorization: 'Token ' + token,
                      },
                  }
                : {
                      baseURL: process.env.REACT_APP_BACKEND_DOMAIN,
                  },
        );
    }

    get(url: string, config?: AxiosRequestConfig) {
        return this.instance.get(url, config);
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    post(url: string, body: any, config?: AxiosRequestConfig) {
        return this.instance.post(url, body, config);
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    put(url: string, body: any, config?: AxiosRequestConfig) {
        return this.instance.put(url, body, config);
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    patch(url: string, body: any, config?: AxiosRequestConfig) {
        return this.instance.patch(url, body, config);
    }

    delete(url: string, config?: AxiosRequestConfig) {
        return this.instance.delete(url, config);
    }

    setAuthHeader(token: string) {
        this.instance.defaults.headers.Authorization = `Token ${token}`;
    }
}

export const apiClient = new APIClient();
