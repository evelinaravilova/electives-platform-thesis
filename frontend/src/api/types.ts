export type SuccessResponseData<TData> = {
    code?: number;
    success: boolean;
    message?: string;
    data: TData;
};

export type ErrorResponseData = {
    code?: number;
    success: false;
    message?: string;
    data: null;
};

export type ResponseData<TData> = SuccessResponseData<TData> | ErrorResponseData;
