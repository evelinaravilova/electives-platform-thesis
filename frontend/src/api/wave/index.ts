import { ResponseData } from '../types';
import { doRequest } from '../../utils/requester';
import { apiClient } from '../apiClient';
import { Wave, WaveBody, WaveBunch, WaveBunchBody, WaveBunchDetail, WavesResult } from './types';

export async function fetchCurrentWave(): Promise<ResponseData<Wave>> {
    return doRequest<Wave>(() => apiClient.get(`/wave_module/waves/current_wave/`));
}

export async function fetchDistributions(): Promise<ResponseData<WaveBunch[]>> {
    return doRequest<WaveBunch[]>(() => apiClient.get(`/wave_module/distributions/`));
}

export async function postDistribution(distribution: WaveBunchBody): Promise<ResponseData<WaveBunchBody>> {
    return doRequest<WaveBunchBody>(() => apiClient.post(`/wave_module/distributions/`, distribution));
}

export async function fetchDistribution(id: number): Promise<ResponseData<WaveBunchDetail>> {
    return doRequest<WaveBunchDetail>(() => apiClient.get(`/wave_module/distributions/${id}/`));
}

export async function putWave(wave: Wave): Promise<ResponseData<WaveBunchDetail>> {
    return doRequest<WaveBunchDetail>(() => apiClient.patch(`/wave_module/waves/${wave.id}/`, wave));
}

export async function fetchStateFields(wave: Wave, state: string): Promise<ResponseData<[string[], string[]]>> {
    return doRequest<[string[], string[]]>(() => apiClient.post(`/wave_module/wavefields`, { wave, state }));
}

export async function fetchAvailableWaveNumbers(wave_bunch_id: number): Promise<ResponseData<{ wave_numbers: number[] }>> {
    return doRequest<{ wave_numbers: number[] }>(() => apiClient.get(`/wave_module/waves/available_wave_numbers/`, { params: { wave_bunch_id } }));
}

export async function postWave(wave: Wave): Promise<ResponseData<Wave>> {
    return doRequest<Wave>(() => apiClient.post(`/wave_module/waves/`, wave));
}

export async function fetchWaveCourses(): Promise<ResponseData<WaveBody[]>> {
    return doRequest<WaveBody[]>(() => apiClient.get(`/wave_module/waves/wave_courses/`));
}

export async function fetchWaveResults(): Promise<ResponseData<WavesResult>> {
    return doRequest<WavesResult>(() => apiClient.get(`/wave_module/waves/wave_results/`));
}
