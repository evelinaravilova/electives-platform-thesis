import { Institute, Program } from '../institute/types';
import { Degree, DistributionStatus, RequestStatus, WaveType } from '../../consts/enums';
import { CourseBlock, RatingType } from '../courses/types';

export type WaveBunch = {
    id: number;
    current_course: number;
    degree: Degree;
    start_date: string | null;
    end_date: string | null;
    programs: Program[];
    semesters: number[];
    status: DistributionStatus;
    course_blocks: number[];
};

export type Wave = {
    id: number;
    wave_number: number;
    start_date: string;
    end_date: string;
    type: WaveType;
    use_rating: boolean;
    course_count: number;
    has_priority: boolean;
    rating_range?: [number, number];
    can_participate: boolean;
    use_previous_requests: boolean;
    forbid_overflow: boolean;
    show_requests_count: boolean;
    show_result: boolean;
    wave_bunch?: number;
    requests_spreadsheet: string;
    auto_distribution_date: string;
    auto_accept: boolean;
};

export type WaveBunchBody = {
    current_course: number;
    degree: Degree;
    institute: Institute;
};

export type WaveBunchDetail = {
    id: number;
    current_course: number;
    degree: Degree;
    start_date: string | null;
    end_date: string | null;
    programs: Program[];
    semesters: number[];
    status: DistributionStatus;
    waves: Wave[];
    course_blocks: CourseBlock[];
};

export type WaveBody = {
    id: number;
    wave_number: number;
    auto_distribution_date: string;
    degree: Degree;
    current_course: number;
    programs: string[];
    end_date: string;
    start_date: string;
    status: DistributionStatus;
    use_rating: boolean;
    courses: {
        id: number;
        name: string;
        block: CourseBlock;
        rating_type: RatingType;
        requests_count: number;
        students_count: number;
    }[]
}

export type WaveRequests = {
    id: number;
    wave_number: number;
    end_date: string;
    start_date: string;
    course_blocks: {
        id: number;
        degree: Degree;
        semester: number
        type: string;
        requests: {
            id: number;
            status: RequestStatus;
            course: string;
        }[]
    }[]
}

export type WavesResult = {
    current_course: number;
    results: {
        1?: WaveRequests[];
        2?: WaveRequests[];
        3?: WaveRequests[];
        4?: WaveRequests[];
    };
}
