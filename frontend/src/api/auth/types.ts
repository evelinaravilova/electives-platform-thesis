export type CurrentUser = {
    id: number;
    first_name: string;
    last_name: string;
    middle_name: string;
    email: string;
    role: string;
    photo: string;
};
