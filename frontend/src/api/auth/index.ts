import { addCurrentUser, addJwt } from '../../utils';
import { apiClient } from '../apiClient';
import { ResponseData } from '../types';
import { CurrentUser } from './types';

export const authorize = async (code: string): Promise<ResponseData<{ token: string; user: CurrentUser }>> => {
    try {
        const result = await apiClient.post('/auth/get_token', { code });
        const data = result.data.data;
        addJwt(data.token);
        addCurrentUser(data.user);
        apiClient.setAuthHeader(data.token);
        return { success: true, data: data };
    } catch (e) {
        return {
            success: false,
            message: e.response ? e.response.status + ' ' + e.response.statusText : 'Network Error',
            data: null,
        };
    }
};
