import { ResponseData } from '../types';
import { doRequest } from '../../utils/requester';
import { apiClient } from '../apiClient';
import { CourseRequests, PopularCourses, StudentRequest, StudentRequestBody } from './types';

export async function postRequest(
    course_id: number,
    wave_id: number,
    priority?: number,
): Promise<ResponseData<StudentRequest>> {
    const body: StudentRequestBody = {
        course_id,
        wave_id,
        priority,
    };
    return doRequest<StudentRequest>(() => apiClient.post(`/request_module/requests/`, body));
}

export async function deleteRequest(
    request_id: number,
    course_id: number,
    wave_id: number,
): Promise<ResponseData<StudentRequest>> {
    return doRequest<StudentRequest>(() =>
        apiClient.delete(`/request_module/requests/${request_id}/`, { params: { course_id, wave_id } }),
    );
}

export async function fetchCourseRequests(wave_id: number, course_id: number): Promise<ResponseData<CourseRequests>> {
    return doRequest<CourseRequests>(() =>
        apiClient.get(`/request_module/requests/`, { params: { course_id, wave_id } }),
    );
}

export async function fetchPopularCourses(block_id: number, year?: number): Promise<ResponseData<PopularCourses>> {
    return doRequest<PopularCourses>(() =>
        apiClient.get(`/request_module/analytics`, { params: { block_id, year } }),
    );
}
