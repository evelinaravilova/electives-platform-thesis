import { DistributionStatus, RequestStatus } from '../../consts/enums';
import { CurrentUser } from '../auth/types';
import { RatingType } from '../courses/types';

export type StudentRequest = {
    id: number;
    status: RequestStatus;
    priority: number | null;
};

export type StudentRequestBody = {
    course_id: number;
    wave_id: number;
    priority?: number;
};

export type RequestForTeacher = {
    id: number;
    user: CurrentUser;
    group: string;
    rating: number;
}

export type CourseRequests = {
    wave_id: number;
    auto_distribution_date: string;
    status: DistributionStatus;
    use_rating: boolean;
    course: {
        id: number;
        name: string;
        students_count: number;
        requests: RequestForTeacher[];
        rating_type: RatingType;
    }
};

export type PopularCourses = {
    intensity: {
        days: number[];
        values: number[];
    }
    popularity: {
        courses: string[];
        values: number[];
    }
    min_accept_score: {
        courses: string[];
        values: number[];
    }
};
