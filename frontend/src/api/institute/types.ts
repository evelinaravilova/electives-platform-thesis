import { CurrentUser } from '../auth/types';
import { Degree } from '../../consts/enums';

export type Group = {
    id: number;
    group_number: string;
    degree: Degree;
    start_year: number;
    end_year: number;
    current_course: number;
    program: number;
};

export type StudentProfile = {
    group: Group;
    email_extra: string;
};

export type TeacherProfile = {
    user: CurrentUser;
};

export type DeaneryProfile = {};

export type Profile = { user: CurrentUser } & (StudentProfile | TeacherProfile | DeaneryProfile);

export type Institute = {
    id: number
    key_number: number
    name: string
    short_name: string | null
}

export type Program = {
    id: number;
    name: string;
    institute?: number;
}

export type StudentRating = {
    student: number;
    rating_type: number;
    score: number;
}
