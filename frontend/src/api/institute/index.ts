import { apiClient } from '../apiClient';
import { ResponseData } from '../types';
import { Institute, Profile, Program, StudentRating } from './types';
import { doRequest } from '../../utils/requester';

export async function fetchProfile(role: string): Promise<ResponseData<Profile>> {
    return doRequest<Profile>(() => apiClient.get(`/institute/profiles/1/`, { params: { role } }));
}

export async function fetchInstitutes(): Promise<ResponseData<Institute[]>> {
    return doRequest<Institute[]>(() => apiClient.get(`/institute/institutes/`));
}

export async function fetchPrograms(): Promise<ResponseData<Program[]>> {
    return doRequest<Program[]>(() => apiClient.get(`/institute/programs/`));
}

export async function postInterviewRating(
    ratings: StudentRating[],
    wave_id: number,
    course_id: number,
): Promise<ResponseData<StudentRating[]>> {
    return doRequest<StudentRating[]>(() => apiClient.post(`/institute/student_ratings/`, { ratings, wave_id, course_id }));
}
