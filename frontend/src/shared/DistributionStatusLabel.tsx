import React, { FC } from 'react';
import styled from 'styled-components';
import { DistributionStatus, DistributionStatusName } from '../consts/enums';
import { theme } from '../styles';
import { FireFilled, HourglassFilled, TrophyFilled } from '@ant-design/icons/lib';

const calcColor = (status: DistributionStatus) => {
    switch (status) {
        case DistributionStatus.Waiting:
            return theme.colors.grey;
        case DistributionStatus.Active:
            return theme.colors.blue;
        case DistributionStatus.Finished:
            return theme.colors.green;
        default:
            return theme.colors.grey;
    }
};

export const DistributionStatusIcon = (status: DistributionStatus): JSX.Element => {
    const color = calcColor(status);

    switch (status) {
        case DistributionStatus.Waiting:
            return <HourglassFilled style={{ color }}/>;
        case DistributionStatus.Active:
            return <FireFilled style={{ color }}/>;
        case DistributionStatus.Finished:
            return <TrophyFilled style={{ color }}/>;
        default:
            return <></>;
    }
};

const StyledDistributionStatusLabel = styled.span<{ status: DistributionStatus }>`
    color: ${({ status }) => calcColor(status)};
    white-space: nowrap;
    font-weight: normal;
    font-size: 14px;
`;

type DistributionStatusLabelProps = {
    status: DistributionStatus;
};

export const DistributionStatusLabel: FC<DistributionStatusLabelProps> = ({ status }) => {
    return (
        <StyledDistributionStatusLabel status={status}>
            {DistributionStatusIcon(status)}
            &nbsp;&nbsp;{DistributionStatusName[status]}
        </StyledDistributionStatusLabel>
    );
};
