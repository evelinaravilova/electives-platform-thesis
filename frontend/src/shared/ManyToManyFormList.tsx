import React, { FC, useMemo } from 'react';
import { Button, Form, Select, Space } from 'antd';
import { DeleteTwoTone, PlusOutlined } from '@ant-design/icons/lib';
import { FormInstance, Rule } from 'antd/es/form';
import { ValidatorRule } from 'rc-field-form/lib/interface';

type ManyToManyFormListProps = {
    selectedItems: string[];
    setSelectedItems: (selectedItems: string[]) => void;
    options: OptionType[];
    name: string;
    form: FormInstance;
    formListRules: ValidatorRule[];
    formItemRules: Rule[];
    entity: string;
    loading?: boolean;
};

export type OptionType = { value: string; label: string };

export const ManyToManyFormList: FC<ManyToManyFormListProps> = ({
    selectedItems,
    setSelectedItems,
    options,
    name,
    form,
    formListRules,
    formItemRules,
    entity,
    loading,
}) => {
    const filteredOptions = useMemo(() => options.filter((item) => !selectedItems.includes(item.value)), [
        options,
        selectedItems,
    ]);

    return (
        <Form.List name={name} rules={formListRules}>
            {(fields, { add, remove }, { errors }) => (
                <>
                    {fields.map((field) => (
                        <Space key={field.key}>
                            <Form.Item {...field} rules={formItemRules} label={entity}>
                                <Select
                                    style={{ width: 450 }}
                                    optionLabelProp="label"
                                    options={filteredOptions}
                                    labelInValue={true}
                                />
                            </Form.Item>
                            <DeleteTwoTone onClick={() => {
                                const value = form.getFieldValue(name)[field.name].value;
                                setSelectedItems(selectedItems.filter(item => item !== value));
                                remove(field.name);
                            }} twoToneColor="#FF647C" />
                        </Space>
                    ))}
                    <br />
                    <Form.Item>
                        <Button
                            type="dashed"
                            onClick={() => add()}
                            block
                            icon={<PlusOutlined />}
                            loading={loading}
                            disabled={filteredOptions.length === 0}
                        >
                            Добавить {entity.toLowerCase()}
                        </Button>
                    </Form.Item>
                    <Form.ErrorList errors={errors} />
                </>
            )}
        </Form.List>
    );
};
