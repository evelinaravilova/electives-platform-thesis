import React from "react";
import styled from 'styled-components';

export const VideoResponsive = styled.div`
    overflow: hidden;
    padding-bottom: 56.25%;
    position: relative;
    height: 0;
    margin-top: 10px;
    
    iframe {
        left: 0;
        top: 0;
        height: 100%;
        width: 100%;
        position: absolute;
    }
`;

type YoutubeEmbedProps = {
    embedId: string;
}

export const YoutubeEmbed = ({ embedId }: YoutubeEmbedProps): JSX.Element => (
    <VideoResponsive>
        <iframe
            width="853"
            height="480"
            src={`https://www.youtube.com/embed/${embedId}`}
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
            title="Embedded youtube"
        />
    </VideoResponsive>
);
