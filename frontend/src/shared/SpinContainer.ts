import styled from 'styled-components';
import { Content } from 'antd/es/layout/layout';

export const SpinContainer = styled(Content)`
    margin: 20% auto;
    text-align: center;
`;
