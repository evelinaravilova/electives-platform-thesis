import styled from 'styled-components';
import { Row } from 'antd';

export const PaddedRow = styled(Row)`
    width: 100%;
    padding: 2%;
    .ant-spin-nested-loading {
        width: 100%;
    }
`;

export const RowContainer = styled.div`
    width: 100%;
    height: 100vh;
    overflow-y: auto;

    &::-webkit-scrollbar {
        display: none;
    }
    -ms-overflow-style: none; /* IE and Edge */
    scrollbar-width: none;
`;
