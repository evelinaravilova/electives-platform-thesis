import styled from 'styled-components';

export const StatusCircle = styled.div`
    background: ${({ status }: { status: string }) => status};
    width: 10px;
    height: 10px;
    border-radius: 50%;
    display: inline-block;
`;
