import styled from 'styled-components';
import { Layout } from 'antd';

export const PageLayout = styled(Layout)`
    min-height: 100vh;
    overflow-x: hidden;
    
    
    .site-layout-sub-header-background {
      background: #fff;
    }
`;
