import React, { FC } from 'react';
import { Form, InputNumber } from 'antd';
import { Rule } from 'antd/es/form';

interface EditableCellProps {
    editing: boolean;
    name: string;
    value: number;
    rules: Rule[];
}

export const EditableCell: FC<EditableCellProps> = ({
    editing,
    name,
    value,
    rules
}) => {
    return (
        editing ? <Form.Item style={{ margin: '0 0 0 10px', }} name={name} rules={rules}>
            <InputNumber />
        </Form.Item> : <span>{value}</span>
    );
};
