import React, { FC, useCallback, useEffect, useMemo, useState } from 'react';

import { Button, Form, message, Table } from 'antd';
import { RatingMode, RatingModeName } from '../../consts/enums';
import { RequestForTeacher } from '../../api/request/types';
import moment from 'moment';
import { EditableCell } from './EditableCell';
import { RatingType } from '../../api/courses/types';
import { postInterviewRating } from '../../api/institute';
import { StudentRating } from '../../api/institute/types';

type CourseRequestsFormProps = {
    autoDistributionDate: string;
    requests: RequestForTeacher[];
    ratingType?: RatingType;
    useRating?: boolean;
    waveId?: number;
    courseId?: number;
    loading: boolean;
};

type RequestScore = { [id: string]: number };

export const CourseRequestsForm: FC<CourseRequestsFormProps> = ({
    autoDistributionDate,
    ratingType,
    requests,
    useRating,
    waveId,
    courseId,
    loading,
}) => {
    const [form] = Form.useForm();

    const [scores, setScores] = useState<RequestScore>({} as RequestScore);

    useEffect(() => {
        const newScores: RequestScore = {};
        for (const request of requests) {
            newScores[request.user.id.toString()] = request.rating;
        }
        setScores(newScores);
    }, [requests]);

    const isInterview = ratingType?.mode === RatingMode.Interview;

    const isEditing = useMemo(() => {
        const currentDate = moment(new Date());
        const date = moment(autoDistributionDate);
        return currentDate.isBefore(date) && isInterview;
    }, [autoDistributionDate, isInterview]);

    const onFinish = useCallback(
        async (values) => {
            const requestBody: StudentRating[] = Object.entries(scores).map(([id, score]) => ({
                student: Number(id),
                score: score,
                rating_type: ratingType!.id,
            }));
            const result = await postInterviewRating(requestBody, waveId!, courseId!);
            if (result.success) {
                message.success("Баллы успешно отправлены")
            }
            console.log(requestBody);
        },
        [scores, ratingType],
    );

    const onFieldsChange = useCallback(
        (changedFields, allFields) => {
            setScores({ ...scores, [changedFields[0].name as string]: changedFields[0].value });
        },
        [scores],
    );

    const columns = useMemo(
        () => [
            {
                key: 'student_name',
                title: 'ФИО студента',
                width: '55%',
                render: (_: any, record: RequestForTeacher) =>
                    `${record.user.last_name} ${record.user.first_name} ${record.user.middle_name}`,
                sorter: (a: RequestForTeacher, b: RequestForTeacher) =>
                    a.user.last_name.localeCompare(b.user.last_name),
            },
            {
                key: 'group',
                title: 'Группа',
                dataIndex: 'group',
                sorter: (a: RequestForTeacher, b: RequestForTeacher) => {
                    const a2 = parseInt(a.group.split('-')[1], 10);
                    const b2 = parseInt(b.group.split('-')[1], 10);
                    return a2 - b2;
                },
            },
            useRating ? {
                key: 'rating',
                title: ratingType
                    ? ratingType.mode === RatingMode.Score
                        ? ratingType.key_name
                        : RatingModeName[ratingType.mode]
                    : '',
                dataIndex: 'rating',
                width: 200,
                render: (rating: number, record: RequestForTeacher) => (
                    <EditableCell
                        value={rating}
                        name={record.user.id.toString()}
                        editing={isEditing}
                        rules={[
                            { required: true, message: 'Пожалуйста, заполните поле' },
                            {
                                validator: async (_, value) => {
                                    const scoresWithoutCurrent = { ...scores };
                                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                                    // @ts-ignore
                                    scoresWithoutCurrent[Number(_.field)] = null;
                                    if (!value) return Promise.resolve();
                                    if (Object.values(scoresWithoutCurrent).includes(value))
                                        return Promise.reject(new Error('Пожалуйста, введите уникальное значение'));
                                },
                            },
                        ]}
                    />
                ),
                sorter: (a: RequestForTeacher, b: RequestForTeacher) => a.rating - b.rating,
            } : {}
        ],
        [ratingType, scores, isEditing, useRating],
    );

    return Object.keys(scores).length > 0 ? (
        <Form form={form} initialValues={scores} onFieldsChange={onFieldsChange} onFinish={onFinish}>
            <Table
                rowKey="id"
                columns={columns}
                dataSource={requests}
                scroll={{ y: window.innerHeight - 350 }}
                style={{ maxWidth: 1000 }}
                pagination={false}
                loading={loading}
                footer={() => (
                    isEditing ? <Button type="primary" htmlType="submit">
                        Отправить
                    </Button> : <></>
                )}
            />
        </Form>
    ) : (
        <>Нет заявок</>
    );
};
