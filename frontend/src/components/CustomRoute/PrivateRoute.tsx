import React, { FC } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';
import * as selectors from '../../redux-store/selectors';

type PrivateRouteProps = {
    path: string | string[];
    exact?: boolean;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    component?: any;
};

export const PrivateRoute: FC<PrivateRouteProps> = ({ ...rest }) => {
    const isUserAuthenticated = useSelector(selectors.user.isAuthenticated);
    return isUserAuthenticated ? (
        <Route {...rest} />
    ) : (
        <Redirect to="/login" />
    );
};
