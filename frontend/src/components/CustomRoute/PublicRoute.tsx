import React, { FC } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';
import * as selectors from '../../redux-store/selectors';
import { UserRole } from '../../consts/enums';

type PublicRouteProps = {
    path: string | string[];
    exact?: boolean;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    component?: any;
};

export const PublicRoute: FC<PublicRouteProps> = ({ ...rest }) => {
    const user = useSelector(selectors.user.user);
    const isUserAuthenticated = useSelector(selectors.user.isAuthenticated);

    const getRedirectPage = () => {
        switch (user.role) {
            case UserRole.STUDENT:
                return <Redirect to="/courses" />;
            case UserRole.TEACHER:
                return <Redirect to="/courses" />;
            case UserRole.DEANERY:
                return <Redirect to="/courses" />;
            default:
                return <Redirect to="/courses" />;
        }
    };

    return !process.env.REACT_APP_MOCK_AUTH || !isUserAuthenticated ? (
        <Route {...rest} />
    ) : (
        getRedirectPage()
    );
};
