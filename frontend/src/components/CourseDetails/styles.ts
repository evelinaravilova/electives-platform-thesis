import styled from 'styled-components';
import { Avatar, Button, Col, Row, Typography } from 'antd';
import { theme } from '../../styles';

export const CourseCover = styled.img`
    height: 100%;
    width: 100%;
    object-fit: cover; 
    border-radius: 10px;
`;

export const CoverRow = styled(Row)`
    height: 25%;
    max-height: 25%;
    width: 100%;
`;

export const CoverCol = styled(Col)`
    height: 100%;
    width: 100%;
`;

export const TeacherCol = styled(Col)`
    display: flex;
`;

export const TeacherCard = styled.div`
    border-radius: 10px;
    -webkit-box-shadow: 0px 4px 4px 0px rgba(34, 60, 80, 0.14);
    -moz-box-shadow: 0px 4px 4px 0px rgba(34, 60, 80, 0.14);
    box-shadow: 0px 4px 4px 0px rgba(34, 60, 80, 0.14);
    min-height: 50px;
    max-width: 230px;
    font-weight: 500;
    font-size: 0.9em;
    display: inline-block;
    display: flex;
    padding: 2%;
    word-wrap: break-word;
    margin: 0 20px 15px 0;
`;

export const TeacherAvatar = styled(Avatar)`
    min-width: 32px;
    margin-right: 10px;
`;

export const DescriptionCol = styled(Col)`
    max-height: 275px;
    overflow-y: auto;
`;

export const DescriptionBlock = styled(Typography.Paragraph)`
    width: 100%;
    white-space: pre-wrap;
    word-wrap: break-word;
`;

export const SubmitCol = styled(Col)`
   position: absolute;
   bottom: 3%;
`;

export const SubmitButton = styled(Button)`

`;

export const StatTag = styled.span`
    border: dashed 1px ${({theme}) => theme.colors.blue};
    border-radius: 10px;
    display: inline-block;
    padding: 5px;
    width: 90px;
    text-align: center;
    margin: 5px;
`;

export const Tag = styled.span`
    border: dashed 1px ${({type}: {type: string}) => type === 'skill' ? theme.colors.pink : theme.colors.blue};
    border-radius: 10px;
    display: inline-block;
    padding: 5px 8px 5px 8px;
    text-align: center;
    margin: 5px;
    font-size: 0.8em;
    font-weight: 500;
    color: ${({type}: {type: string}) => type === 'skill' ? theme.colors.pink : theme.colors.blue};
`;

export const statValueStyle = {
    marginBottom: 0,
    color: theme.colors.darkBlue,
};
