import React, { FC, useCallback, useEffect, useRef, useState } from 'react';
import cover from '../../assets/kpfu_logo_background_light2.jpg';
import { Col, Popover } from 'antd';
import Title from 'antd/es/typography/Title';
import {
    CourseCover,
    CoverCol,
    CoverRow,
    TeacherAvatar,
    TeacherCard,
    TeacherCol,
    DescriptionBlock,
    SubmitButton,
    SubmitCol,
    StatTag,
    statValueStyle,
    Tag,
    DescriptionCol,
} from './styles';
import { UserOutlined } from '@ant-design/icons/lib';
import { useDispatch, useSelector } from 'react-redux';
import {
    incrementBlockRequestsCount,
    decrementBlockRequestsCount,
} from '../../redux-store/courseReducer/courseBlockSlice';
import * as selectors from '../../redux-store/selectors';
import { TeacherProfile } from '../../api/institute/types';
import { Course } from '../../api/courses/types';
import { deleteRequest, postRequest } from '../../api/request';
import { loadCourses } from '../../redux-store/courseReducer/thunks';
import { RatingMode, RatingModeName } from '../../consts/enums';
import { YoutubeEmbed } from '../../shared/YoutubeEmbed';

type CourseDetailsProps = {
    waveId: number;
    showRequestCount: boolean;
    useRating: boolean;
};

export const CourseDetails: FC<CourseDetailsProps> = ({ waveId, showRequestCount, useRating }) => {
    const dispatch = useDispatch();
    const descriptionRef = useRef<HTMLDivElement>() as React.MutableRefObject<HTMLDivElement>;

    const courses = useSelector(selectors.course.courses);
    const activeCourse = useSelector(selectors.course.activeCourse);
    const activeBlock = useSelector(selectors.courseBlock.activeBlock);

    const [courseDetails, setCourseDetails] = useState<Course | null>(null);
    const [requested, setRequested] = useState(false);

    useEffect(() => {
        const course = courses.find((course) => course.id === activeCourse);
        if (course) {
            setCourseDetails(course);
            setRequested(!!course.request);
        }
    }, [courses, activeCourse]);

    useEffect(() => {
        if (descriptionRef.current) descriptionRef.current.scrollTo(0, 0);
    }, [activeCourse]);

    const teachersCards = useCallback(
        (teachers?: TeacherProfile[]) =>
            teachers &&
            teachers.map((teacher) => (
                <TeacherCard key={teacher.user.id}>
                    <TeacherAvatar icon={<UserOutlined />} src={teacher.user.photo} />{' '}
                    <span>
                        {teacher.user.last_name} {teacher.user.first_name} {teacher.user.middle_name}
                    </span>
                </TeacherCard>
            )),
        [],
    );

    const tagCards =
        courseDetails &&
        courseDetails.course_tag.map((tag) => (
            <Tag key={tag.name} type={tag.type}>
                {tag.name}
            </Tag>
        ));

    const youtubeVideos =
        courseDetails && courseDetails.youtube_videos.map((video) => <YoutubeEmbed key={video} embedId={video} />);

    const makeRequest = useCallback(async () => {
        if (!courseDetails) return;

        const result = await postRequest(courseDetails.id, waveId);
        if (result.success) {
            loadCourses(dispatch, activeBlock);
            dispatch(incrementBlockRequestsCount());
        }
    }, [activeBlock, courseDetails, dispatch, waveId]);

    const revokeRequest = useCallback(async () => {
        if (!courseDetails) return;

        const result = await deleteRequest(courseDetails.request!.id, courseDetails.id, waveId);
        if (result.success) {
            loadCourses(dispatch, activeBlock);
            dispatch(decrementBlockRequestsCount());
        }
    }, [activeBlock, courseDetails, dispatch, waveId]);

    return (
        <>
            {courseDetails && (
                <CoverRow gutter={[16, 16]}>
                    <CoverCol span={14}>
                        <CourseCover src={courseDetails.logo ? courseDetails.logo : cover} />
                    </CoverCol>
                    <Col span={10}>
                        {courseDetails.students_count && (
                            <StatTag>
                                <Title level={2} style={statValueStyle}>
                                    {courseDetails.students_count}
                                </Title>
                                квота
                            </StatTag>
                        )}
                        {showRequestCount && (
                            <StatTag>
                                <Title level={2} style={statValueStyle}>
                                    {courseDetails.requests_count}
                                </Title>
                                заявок
                            </StatTag>
                        )}
                        <div>{tagCards}</div>
                    </Col>

                    <Col span={24}>
                        <Title level={2}>{courseDetails?.name}</Title>
                    </Col>

                    <DescriptionCol span={24} ref={descriptionRef}>
                        {courseDetails.description && (
                            <DescriptionBlock>
                                <Title level={5}>Описание</Title>
                                {courseDetails?.description}
                            </DescriptionBlock>
                        )}
                        {courseDetails.requirements && (
                            <DescriptionBlock>
                                <Title level={5}>Требования</Title>
                                {courseDetails.requirements}
                            </DescriptionBlock>
                        )}
                        {useRating && (
                            <DescriptionBlock>
                                <Title level={5}>Тип рейтинга</Title>
                                {courseDetails.rating_type.mode === RatingMode.Score
                                    ? courseDetails.rating_type.key_name
                                    : RatingModeName[courseDetails.rating_type.mode]}
                            </DescriptionBlock>
                        )}
                        {courseDetails.youtube_videos.length > 0 && (
                            <DescriptionBlock>
                                <Title level={5}>Видео</Title>
                                {youtubeVideos}
                            </DescriptionBlock>
                        )}
                    </DescriptionCol>

                    <TeacherCol span={24}>
                        {teachersCards(courseDetails.head.slice(0, 2))}{' '}
                        {courseDetails.head.length > 2 && (
                            <Popover content={teachersCards(courseDetails.head.slice(2))} placement="topRight">
                                <TeacherCard>...</TeacherCard>
                            </Popover>
                        )}
                    </TeacherCol>
                    <SubmitCol span={24}>
                        {!requested ? (
                            <SubmitButton type="primary" size="large" onClick={makeRequest}>
                                Подать заявку
                            </SubmitButton>
                        ) : (
                            <SubmitButton type="primary" size="large" danger onClick={revokeRequest}>
                                Отменить заявку
                            </SubmitButton>
                        )}
                    </SubmitCol>
                </CoverRow>
            )}
        </>
    );
};
