import React, { FC, useEffect } from 'react';
import { DistributionTable } from './DistributionTable';
import { useDispatch, useSelector } from 'react-redux';
import * as selectors from '../../redux-store/selectors';
import { loadDistributions } from '../../redux-store/waveReducer/thunks';

export const DistributionList: FC = () => {
    const dispatch = useDispatch();
    const distributions = useSelector(selectors.waveBunches.waveBunches);
    const loading = useSelector(selectors.waveBunches.loading);

    useEffect(() => {
        loadDistributions(dispatch);
    }, [dispatch]);


    return (
        <div>
            {/*<iframe style={{height: 500}} src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRKv7kZ15O6inW4SKrFWmOCs4Q3n7nVrw8nDuMU43fSV4fIEBjP0ZPNzQGpLNk3CBp_0dvH48jaT3tu/pubhtml?widget=true&amp;headers=false"></iframe>*/}
            <DistributionTable distributions={distributions} loading={loading} />
        </div>
    );
};
