import styled from 'styled-components';
import { Button } from 'antd';

export const ActionRow = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
`;

export const ClearFiltersButton = styled(Button)`
    margin-bottom: 15px;
`;
