import moment from 'moment';
import { DistributionStatus } from '../../consts/enums';

export const renderDate = (date: string | null): string  => {
    if (date)
        return moment(date).format('LLL');
    return 'Не установлено';
};

export const mapDistributionStatus = (a: DistributionStatus): number => {
    switch (a) {
        case DistributionStatus.Active:
            return 1;
        case DistributionStatus.Waiting:
            return 2;
        case DistributionStatus.Finished:
            return 3;
    }
};
