import React, { FC, useCallback, useMemo, useState } from 'react';
import { useHistory } from 'react-router-dom';

import { Button, Table } from 'antd';
import { WaveBunch } from '../../api/wave/types';
import { Degree, DegreeName, DistributionStatus } from '../../consts/enums';
import { Program } from '../../api/institute/types';
import { DistributionStatusLabel } from '../../shared/DistributionStatusLabel';
import { mapDistributionStatus, renderDate } from './utils';
import { ActionRow, ClearFiltersButton } from './styles';
import { PlusOutlined } from '@ant-design/icons/lib';
import { DistributionModal } from '../DistributionModal';
import { Routes } from '../../consts';

export interface DistributionTableFilters {
    status: string[];
    course_number: string[];
    degree: string[];
}

type DistributionTableProps = {
    distributions: WaveBunch[];
    loading: boolean;
};

export const DistributionTable: FC<DistributionTableProps> = ({ distributions, loading }) => {
    const history = useHistory();

    const [modalOpen, setModalOpen] = useState<boolean>(false);
    const [filter, setFilter] = useState<DistributionTableFilters>({
        status: [],
        course_number: [],
        degree: [],
    });

    const onChange = useCallback((_pagination, filters) => {
        setFilter(filters);
    }, []);

    const clearFilters = () => {
        setFilter({ status: [], course_number: [], degree: [] });
    };

    const columns = useMemo(
        () => [
            {
                key: 'course_number',
                title: 'Курс',
                dataIndex: 'current_course',
                width: '7%',
                filters: [
                    { text: '1', value: 1 },
                    { text: '2', value: 2 },
                    { text: '3', value: 3 },
                    { text: '4', value: 4 },
                ],
                filteredValue: filter.course_number || null,
                onFilter: (value: string | number | boolean, record: WaveBunch) => record.current_course === value,
            },
            {
                key: 'degree',
                title: 'Академ. степень',
                dataIndex: 'degree',
                render: (degree: Degree) => DegreeName[degree],
                width: '12%',
                filters: [
                    { text: DegreeName.bachelor, value: Degree.Bachelor },
                    { text: DegreeName.magister, value: Degree.Master },
                ],
                filteredValue: filter.degree || null,
                onFilter: (value: string | number | boolean, record: WaveBunch) => record.degree === value,
            },
            {
                key: 'semesters',
                title: 'Семестры курсов',
                dataIndex: 'semesters',
                render: (semesters: number[]) => semesters.join(', '),
                width: '12%',
            },
            {
                key: 'start_date',
                title: 'Начало распределения',
                dataIndex: 'start_date',
                width: 210,
                render: renderDate,
                sorter: (a: WaveBunch, b: WaveBunch) => {
                    const [a2, b2] = [
                        Date.parse(a.start_date ?? '1970-01-01'),
                        Date.parse(b.start_date ?? '1970-01-01'),
                    ];
                    return a2 - b2;
                },
            },
            {
                key: 'end_date',
                title: 'Конец распределения',
                dataIndex: 'end_date',
                width: 200,
                render: renderDate,
                sorter: (a: WaveBunch, b: WaveBunch) => {
                    const [a2, b2] = [Date.parse(a.end_date ?? '1970-01-01'), Date.parse(b.end_date ?? '1970-01-01')];
                    return a2 - b2;
                },
            },
            {
                key: 'status',
                title: 'Статус',
                dataIndex: 'status',
                width: '12%',
                render: (status: DistributionStatus) => <DistributionStatusLabel status={status} />,
                sorter: (a: WaveBunch, b: WaveBunch) => {
                    const [a2, b2] = [mapDistributionStatus(a.status), mapDistributionStatus(b.status)];
                    return a2 - b2;
                },
                filters: [
                    {
                        text: <DistributionStatusLabel status={DistributionStatus.Waiting} />,
                        value: DistributionStatus.Waiting,
                    },
                    {
                        text: <DistributionStatusLabel status={DistributionStatus.Active} />,
                        value: DistributionStatus.Active,
                    },
                    {
                        text: <DistributionStatusLabel status={DistributionStatus.Finished} />,
                        value: DistributionStatus.Finished,
                    },
                ],
                filteredValue: filter.status || null,
                onFilter: (value: string | number | boolean, record: WaveBunch) => record.status === value,
            },
            {
                key: 'programs',
                title: 'Направления',
                dataIndex: 'programs',
                render: (programs: Program[]) => programs.map(program => program.name).join(', '),
                ellipsis: true,
            },
        ],
        [filter],
    );

    return (
        <>
            <ActionRow>
                <ClearFiltersButton onClick={clearFilters} type="text">
                    Очистить фильтры
                </ClearFiltersButton>
                <Button icon={<PlusOutlined />} onClick={() => setModalOpen(true)}>Создать распределение</Button>
            </ActionRow>

            <Table
                rowKey="id"
                columns={columns}
                dataSource={distributions}
                scroll={{ y: window.innerHeight - 150 }}
                pagination={false}
                onChange={onChange}
                loading={loading}
                onRow={(record, rowIndex) => {
                    return {
                        onClick: event => history.push(`${Routes.DISTRIBUTIONS}/${record.id}`)
                    };
                }}
            />
            <DistributionModal visible={modalOpen} setVisible={setModalOpen} />
        </>
    );
};
