import { WaveBody } from '../../api/wave/types';
import React, { FC, useMemo } from 'react';
import { Degree, DegreeName, DistributionStatus, RatingMode, RatingModeName } from '../../consts/enums';
import { mapDistributionStatus, renderDate } from '../DistributionList/utils';
import { DistributionStatusLabel } from '../../shared/DistributionStatusLabel';
import { Table } from 'antd';
import { Routes } from '../../consts';
import { CourseBlock, RatingType } from '../../api/courses/types';
import { useHistory } from 'react-router';
import { getWaveId } from './utils';

type WaveCoursesTableProps = {
    waveCourses: WaveBody[];
    loading: boolean;
};

export const WaveCoursesTable: FC<WaveCoursesTableProps> = ({ waveCourses, loading }) => {
    const history = useHistory();

    const sortedWaveCourses = useMemo(
        () =>
            [...waveCourses].sort((a: WaveBody, b: WaveBody) => {
                const [a2, b2] = [mapDistributionStatus(a.status), mapDistributionStatus(b.status)];
                return a2 - b2;
            }),
        [waveCourses],
    );

    const columns = useMemo(
        () => [
            {
                key: 'auto_distribution_date',
                title: 'Автоматическое распределение',
                dataIndex: 'auto_distribution_date',
                width: 210,
                render: (date: string) => <b>{renderDate(date)}</b>,
            },
            {
                key: 'status',
                title: 'Статус',
                dataIndex: 'status',
                width: 150,
                render: (status: DistributionStatus) => <DistributionStatusLabel status={status} />,
            },
            {
                key: 'start_date',
                title: 'Начало волны у студентов',
                dataIndex: 'start_date',
                width: 210,
                render: renderDate,
            },
            {
                key: 'end_date',
                title: 'Конец волны у студентов',
                dataIndex: 'end_date',
                width: 200,
                render: renderDate,
            },
            {
                key: 'course_number',
                title: 'Курс',
                dataIndex: 'current_course',
                width: 70,
            },
            {
                key: 'degree',
                title: 'Академ. степень',
                dataIndex: 'degree',
                width: 125,
                render: (degree: Degree) => DegreeName[degree],
            },
            {
                key: 'wave_number',
                title: 'Номер волны',
                dataIndex: 'wave_number',
                width: 100,
            },
            {
                key: 'programs',
                title: 'Направления',
                dataIndex: 'programs',
                render: (programs: string[]) => programs.join(', '),
                ellipsis: true,
            },
        ],
        [],
    );

    const expandedRowRender = (record: WaveBody) => {
        if (record.id) {
            const columns = [
                { key: 'wave_id', width: 0 },
                { title: 'Название', dataIndex: 'name', key: 'name' },
                {
                    title: 'Блок',
                    dataIndex: 'block',
                    key: 'block',
                    render: (block: CourseBlock) =>
                        `${block.type} ${block.semester} семестр ${DegreeName[block.degree]}`,
                },
                {
                    title: 'Тип рейтинга',
                    key: 'rating_type',
                    dataIndex: 'rating_type',
                    render: (ratingType: RatingType) => record.use_rating ? (
                        ratingType.mode === RatingMode.Score ? ratingType.key_name : RatingModeName[ratingType.mode]) : '-',
                },
                { title: 'Количество заявок', dataIndex: 'requests_count', key: 'requests_count' },
                { title: 'Квота', dataIndex: 'students_count', key: 'students_count' },
            ];
            return (
                <Table
                    rowKey="id"
                    columns={columns}
                    dataSource={record.courses}
                    pagination={false}
                    rowClassName="course"
                    onRow={(record, rowIndex) => {
                        return {
                            onClick: (event) => {
                                history.push(`${Routes.COURSE_REQUESTS}?course_id=${record.id}&wave_id=${getWaveId(event.target as HTMLElement)}`);
                            },
                        };
                    }}
                />
            );
        }
        return null;
    };

    return (
        <Table<WaveBody>
            rowKey="id"
            columns={columns}
            pagination={false}
            scroll={{ y: window.innerHeight - 300 }}
            dataSource={sortedWaveCourses}
            loading={loading}
            rowClassName="wave"
            expandable={{
                expandedRowRender,
            }}
            expandRowByClick
        />
    );
};
