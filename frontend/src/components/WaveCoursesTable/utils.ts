export const getWaveId = (node: HTMLElement): number => {
    let result: HTMLElement | null = node;
    for (let i = 0; i < 11; i++) {
        result = result!.parentElement;
    }
    return Number(result!.previousElementSibling!.getAttribute('data-row-key'));
};