import React, { FC } from 'react';
import { StyledAlert } from './styles';
import 'moment/locale/ru'

type InfoMessageProps = {
    info: React.ReactNode;
    style?: any;
};

export const InfoMessage: FC<InfoMessageProps> = ({ info, style }) => {

    return (
        <StyledAlert
            message="Информация"
            description={info}
            type="info"
            showIcon
            style={style}
        />
    );
};
