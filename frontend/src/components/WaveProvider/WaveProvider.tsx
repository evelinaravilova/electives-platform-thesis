import WaveContext from './WaveContext';
import React, { FC, useState } from 'react';
import { Wave } from '../../api/wave/types';
import { fetchCurrentWave } from '../../api/wave';
import { useSelector } from 'react-redux';
import * as selectors from '../../redux-store/selectors';
import { useComponentWillMount } from '../../utils/hooks';
import { UserRole } from '../../consts/enums';

export const WaveProvider: FC = (props) => {
    const user = useSelector(selectors.user.user);
    const isUserAuthenticated = useSelector(selectors.user.isAuthenticated);
    const [wave, setWave] = useState<Wave | null>(null);

    useComponentWillMount(() => {
        if (isUserAuthenticated && user.role === UserRole.STUDENT) {
            fetchCurrentWave().then((result) => {
                if (result.success) setWave(result.data)
            });
        }
    });

    /*useEffect(() => {
        if (isUserAuthenticated && user.role === UserRole.STUDENT) {
            fetchCurrentWave().then((result) => {
                if (result.success) setWave(result.data)
            });
        }
    }, [isUserAuthenticated, user.role]);*/

    return <WaveContext.Provider value={wave}>{props.children}</WaveContext.Provider>;
};
