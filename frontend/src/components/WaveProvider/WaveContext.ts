import React from 'react';
import { Wave } from '../../api/wave/types';

const WaveContext = React.createContext<Wave | null>(null);

export default WaveContext;
