import React, { FC, useCallback, useEffect, useState } from 'react';
import { Form, Modal, Select } from 'antd';
import { Degree, DegreeName } from '../../consts/enums';
import { Program } from '../../api/institute/types';
import { fetchPrograms } from '../../api/institute';
import { postDistribution } from '../../api/wave';
import { loadDistributions } from '../../redux-store/waveReducer/thunks';
import { useDispatch } from 'react-redux';
import { CourseBlocksFormList } from '../WaveModal/steps-content/CourseBlocksForm';
import { loadCourseBlocks } from '../../redux-store/courseReducer/thunks';
import { OptionType } from '../../shared/ManyToManyFormList';
import { ProgramsFormList } from '../WaveModal/steps-content/ProgramsForm';

const { Option } = Select;

type DistributionModalProps = {
    visible: boolean;
    setVisible: (visible: boolean) => void;
};

export const DistributionModal: FC<DistributionModalProps> = ({ visible, setVisible }) => {
    const dispatch = useDispatch();
    const [form] = Form.useForm();

    const [confirmLoading, setConfirmLoading] = useState(false);

    const [isBachelor, setBachelor] = useState<boolean | null>(null);
    const [programs, setPrograms] = useState<Program[]>([]);
    const [programsLoading, setProgramsLoading] = useState<boolean>(true);
    const [selectedCourseBlocks, setSelectedCourseBlocks] = useState<string[]>([]);
    const [selectedPrograms, setSelectedPrograms] = useState<string[]>([]);

    useEffect(() => {
        if (visible) {
            fetchPrograms().then((result) => {
                if (result.success) {
                    setPrograms(result.data);
                }
                setProgramsLoading(false);
            });
            loadCourseBlocks(dispatch);
        }
    }, [visible, dispatch]);

    const handleClose = useCallback(() => {
        setVisible(false);
        form.resetFields();
        setBachelor(null);
        setSelectedCourseBlocks([]);
        setSelectedPrograms([]);
    }, [form, setVisible]);

    const handleOk = useCallback(async () => {
        setConfirmLoading(true);
        form.validateFields()
            .then(async (values) => {
                const resultValues = {
                    ...values,
                    course_blocks: values.course_blocks.map((option: OptionType) => +option.value),
                    programs_id: values.programs_id.map((option: OptionType) => +option.value),
                };
                const result = await postDistribution(resultValues);
                if (result.success) {
                    handleClose();
                    loadDistributions(dispatch);
                }
            })
            .catch((info) => {
                console.error('Validate Failed:', info);
            });
        setConfirmLoading(false);
    }, [form, dispatch, handleClose]);

    return (
        <Modal
            title="Создать распределение"
            visible={visible}
            onOk={handleOk}
            confirmLoading={confirmLoading}
            onCancel={handleClose}
            destroyOnClose
            okText="Создать"
            cancelText="Отмена"
        >
            <Form
                form={form}
                requiredMark={false}
                layout="vertical"
                autoComplete="off"
                onFieldsChange={(changedField: any, allFields) => {
                    const value = changedField[0].value.value;
                    if (value) {
                        switch (changedField[0].name[0]) {
                            case 'course_blocks':
                                if (!selectedCourseBlocks.includes(value))
                                    setSelectedCourseBlocks([...selectedCourseBlocks, value]);
                                break;
                            case 'programs_id':
                                if (!selectedPrograms.includes(value))
                                    setSelectedPrograms([...selectedPrograms, value]);
                                break;
                        }
                    }
                }}
            >
                <Form.Item
                    name="degree"
                    label="Академ. степень студентов"
                    rules={[{ required: true, message: 'Пожалуйста, выберите академ. степень' }]}
                >
                    <Select onSelect={(value: string) => setBachelor(value === Degree.Bachelor)}>
                        <Option value={Degree.Bachelor}>{DegreeName.bachelor}</Option>
                        <Option value={Degree.Master}>{DegreeName.magister}</Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    name="current_course"
                    label="Номер курса студентов"
                    rules={[
                        { required: true, message: 'Пожалуйста, выберите номер курса' },
                        {
                            validator: (_, value) => {
                                if (!isBachelor && value > 2)
                                    return Promise.reject(new Error('Выберите курс от 1 до 2'));
                                return Promise.resolve();
                            },
                        },
                    ]}
                >
                    <Select disabled={isBachelor === null}>
                        <Option value={1}>1</Option>
                        <Option value={2}>2</Option>
                        {isBachelor && (
                            <>
                                <Option value={3}>3</Option>
                                <Option value={4}>4</Option>
                            </>
                        )}
                    </Select>
                </Form.Item>
                <CourseBlocksFormList
                    selectedCourseBlocks={selectedCourseBlocks}
                    setSelectedCourseBlocks={setSelectedCourseBlocks}
                    form={form}
                />
                <ProgramsFormList
                    programs={programs}
                    selectedPrograms={selectedPrograms}
                    setSelectedPrograms={setSelectedPrograms}
                    form={form}
                    loading={programsLoading}
                />
            </Form>
        </Modal>
    );
};
