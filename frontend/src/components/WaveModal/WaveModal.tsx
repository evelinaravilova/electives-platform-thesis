import React, { FC, useCallback, useEffect, useState } from 'react';
import { Button, Form, Modal } from 'antd';
import { fetchAvailableWaveNumbers, postWave } from '../../api/wave';
import { loadDistribution } from '../../redux-store/waveReducer/thunks';
import { useDispatch } from 'react-redux';
import { ParametersForm } from './steps-content/ParametersForm';
import { WaveFieldType } from './utils';
import { Wave } from '../../api/wave/types';
import { emptyWave } from '../../consts/emptyWave';
import { useRouteMatch } from 'react-router';

type WaveModalProps = {
    visible: boolean;
    setVisible: (visible: boolean) => void;
};

const initialFields: WaveFieldType[] = ['wave_number', 'type', 'use_rating', 'auto_accept', 'show_result', 'course_count'];

export const WaveModal: FC<WaveModalProps> = ({ visible, setVisible }) => {
    const dispatch = useDispatch();
    const [form] = Form.useForm();

    const { distribution_id: distributionId } = useRouteMatch<{ distribution_id: string }>().params;

    // const [currentStep, setCurrentStep] = useState(0);

    const [confirmLoading, setConfirmLoading] = useState(false);

    const [fields, setFields] = useState<WaveFieldType[]>(initialFields);
    const [waveValues, setWaveValues] = useState<Wave>({...emptyWave as Wave, wave_bunch: Number(distributionId)});

   /* const changeStep = useCallback(
        (newStep) => {
            if (currentStep === 0) {

            }
            setCurrentStep(newStep);
        },
        [currentStep],
    );*/


    useEffect(() => {
        fetchAvailableWaveNumbers(Number(distributionId)).then((result) => {
            const newWaveNumber = result.data!.wave_numbers[0];
            const newWaveValues = { ...waveValues, wave_number: newWaveNumber };
            setWaveValues(newWaveValues);
        });
    }, [visible]);

    /*const steps = useMemo(
        () => [
            {
                title: 'Параметры',
                content: <ParametersForm
                    fields={fields}
                    setFields={setFields}
                    waveValues={waveValues}
                    setWaveValues={setWaveValues}
                />,
                status: parametersStatus,
            },
            { title: 'Блоки курсов', content: <CourseBlocksForm
                                                                fields={fields}
                                                                setFields={setFields}
                                                                waveValues={waveValues}
                                                                setWaveValues={setWaveValues} /> },
            { title: 'Подтверждение', content: 'Last-content' },
        ],
        [parametersStatus, fields, waveValues],
    );*/

    const handleClose = useCallback(() => {
        setVisible(false);
        // setCurrentStep(0);
        setFields(initialFields);
    }, [setVisible]);


    const handleOk = useCallback(async () => {
        form.validateFields()
            .then(async (values) => {
                setConfirmLoading(true);
                const result = await postWave(waveValues);
                if (result.success){
                    loadDistribution(dispatch, Number(distributionId));
                    handleClose();
                }
                setConfirmLoading(false);
            })
            .catch((info) => {
                console.error('Validate Failed:', info);
            });
    }, [form, dispatch, handleClose, waveValues, distributionId]);

    /*const stepActions = useMemo(() => {
        const result: JSX.Element[] = [];
        if (currentStep > 0)
            result.push(
                <Button key="back" onClick={() => setCurrentStep(currentStep - 1)} style={{ margin: '0 8px' }}>
                    Назад
                </Button>,
            );
        if (currentStep < steps.length - 1)
            result.push(
                <Button
                    key="forward"
                    type="primary"
                    onClick={() => {
                        if (currentStep === 0) {
                        }
                        setCurrentStep(currentStep + 1);
                    }}
                >
                    Вперед
                </Button>,
            );
        if (currentStep === steps.length - 1)
            result.push(
                <Button key="submit" type="primary" onClick={handleOk} loading={confirmLoading}>
                    Отправить
                </Button>,
            );
        return result;
    }, [currentStep, steps.length, handleOk, confirmLoading]);*/


    return (
        <Modal
            title="Создать волну"
            visible={visible}
            confirmLoading={confirmLoading}
            destroyOnClose
            width={600}
            onCancel={handleClose}
            footer={[
                <Button key="cancel" onClick={handleClose}>
                    Отмена
                </Button>,
                <Button key="submit" type="primary" onClick={handleOk} loading={confirmLoading}>
                    Отправить
                </Button>
                // ...stepActions,
            ]}
        >
            <ParametersForm
                form={form}
                fields={fields}
                setFields={setFields}
                waveValues={waveValues}
                setWaveValues={setWaveValues}
            />
            {/*<Steps type="navigation" current={currentStep} onChange={changeStep}>
                {steps.map((item) => (
                    <Step key={item.title} title={item.title} />
                ))}
            </Steps>
            <StepContent>{steps[currentStep].content}</StepContent>*/}
        </Modal>
    );
};
