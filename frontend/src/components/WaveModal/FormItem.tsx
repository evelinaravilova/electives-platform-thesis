import React, {FC} from 'react';
import { Form } from 'antd';

export type FormItemProps = {
    component: JSX.Element;
    label: string;
    required: boolean;
    message: string;
    name: string;
    valuePropName?: string;
}

export const FormItem: FC<FormItemProps> = ({ component, label, required, message, name, valuePropName }) => {

    return (
        <Form.Item
            label={label}
            name={name}
            rules={[{ required, message: message }]}
            valuePropName={valuePropName}
        >
            {component}
        </Form.Item>
    )
};