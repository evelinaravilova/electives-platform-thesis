import React, { FC, useCallback, useEffect, useMemo } from 'react';
import { DatePicker, Form, InputNumber, Select, Slider, Switch } from 'antd';
import { FormInstance } from 'antd/es/form';
import { WaveType, WaveTypeName } from '../../../consts/enums';
import { fetchStateFields } from '../../../api/wave';
import { Wave } from '../../../api/wave/types';
import { FormItem } from '../FormItem';
import { WaveFieldType } from '../utils';
import { FieldData } from 'rc-field-form/lib/interface';

const { Option } = Select;

type ParametersFormProps = {
    form: FormInstance;
    fields: WaveFieldType[];
    setFields: (fields: WaveFieldType[]) => void;
    waveValues: Wave;
    setWaveValues: (wave: Wave) => void;
};

type WaveField = {
    [fieldName in WaveFieldType]: {
        component: JSX.Element;
        label: string;
        required: boolean;
        message: string;
        name: string;
        valuePropName?: string;
    };
};

export const ParametersForm: FC<ParametersFormProps> = ({ form, fields, setFields, waveValues, setWaveValues }) => {

    useEffect(() => {
        form.setFieldsValue({ wave_number: waveValues.wave_number });
    }, [waveValues.wave_number, form]);

    const setNewFields = useCallback(
        (resultFields) => {
            let newFields = [...fields];
            for (const f of resultFields[0]) {
                newFields.push(f);
            }
            newFields = newFields.filter((field) => !resultFields[1].includes(field));
            form.resetFields(resultFields[1]);
            setFields(newFields);
        },
        [fields, form, setFields],
    );

    useEffect(() => {
        waveValues.wave_number &&
            fetchStateFields(waveValues, 'wave_number').then((result) => {
                if (result.success) {
                    setNewFields(result.data);
                }
            });
    }, [waveValues.wave_number]);

    const handleValueEnter = useCallback(
        (state: string) => {
            form.validateFields(['type'])
                .then(async (values) => {
                    const newWaveValues = { ...waveValues, [state]: form.getFieldValue(state) };
                    setWaveValues(newWaveValues);
                    const result = await fetchStateFields(newWaveValues, state);
                    if (result.success) {
                        setNewFields(result.data);
                    }
                })
                .catch((info) => {
                    console.error('Validate Failed:', info);
                    form.resetFields([state]);
                });
        },
        [form, waveValues, setNewFields, setWaveValues],
    );

    const onFieldsChange = (changedFields: FieldData[], allFields: FieldData[]) => {
        changedFields.length &&
            setWaveValues({ ...waveValues, [(changedFields[0].name as string[])[0]]: changedFields[0].value });
    };

    const fieldsAll: WaveField[] = useMemo(
        () => [
            {
                wave_number: {
                    component: <InputNumber min={1} max={100} value={waveValues.wave_number} disabled />,
                    label: 'Номер волны',
                    required: true,
                    message: '',
                    name: 'wave_number',
                },
            } as WaveField,
            {
                type: {
                    component: (
                        <Select
                            onSelect={() => handleValueEnter('type')}
                            style={{ width: 210 }}
                            value={waveValues.type}
                        >
                            <Option value={WaveType.Selection}>{WaveTypeName.selection}</Option>
                            <Option value={WaveType.Algorithm}>{WaveTypeName.algorithm}</Option>
                        </Select>
                    ),
                    label: 'Тип волны',
                    required: true,
                    message: 'Пожалуйста, выберите тип',
                    name: 'type',
                },
            } as WaveField,
            {
                start_date: {
                    component: (
                        <DatePicker
                            showTime
                            // value={moment(editDetails.start_date)}
                            allowClear={false}
                        />
                    ),
                    label: 'Дата начала',
                    required: true,
                    message: 'Пожалуйста, выберите дату начала',
                    name: 'start_date',
                },
            } as WaveField,
            {
                end_date: {
                    component: (
                        <DatePicker
                            showTime
                            // value={moment(editDetails.start_date)}
                            allowClear={false}
                        />
                    ),
                    label: 'Дата конца',
                    required: true,
                    message: 'Пожалуйста, выберите дату конца',
                    name: 'end_date',
                },
            } as WaveField,
            {
                auto_distribution_date: {
                    component: (
                        <DatePicker
                            showTime
                            // value={moment(editDetails.start_date)}
                            allowClear={false}
                        />
                    ),
                    label: 'Дата автоматического распределения',
                    required: false,
                    message: 'Пожалуйста, выберите дату автоматического распределения',
                    name: 'auto_distribution_date',
                },
            } as WaveField,
            {
                use_rating: {
                    component: <Switch onChange={() => handleValueEnter('use_rating')} />,
                    label: 'Рейтинг',
                    required: false,
                    message: '',
                    name: 'use_rating',
                    valuePropName: 'checked',
                },
            } as WaveField,
            {
                auto_accept: {
                    component: <Switch />,
                    label: 'Автоматический прием',
                    required: false,
                    message: '',
                    name: 'auto_accept',
                    valuePropName: 'checked',
                },
            } as WaveField,
            {
                rating_range: {
                    component: (
                        <Slider
                            range
                            value={waveValues.rating_range}
                            marks={{
                                71: '71',
                                86: '86',
                                100: '100',
                            }}
                        />
                    ),
                    label: 'Интервал рейтинга',
                    required: false,
                    message: '',
                    name: 'rating_range',
                },
            } as WaveField,
            {
                course_count: {
                    component: <InputNumber min={1} max={10} onChange={() => handleValueEnter('course_count')} />,
                    label: 'Количество курсов',
                    required: true,
                    message: 'Пожалуйста, выберите количество курсов',
                    name: 'course_count',
                },
            } as WaveField,
            {
                has_priority: {
                    component: <Switch />,
                    label: 'Приоритет',
                    required: false,
                    message: '',
                    name: 'has_priority',
                    valuePropName: 'checked',
                },
            } as WaveField,
            {
                show_result: {
                    component: <Switch />,
                    label: 'Показать результаты волны студенту',
                    required: false,
                    message: '',
                    name: 'show_result',
                    valuePropName: 'checked',
                },
            } as WaveField,
            {
                forbid_overflow: {
                    component: <Switch />,
                    label: 'Запретить переполнение',
                    required: false,
                    message: '',
                    name: 'forbid_overflow',
                    valuePropName: 'checked',
                },
            } as WaveField,
            {
                show_requests_count: {
                    component: <Switch />,
                    label: 'Показывать количество заявок на курс',
                    required: false,
                    message: '',
                    name: 'show_requests_count',
                    valuePropName: 'checked',
                },
            } as WaveField,
            {
                can_participate: {
                    component: <Switch />,
                    label: 'Можно ли участвовать тем, кто уже прошел',
                    required: false,
                    message: '',
                    name: 'can_participate',
                    valuePropName: 'checked',
                },
            } as WaveField,
            {
                use_previous_requests: {
                    component: <Switch />,
                    label: 'Выбор из тех курсов, куда прошел',
                    required: false,
                    message: '',
                    name: 'use_previous_requests',
                    valuePropName: 'checked',
                },
            } as WaveField,
        ],
        [handleValueEnter, waveValues],
    );

    return (
        <Form
            form={form}
            requiredMark={false}
            layout="vertical"
            onFieldsChange={onFieldsChange}
            onFinish={(v) => console.log(v)}
            onFinishFailed={({ errorFields }) => {
                console.log(errorFields);
            }}
            style={{margin: 20}}
        >
            {fieldsAll.map((field) => {
                const key = Object.keys(field)[0] as WaveFieldType;
                if (fields.includes(key)) return <FormItem key={key} {...field[key as WaveFieldType]} />;
            })}
        </Form>
    );
};
