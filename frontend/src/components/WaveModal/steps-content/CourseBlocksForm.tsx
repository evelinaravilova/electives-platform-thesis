import React, { FC, useEffect, useState } from 'react';
import { Form, Select } from 'antd';
import { WaveBunch } from '../../../api/wave/types';
import { DegreeName } from '../../../consts/enums';
import { useDispatch, useSelector } from 'react-redux';
import * as selectors from '../../../redux-store/selectors';
import { FieldData } from 'rc-field-form/lib/interface';
import { SelectValue } from 'antd/es/select';
import { loadCourseBlocks } from '../../../redux-store/courseReducer/thunks';
import { ManyToManyFormList } from '../../../shared/ManyToManyFormList';
import { FormInstance } from 'antd/es/form';

type CourseBlocksFormProps = {
    waveBunchValues?: WaveBunch;
    setWaveBunchValues?: (waveBunch: WaveBunch) => void;
};

export const CourseBlockSelect: FC<{
    onSelect: (value: SelectValue) => void;
    courseBlockOptions: JSX.Element[];
    options: { key: number; value: number; name: string }[];
}> = ({ onSelect, courseBlockOptions, options }) => {
    const [selectedName, setSelectedName] = useState('');

    const onOptSelect = (value: any) => {
        setSelectedName(options.find((o) => o.key === value)!.name);
        onSelect(value);
    };

    return (
        <Select style={{ width: 450 }} onSelect={onOptSelect} optionLabelProp="label" value={selectedName}>
            {courseBlockOptions}
        </Select>
    );
};

type CourseBlocksFormListProps = {
    selectedCourseBlocks: string[];
    setSelectedCourseBlocks: (selectedItems: string[]) => void;
    form: FormInstance;
};

export const CourseBlocksFormList: FC<CourseBlocksFormListProps> = ({ selectedCourseBlocks, setSelectedCourseBlocks, form }) => {
    const courseBlocks = useSelector(selectors.courseBlock.blocks);

    const options = courseBlocks.map((courseBlock) => ({
        value: courseBlock.id.toString(),
        label: `${courseBlock.type} блок - ${courseBlock.semester} семестр - ${DegreeName[courseBlock.degree]}`,
    }));

    return (
        <ManyToManyFormList
            selectedItems={selectedCourseBlocks}
            setSelectedItems={setSelectedCourseBlocks}
            options={options}
            name="course_blocks"
            entity="Блок курсов"
            form={form}
            formListRules={[
                {
                    validator: async (_, courseBlocks) => {
                        if (!courseBlocks || courseBlocks.length === 0) {
                            return Promise.reject(new Error('Пожалуйста, добавьте как минимум один блок курсов'));
                        }
                    },
                },
            ]}
            formItemRules={[{ required: true, message: 'Пожалуйста, выберите блок курсов' }]}
        />
    );
};

export const CourseBlocksForm: FC<CourseBlocksFormProps> = ({ waveBunchValues, setWaveBunchValues }) => {
    const [form] = Form.useForm();
    const dispatch = useDispatch();

    const [selectedCourseBlocks, setSelectedCourseBlocks] = useState<string[]>([]);

    useEffect(() => {
        loadCourseBlocks(dispatch);
    }, [dispatch]);

    const onFieldsChange = (changedFields: FieldData[], allFields: FieldData[]) => {
        console.log(allFields);
    };

    return (
        <Form
            form={form}
            requiredMark={false}
            layout="vertical"
            onFieldsChange={onFieldsChange}
            onFinish={(values) => console.log('Received values of form:', values)}
            autoComplete="off"
        >
            <CourseBlocksFormList selectedCourseBlocks={selectedCourseBlocks} setSelectedCourseBlocks={setSelectedCourseBlocks} form={form} />
        </Form>
    );
};
