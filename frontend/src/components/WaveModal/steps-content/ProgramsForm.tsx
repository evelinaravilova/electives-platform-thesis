import React, { FC } from 'react';
import { ManyToManyFormList } from '../../../shared/ManyToManyFormList';
import { Program } from '../../../api/institute/types';
import { FormInstance } from 'antd/es/form';

type ProgramsFormListProps = {
    programs: Program[];
    selectedPrograms: string[];
    setSelectedPrograms: (selectedItems: string[]) => void;
    form: FormInstance;
    loading: boolean;
};

export const ProgramsFormList: FC<ProgramsFormListProps> = ({ programs, selectedPrograms, setSelectedPrograms, form, loading }) => {
    const options = programs.map((program) => ({
        value: program.id.toString(),
        label: program.name,
    }));

    return (
        <ManyToManyFormList
            selectedItems={selectedPrograms}
            setSelectedItems={setSelectedPrograms}
            options={options}
            name="programs_id"
            entity="Направление"
            form={form}
            formListRules={[
                {
                    validator: async (_, programs) => {
                        if (!programs || programs.length === 0) {
                            return Promise.reject(new Error('Пожалуйста, добавьте как минимум одно направление'));
                        }
                    },
                },
            ]}
            formItemRules={[{ required: true, message: 'Пожалуйста, выберите направление' }]}
            loading={loading}
        />
    );
};