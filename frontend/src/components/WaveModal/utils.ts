export const waveFields = [
    'wave_number',
    'start_date',
    'end_date',
    'type',
    'use_rating',
    'course_count',
    'has_priority',
    'can_participate',
    'use_previous_requests',
    'forbid_overflow',
    'show_requests_count',
    'show_result',
    'auto_accept',
    'auto_distribution_date',
];

export type WaveFieldType =
    | 'wave_number'
    | 'start_date'
    | 'end_date'
    | 'type'
    | 'use_rating'
    | 'course_count'
    | 'has_priority'
    | 'can_participate'
    | 'use_previous_requests'
    | 'forbid_overflow'
    | 'show_requests_count'
    | 'show_result'
    | 'rating_range'
    | 'auto_accept'
    | 'auto_distribution_date';
