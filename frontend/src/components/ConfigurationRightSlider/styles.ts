import styled from 'styled-components';
import { Col, Row } from 'antd';

export const SlideContainer = styled.div`
    right: 0;
    position: absolute;
    height: 100%
`;

export const Slide = styled.div<{visible: boolean}>`
    background-color: white;
    border-top: solid 1px ${({theme}) => theme.elements.bodyBackgroundColor};
    border-width: 2px 2px 2px 0;
    height: 100%;
    line-height: 48px;
    margin: 0 ${({ visible }) => visible ? '0' : '-500px'} 0 0;
    position: relative;
    text-align: start;
    width: 500px;
    -moz-transition: margin 0.3s;
    -o-transition: margin 0.3s;
    -webkit-transition: margin 0.3s;
    transition: margin 0.3s;
    overflow-y: auto;

`;

export const SlideContent = styled.div`
    padding: 20px;
    width: 100%;
`;

export const FieldsContainer = styled(Row)`
    margin-top: 40px;
`;

export const FieldCol = styled(Col)`
    line-height: 1.5;
    margin-bottom: 15px;
    vertical-align: middle;
`;
