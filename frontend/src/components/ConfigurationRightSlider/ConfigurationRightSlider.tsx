import React, { FC, useCallback, useEffect, useMemo, useState } from 'react';
import { SlideContainer, Slide, SlideContent, FieldCol, FieldsContainer } from './styles';
import { useDispatch, useSelector } from 'react-redux';
import * as selectors from '../../redux-store/selectors';
import { Wave } from '../../api/wave/types';
import Title from 'antd/es/typography/Title';
import { WaveTypeName } from '../../consts/enums';
import { Button, DatePicker, InputNumber, Row, Select, Slider, Switch } from 'antd';
import moment from 'moment';
import { putWave } from '../../api/wave';
import { loadDistribution } from '../../redux-store/waveReducer/thunks';

type ConfigurationRightSliderProps = {
    visible: boolean;
};

const { Option } = Select;

export const ConfigurationRightSlider: FC<ConfigurationRightSliderProps> = ({ visible }) => {
    const dispatch = useDispatch();

    const waveBunch = useSelector(selectors.waveBunch.waveBunch);
    const activeWave = useSelector(selectors.waveBunch.activeWave);

    const [waveDetails, setWaveDetails] = useState<Wave | null>(null);
    const [editDetails, setEditDetails] = useState<Wave>({} as Wave);

    useEffect(() => {
        const wave = waveBunch.waves.find((wave) => wave.id === activeWave);
        if (wave) {
            setWaveDetails(wave);
            setEditDetails({ ...wave });
        }
    }, [waveBunch.waves, activeWave]);

    const updateWave = useCallback(async () => {
        const result = await putWave(editDetails);
        if (result.success) {
            loadDistribution(dispatch, Number(waveBunch.id));
        }
    }, [editDetails, dispatch, waveBunch.id]);

    const waveFields = useMemo(
        () =>
            waveDetails
                ? [
                      {
                          field: 'Тип',
                          value: (
                              <Select value={waveDetails.type} disabled>
                                  <Option value={waveDetails.type}>
                                      {WaveTypeName[waveDetails.type].toLowerCase()}
                                  </Option>
                              </Select>
                          ),
                          show: true,
                      },
                      {
                          field: 'Дата начала',
                          value: (
                              <DatePicker
                                  showTime
                                  value={moment(editDetails.start_date)}
                                  allowClear={false}
                                  onChange={(m, dateString) =>
                                      setEditDetails({ ...editDetails, start_date: dateString })
                                  }
                              />
                          ),
                          show: true,
                      },
                      {
                          field: 'Дата конца',
                          value: (
                              <DatePicker
                                  showTime
                                  value={moment(editDetails.end_date)}
                                  allowClear={false}
                                  onChange={(m, dateString) =>
                                      setEditDetails({ ...editDetails, end_date: dateString })
                                  }
                              />
                          ),
                          show: true,
                      },
                      {
                          field: 'Дата автоматического распределения',
                          value: (
                              <DatePicker
                                  showTime
                                  value={moment(editDetails.auto_distribution_date)}
                                  allowClear={false}
                                  onChange={(m, dateString) =>
                                      setEditDetails({ ...editDetails, auto_distribution_date: dateString })
                                  }
                              />
                          ),
                          show: waveDetails.use_rating,
                      },
                      { field: 'Рейтинг', value: <Switch checked={waveDetails.use_rating} disabled />, show: true },
                      {
                          field: 'Количество курсов на выбор',
                          value: <InputNumber min={1} max={10} value={waveDetails.course_count} disabled />,
                          show: true,
                      },
                      {
                          field: 'Приоритет курсов',
                          value: <Switch checked={waveDetails.has_priority} disabled />,
                          show: waveDetails.use_rating,
                      },
                      {
                          field: 'Автоматический прием',
                          value: (
                              <Switch
                                  checked={editDetails.auto_accept}
                                  disabled
                                  onChange={(value) => setEditDetails({ ...editDetails, auto_accept: value })}
                              />
                          ),
                          show: true,
                      },

                      {
                          field: 'Интервал рейтинга',
                          value: (
                              <Slider
                                  range
                                  value={waveDetails.rating_range}
                                  disabled
                                  /*marks={{
                                      71: '71',
                                      86: '86',
                                      100: '100',
                                  }}*/
                              />
                          ),
                          show: waveDetails.use_rating,
                      },
                      {
                          field: 'Можно ли участвовать тем, кто уже прошел',
                          value: <Switch checked={waveDetails.can_participate} disabled />,
                          show: waveDetails.wave_number > 1,
                      },
                      {
                          field: 'Выбор из тех курсов, куда прошел',
                          value: <Switch checked={waveDetails.use_previous_requests} disabled />,
                          show: waveDetails.wave_number > 1,
                      },
                      {
                          field: 'Запретить переполнение',
                          value: <Switch checked={waveDetails.forbid_overflow} disabled />,
                          show: true,
                      },
                      {
                          field: 'Показывать количество заявок на курс',
                          value: (
                              <Switch
                                  checked={editDetails.show_requests_count}
                                  onChange={(value) => setEditDetails({ ...editDetails, show_requests_count: value })}
                              />
                          ),
                          show: true,
                      },
                      {
                          field: 'Показать результаты волны студенту',
                          value: (
                              <Switch
                                  checked={editDetails.show_result}
                                  onChange={(value) => setEditDetails({ ...editDetails, show_result: value })}
                              />
                          ),
                          show: true,
                      },
                  ]
                : [],
        [waveDetails, editDetails],
    );

    return (
        <SlideContainer>
            <Slide visible={visible}>
                <SlideContent>
                    <Title level={4}>Конфигурация волны</Title>
                    {waveDetails && (
                        <FieldsContainer>
                            {waveFields.map(
                                (waveField) =>
                                    waveField.show && (
                                        <React.Fragment key={waveField.field}>
                                            <FieldCol span={15}>{waveField.field}</FieldCol>
                                            <FieldCol span={9}>{waveField.value}</FieldCol>
                                        </React.Fragment>
                                    ),
                            )}
                        </FieldsContainer>
                    )}
                    <br />
                    <Row>
                        <FieldCol span={18} />
                        <FieldCol span={6}>
                            <Button type="primary" onClick={updateWave}>
                                Сохранить
                            </Button>
                        </FieldCol>
                    </Row>
                </SlideContent>
            </Slide>
        </SlideContainer>
    );
};
