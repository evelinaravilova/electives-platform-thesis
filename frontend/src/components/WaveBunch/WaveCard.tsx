import React, { FC, useCallback, useMemo } from 'react';
import { Button, List } from 'antd';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons/lib';
import { ListItem, ListItemCol, StyledWaveCard } from './styles';
import { Wave } from '../../api/wave/types';
import { getWaveEntries, isWaveActive, WaveFieldKeys, WaveFields } from './utils';
import { setActiveWave } from '../../redux-store/waveReducer/waveBunchSlice';
import { useDispatch, useSelector } from 'react-redux';
import { StatusCircle } from '../../shared/StatusCircle';
import { theme } from '../../styles';
import * as selectors from '../../redux-store/selectors';

type WaveCardProps = {
    wave: Wave;
    configVisible: boolean;
    setConfigVisible: (visible: boolean) => void;
};

export const WaveCard: FC<WaveCardProps> = ({ wave, configVisible, setConfigVisible }) => {
    const dispatch = useDispatch();

    const activeWave = useSelector(selectors.waveBunch.activeWave);

    const isActive = useMemo(() => isWaveActive(wave.start_date, wave.end_date), [wave.start_date, wave.end_date]);

    const contentRender = useMemo(
        () => (
            <List
                size="small"
                dataSource={getWaveEntries(wave)}
                renderItem={(item) => (
                    <ListItem key={item[0]}>
                        <ListItemCol>{WaveFields[item[0] as WaveFieldKeys]}</ListItemCol>{' '}
                        <ListItemCol>{item[1]!.toString().toLowerCase()}</ListItemCol>
                    </ListItem>
                )}
            />
        ),
        [wave],
    );

    const onMoreClick = useCallback(() => {
        if (wave.id === activeWave && configVisible) {
            setConfigVisible(false);
        } else setConfigVisible(true);
        dispatch(setActiveWave(wave.id));
    }, [dispatch, setConfigVisible, wave.id, activeWave, configVisible]);

    return (
        <StyledWaveCard
            actions={[<EditOutlined key="edit" />, <DeleteOutlined key="delete" />]}
            extra={
                <Button type="link" onClick={onMoreClick}>
                    Подробнее
                </Button>
            }
            title={
                <span>
                    {wave.wave_number} волна &nbsp;
                    {isActive && <StatusCircle status={theme.colors.green} />}
                </span>
            }
            size="small"
        >
            {contentRender}
        </StyledWaveCard>
    );
};
