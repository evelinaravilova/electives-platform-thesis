import { Wave } from '../../api/wave/types';
import moment from 'moment';
import { WaveType, WaveTypeName } from '../../consts/enums';

export enum WaveFields {
    date = 'Период',
    type = 'Тип',
    use_rating = 'Рейтинг',
    course_count = 'Количество курсов',
    has_priority = 'Приоритет',
}

export type WaveFieldKeys = 'type' | 'course_count' | 'use_rating' | 'has_priority' | 'date';

export const waveFields = ['type', 'course_count', 'use_rating', 'has_priority', 'date'];

export const mapBoolean = (value: boolean): string => (value ? 'Да' : 'Нет');

export const getWaveEntries = (wave: Wave): [string, string | number | boolean | number[] | undefined][] => {
    const entries = Object.entries(wave);
    const result: [string, string | number | boolean | number[] | undefined][] = [];
    for (const entry of entries) {
        switch (entry[0]) {
            case waveFields[0]:
                result.push([entry[0], WaveTypeName[entry[1] as WaveType]]);
                break;
            case waveFields[1]:
                result.push(entry);
                break;
            default:
                if (entry[0] === waveFields[2] || entry[0] === waveFields[3]) {
                    result.push([entry[0], mapBoolean(entry[1] as boolean)]);
                }
                break;
        }
    }
    result.push(['date', `${moment(wave.start_date).format('L')} - ${moment(wave.end_date).format('L')}`]);
    return result;
};

export const isWaveActive = (startDate: string, endDate: string): boolean => {
    const currentDate = moment(new Date());
    const date1 = moment(startDate);
    const date2 = moment(endDate);
    return currentDate.isBetween(date1, date2);
};
