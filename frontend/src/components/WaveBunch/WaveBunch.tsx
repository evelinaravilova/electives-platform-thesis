import React, { FC, useMemo, useState } from 'react';
import { Spin } from 'antd';
import { Wave } from '../../api/wave/types';
import { WaveCard } from './WaveCard';
import { useSelector } from 'react-redux';
import * as selectors from '../../redux-store/selectors';
import {
    AddWaveButton,
    ArrowIconDown,
    ArrowIconRight,
    DocBlock,
    FileIcon, NoContentText,
    WaveBlock,
    WaveBunchContainer,
} from './styles';
import { WaveModal } from '../WaveModal';

type WaveBunchProps = {
    waves: Wave[];
    configVisible: boolean;
    setConfigVisible: (visible: boolean) => void;
};

export const WaveBunch: FC<WaveBunchProps> = ({ waves, configVisible, setConfigVisible }) => {
    const loading = useSelector(selectors.waveBunch.loading);

    const [modalOpen, setModalOpen] = useState<boolean>(false);

    const wavesRender = useMemo(
        () =>
            [...waves]
                .sort((a, b) => a.wave_number - b.wave_number)
                .map((wave) => (
                    <WaveBlock key={wave.id}>
                        <WaveCard wave={wave} configVisible={configVisible} setConfigVisible={setConfigVisible} />
                        <ArrowIconRight />
                        <DocBlock href={wave.requests_spreadsheet} target='_blank'>
                            <FileIcon />
                            <div>Заявки</div>
                        </DocBlock>
                    </WaveBlock>
                ))
                .reduce((accu: JSX.Element[], elem: JSX.Element, index) => {
                    return !accu.length ? [elem] : [...accu, <ArrowIconDown key={index} />, elem];
                }, []),
        [waves, setConfigVisible, configVisible],
    );

    return (
        <WaveBunchContainer>
            <AddWaveButton onClick={() => setModalOpen(true)} />
            <Spin tip="Загрузка..." size="large" spinning={loading} style={{ width: '100%', position: 'absolute' }}>
                {waves.length ? wavesRender : <NoContentText>Нет волн</NoContentText>}
            </Spin>
            <WaveModal visible={modalOpen} setVisible={setModalOpen}/>
        </WaveBunchContainer>
    );
};
