import styled, { css } from 'styled-components';
import { Card, List } from 'antd';
import {
    ArrowRightOutlined,
    DownOutlined,
    FileExcelOutlined,
    PlusCircleOutlined,
} from '@ant-design/icons/lib';

export const WaveBunchContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    overflow-y: auto;
    position: absolute;
    height: 100%;
    width: 100%;
    
    &::-webkit-scrollbar {
        display: none;
    }
    -ms-overflow-style: none; 
    scrollbar-width: none;
`;

export const StyledWaveCard = styled(Card)`
    width: 330px;
    margin: 20px 20px 20px 0;
`;

export const ArrowIconStyle = css`
    font-size: 24px;
    color: #7d7d7d;
`;

export const ArrowIconDown = styled(DownOutlined)`
    ${ArrowIconStyle}
    margin-left: 153px;
`;

export const ArrowIconRight = styled(ArrowRightOutlined )`
    ${ArrowIconStyle}
`;

export const ListItem = styled(List.Item)`
    font-size: 0.9em;
    display: flex;
`;

export const ListItemCol = styled.div`
    width: 100%;
    flex: 1 1 auto;
`;

export const WaveBlock = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

export const DocBlock = styled.a`
    border: dashed 1px ${({ theme }) => theme.colors.blue};
    border-radius: 3px;
    display: inline-block;
    padding: 22px;
    width: 100px;
    height: 100px;
    text-align: center;
    margin: 20px;
    color: ${({ theme }) => theme.colors.blue};
    cursor: pointer;
`;

export const FileIcon = styled(FileExcelOutlined)`
    font-size: 30px;
`;

export const AddWaveButton = styled(PlusCircleOutlined)`
    font-size: 36px;
    color: #8e8d8d;
    cursor: pointer;
    margin: 20px 180px 10px 0;
    
    &:hover {
        color: #7d7d7d;
    }
`;

export const NoContentText = styled.div`
    margin: 10px 180px 0 0;
`;
