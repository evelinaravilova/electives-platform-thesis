import React, { FC, useEffect, useMemo } from 'react';
import { BlockList } from './styles';
import { CourseBlockItem } from './CourseBlockItem';
import { setActiveBlock } from '../../redux-store/courseReducer/courseBlockSlice';
import { useDispatch, useSelector } from 'react-redux';
import * as selectors from '../../redux-store/selectors';
import { loadCourseBlocks } from '../../redux-store/courseReducer/thunks';
import { Spin } from 'antd';

// type CourseBlockListProps = {};

export const CourseBlockList: FC = () => {
    const dispatch = useDispatch();

    const courseBlocks = useSelector(selectors.courseBlock.blocks);
    const activeBlock = useSelector(selectors.courseBlock.activeBlock);
    const courseBlocksLoading = useSelector(selectors.courseBlock.loading);

    useEffect(() => {
        loadCourseBlocks(dispatch);
    }, [dispatch]);

    const blockItems = useMemo(
        () =>
            courseBlocks.map((item) => (
                <CourseBlockItem
                    key={item.id}
                    type={item.type}
                    semester={item.semester}
                    requestsCount={item.requests_count}
                    handleClick={() => dispatch(setActiveBlock(item.id))}
                    selected={activeBlock === item.id}
                />
            )),
        [activeBlock, courseBlocks, dispatch],
    );

    return (
        <Spin tip="Загрузка..." size="large" spinning={courseBlocksLoading} style={{ width: '100%' }}>
            <BlockList>
                {/* <GradientOverlay>
            </GradientOverlay>*/}
                {blockItems}
            </BlockList>
        </Spin>
    );
};
