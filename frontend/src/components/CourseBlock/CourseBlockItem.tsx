import React, { FC, useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { BlockItemDisabled, BlockItemSelected, NameText, RequestsText, SemesterText } from './styles';
import blockShape from '../../assets/block-item.svg';
import { theme } from '../../styles';
import { StatusCircle } from '../../shared/StatusCircle';
import WaveContext from '../WaveProvider/WaveContext';

type CourseBlockItemProps = {
    type: string;
    semester: number;
    requestsCount: number;
    handleClick: () => void;
    selected: boolean;
};

export const CourseBlockItem: FC<CourseBlockItemProps> = ({
    type,
    semester,
    requestsCount,
    handleClick,
    selected,
}) => {

    const wave = useContext(WaveContext);

    const [requestCount, setRequestCount] = useState(requestsCount);

    useEffect(() => {
        setRequestCount(requestsCount);
    }, [requestsCount]);

    const status = useMemo(() => {
        const diff = wave!.course_count - requestCount;
        switch (diff) {
            case 0:
                return theme.colors.green;
            case wave!.course_count:
                return theme.colors.red;
            default:
                return theme.colors.yellow;
        }
    }, [wave, requestCount]);

    const content = useMemo(
        () => (
            <>
                <NameText>{type}<br/> блок</NameText>
                <SemesterText>{semester} семестр</SemesterText>
                <RequestsText>
                    <StatusCircle status={status}/> {requestCount}/{wave!.course_count}
                </RequestsText>
            </>
        ),
        [type, semester, requestCount, wave, status],
    );

    const onSelectedItemClick = useCallback(() => {
        handleClick();
    }, [handleClick]);

    return selected ? (
        <BlockItemSelected bg={blockShape}>{content}</BlockItemSelected>
    ) : (
        <BlockItemDisabled onClick={onSelectedItemClick}>{content}</BlockItemDisabled>
    );
};
