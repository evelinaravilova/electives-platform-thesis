import styled from 'styled-components';

export const BlockItem = styled.div`
    width: 140px;
    flex-grow: 0;
    flex-shrink: 0;
    padding: 7px;
    cursor: pointer;
    margin-bottom: 10px;
    margin-left: 10px;
`;

export const BlockItemDisabled = styled(BlockItem)`
    background-color: transparent;
    border: solid 1px #C4C4C4;
    border-radius: 10px;
    height: 85px;
`;

type BlockItemSelectedProps = {
    bg: string;
};

export const BlockItemSelected = styled(BlockItem)`
    background-image: url(${(props: BlockItemSelectedProps) => props.bg});
    background-repeat: no-repeat;
    background-size: 100%;
    filter: drop-shadow(3px 11px 40px rgba(34, 60, 80, 0.16));
    height: 108px;
`;

export const BlockList = styled.div`
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    align-content: space-between;
    width: 100%;
/*    overflow-x: auto;
    position: relative;
    
    &::-webkit-scrollbar {
        display: none;
    }*/
`;

export const NameText = styled.div`
    word-wrap: break-word;
    font-weight: 500;
    line-height: 18px;
`;

export const SemesterText = styled.div`
    font-size: 12px;
`;

export const RequestsText = styled.div`
    font-size: 12px;
`;

export const GradientOverlay = styled.div`
    position: absolute;
    left: 0;
    top: 0;
    pointer-events: none;
    width: 100%;
    height: 100%;
    background: radial-gradient(circle, rgba(2,0,36,0) 77%, rgba(240,242,245,1) 100%);
`;
