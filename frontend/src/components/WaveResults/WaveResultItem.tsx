import React, { FC, useMemo } from 'react';
import { WaveRequests } from '../../api/wave/types';
import moment from 'moment';
import { CourseBlockResult, WaveResultCard } from './styles';
import Title from 'antd/es/typography/Title';
import { Col, List } from 'antd';
import { RequestStatusName } from '../../consts/enums';
import { StatusCircle } from '../../shared/StatusCircle';
import { getStatusColor } from '../CourseList/utils';
import { StatusText } from '../CourseList/styles';

type WaveResultItemProps = {
    waveResult: WaveRequests;
};

export const WaveResultItem: FC<WaveResultItemProps> = ({ waveResult }) => {

    const title = useMemo(
        () => `Волна ${moment(waveResult.start_date).format('LL')} - ${moment(waveResult.end_date).format('LL')}`,
        [waveResult.start_date, waveResult.end_date],
    );

    const courseBlocks = useMemo(
        () =>
            waveResult.course_blocks.map((block) => (
                <CourseBlockResult key={block.id}>
                    <Title level={5}>{block.type} блок</Title>
                    <List
                        itemLayout="horizontal"
                        dataSource={block.requests}
                        renderItem={(request) => (
                            <List.Item>
                                <Col span={18}>{request.course}</Col>
                                <Col span={6}>
                                    <StatusText>
                                        <StatusCircle status={getStatusColor(request.status)} />{' '}
                                        {RequestStatusName[request.status]}
                                    </StatusText>
                                </Col>
                            </List.Item>
                        )}
                    />
                </CourseBlockResult>
            )),
        [waveResult.course_blocks],
    );

    return (
        <WaveResultCard title={title} bordered={false}>
            {courseBlocks}
        </WaveResultCard>
    );
};
