import styled from 'styled-components';
import { Card } from 'antd';

export const WaveResultsCardList = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    max-width: 450px;
    @media only screen and (min-device-width: 1000px) {
        max-width: 800px;
    }
    min-width: 420px;
    height: 88vh;
    margin: 20px;
`;

export const WaveResultCard = styled(Card)`
    margin: 20px;
`;

export const CourseBlockResult = styled.div`
    margin-bottom: 25px;
`;
