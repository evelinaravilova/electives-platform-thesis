import React, { FC, useMemo } from 'react';
import { WaveRequests } from '../../api/wave/types';
import { WaveResultItem } from './WaveResultItem';
import { WaveResultsCardList } from './styles';

type WaveResultListProps = {
    wavesResults: WaveRequests[];
};

export const WaveResultList: FC<WaveResultListProps> = ({ wavesResults }) => {

    const waveResultItems = useMemo(
        () =>
            wavesResults && wavesResults.map((item) => (
                <WaveResultItem
                    key={item.id}
                    waveResult={item}
                />
            )),
        [wavesResults],
    );

    return (
        <WaveResultsCardList>
            {waveResultItems}
        </WaveResultsCardList>
    );
};
