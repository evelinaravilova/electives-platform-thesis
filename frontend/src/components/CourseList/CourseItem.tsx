import React, { FC, useMemo, useRef } from 'react';
import {
    CourseItemSelected,
    CourseItemDisabled,
    MicroCover,
    CourseMicroInfo,
    TeacherText,
    NameText,
    StatusText,
    CourseItemContainer,
    DragIcon,
    MicroCoverContainer,
    MicroCoverPriority,
} from './styles';
import { StatusCircle } from '../../shared/StatusCircle';
import { getStatusColor } from './utils';
import courseShape from '../../assets/course-item.svg';
import cover from '../../assets/kpfu_logo_background_light2.jpg';
import { DropTargetMonitor, useDrop, useDrag } from 'react-dnd';
import { XYCoord } from 'dnd-core';
import { TeacherProfile } from '../../api/institute/types';
import { RequestStatusName } from '../../consts/enums';
import { StudentRequest } from '../../api/request/types';

type CourseItemProps = {
    id: number;
    name: string;
    teacher: TeacherProfile;
    request: StudentRequest | null;
    logo: string;
    handleClick: () => void;
    selected: boolean;
    index?: number;
    moveCard?: (dragIndex: number, hoverIndex: number) => void;
};

interface DragItem {
    index: number;
    id: string;
    type: string;
}

export const CourseItem: FC<CourseItemProps> = ({
    id,
    name,
    teacher,
    request,
    logo,
    handleClick,
    selected,
    index,
    moveCard,
}) => {
    const ref = useRef<HTMLDivElement>(null);

    const [{ handlerId }, drop] = useDrop({
        accept: 'card',
        collect(monitor) {
            return {
                handlerId: monitor.getHandlerId(),
            };
        },
        hover(item: DragItem, monitor: DropTargetMonitor) {
            console.log(request?.priority, item);
            if (!ref.current) {
                return;
            }
            const dragIndex = item.index;
            const hoverIndex = index!;

            // Don't replace items with themselves
            if (dragIndex === hoverIndex) {
                return;
            }

            // Determine rectangle on screen
            const hoverBoundingRect = ref.current?.getBoundingClientRect();

            // Get vertical middle
            const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

            // Determine mouse position
            const clientOffset = monitor.getClientOffset();

            // Get pixels to the top
            const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top;

            // Only perform the move when the mouse has crossed half of the items height
            // When dragging downwards, only move when the cursor is below 50%
            // When dragging upwards, only move when the cursor is above 50%

            // Dragging downwards
            if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
                return;
            }

            // Dragging upwards
            if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
                return;
            }

            // Time to actually perform the action
            if (moveCard) moveCard(dragIndex, hoverIndex);

            // Note: we're mutating the monitor item here!
            // Generally it's better to avoid mutations,
            // but it's good here for the sake of performance
            // to avoid expensive index searches.
            item.index = hoverIndex;
        },
    });

    const [{ isDragging }, drag] = useDrag({
        type: 'card',
        item: () => {
            return { id, index };
        },
        collect: (monitor: any) => ({
            isDragging: monitor.isDragging(),
        }),
    });

    const opacity = isDragging ? 0 : 1;

    drag(drop(ref));

    const priorityProps = request?.priority ? { ref: ref, style: { opacity } } : {};

    const courseCover = logo ? logo : cover;

    const content = useMemo(
        () => (
            <>
                {request?.priority ? (
                    <MicroCoverContainer>
                        <MicroCover src={courseCover} />
                        <MicroCoverPriority>{request.priority}</MicroCoverPriority>
                    </MicroCoverContainer>
                ) : (
                    <MicroCover src={courseCover} />
                )}
                <CourseMicroInfo>
                    <NameText>{name}</NameText>
                    {teacher && <TeacherText>{teacher.user.last_name} {teacher.user.first_name} {teacher.user.middle_name}</TeacherText>}
                    {request?.status && (
                        <StatusText>
                            <StatusCircle status={getStatusColor(request.status)} /> {RequestStatusName[request.status]}
                        </StatusText>
                    )}
                </CourseMicroInfo>
            </>
        ),
        [name, teacher, request, courseCover],
    );

    const renderCourseCard = useMemo(
        () =>
            selected ? (
                <CourseItemSelected bg={courseShape} {...priorityProps}>
                    {content}
                </CourseItemSelected>
            ) : (
                <CourseItemDisabled onClick={handleClick} {...priorityProps}>
                    {content}
                </CourseItemDisabled>
            ),
        [selected, content, handleClick, priorityProps],
    );

    return (
        <CourseItemContainer>
            {request?.priority && <DragIcon />}
            {renderCourseCard}
        </CourseItemContainer>
    );
};
