import React, { FC, useCallback, useContext, useEffect, useMemo } from 'react';
import { PriorityList, StyledCourseList } from './styles';
import { CourseItem } from './CourseItem';
/*import Title from 'antd/es/typography/Title';
import update from 'immutability-helper';
import WaveContext from '../WaveProvider/WaveContext';
import { fetchCourses } from '../../api/courses';*/
import { useDispatch, useSelector } from 'react-redux';
import * as selectors from '../../redux-store/selectors';
import { setCourses, setActiveCourse } from '../../redux-store/courseReducer/courseSlice';
import { Course } from '../../api/courses/types';
import { loadCourses } from '../../redux-store/courseReducer/thunks';
import { Spin } from 'antd';
import Title from 'antd/es/typography/Title';
import WaveContext from '../WaveProvider/WaveContext';

// type CourseBlockListProps = {};

export const CourseList: FC = () => {
    const dispatch = useDispatch();
    const wave = useContext(WaveContext);
    const activeBlock = useSelector(selectors.courseBlock.activeBlock);
    const courses = useSelector(selectors.course.courses);
    const activeCourse = useSelector(selectors.course.activeCourse);

    const coursesLoading = useSelector(selectors.course.loading);

    //const [cards, setCards] = useState(courses.filter(item => !item.priority));
    // const [priorityCards, setPriorityCards] = useState(courses.slice(0, 3));
    // console.log(priorityCards)

    useEffect(() => {
        activeBlock && loadCourses(dispatch, activeBlock);
    }, [activeBlock, dispatch]);

    /*const moveCard = useCallback(
        (dragIndex: number, hoverIndex: number) => {
            const dragCard = priorityCards[dragIndex];
            /!*setPriorityCards(
                update(priorityCards, {
                    $splice: [
                        [dragIndex, 1],
                        [hoverIndex, 0, dragCard],
                    ],
                }),
            )*!/
        },
        [priorityCards],
    );*/

    const courseItemRender = useCallback(
        (item: Course, index?: number) => (
            <CourseItem
                key={item.id}
                id={item.id}
                name={item.name}
                teacher={item.head[0]}
                logo={item.logo}
                request={item.request}
                handleClick={() => dispatch(setActiveCourse(item.id))}
                selected={activeCourse === item.id}
                index={index}
                // moveCard={index ? moveCard : undefined}
            />
        ),
        [activeCourse, dispatch],
    );

    const courseItems = useMemo(() => courses.map((item) => courseItemRender(item)), [courses, courseItemRender]);
    const coursePriorityItems = useMemo(() => courses.slice(0, 3).map((item, i) => courseItemRender(item, i)), [courses, courseItemRender]);

    return (
        <StyledCourseList>
            {wave!.has_priority && (
                <PriorityList>
                    <Title level={5}>Приоритет</Title>
                    {coursePriorityItems}
                </PriorityList>
            )}
            <Spin tip="Загрузка..." size="large" spinning={coursesLoading}>
                {courses.length > 0 ? courseItems : "Нет курсов"}
            </Spin>
        </StyledCourseList>
    );
};
