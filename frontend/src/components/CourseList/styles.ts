import styled from 'styled-components';
import { DragOutlined } from '@ant-design/icons/lib';

export const StyledCourseList = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    width: 100%;
    min-width: 300px;
    overflow-y: auto;
`;

export const PriorityList = styled(StyledCourseList)`
    border: dashed 1px ${({theme}) => theme.colors.blue};
    border-radius: 10px;
    padding: 15px 15px 0 15px;
    margin-bottom: 15px;
`;

export const DragIcon = styled(DragOutlined)`
    margin: auto 10px auto 0;
`;

export const CourseItemContainer = styled.div`
    display: flex;
`;

export const CourseItem = styled.div`
    flex-grow: 0;
    flex-shrink: 0;
    padding: 10px;
    cursor: pointer;
    z-index: 10;
    margin-bottom: 15px;
    display: flex;
`;

export const CourseItemDisabled = styled(CourseItem)`
    background-color: transparent;
    border: solid 1px #C4C4C4;
    border-radius: 10px;
    width: 95%;
    height: 100px;
`;

type BlockItemSelectedProps = {
    bg: string;
};

export const CourseItemSelected = styled(CourseItem)`
    background-image: url(${(props: BlockItemSelectedProps) => props.bg});
    background-repeat: no-repeat;
    background-size: 100%;
    filter: drop-shadow(4px 4px 8px rgba(34, 60, 80, 0.1));
    width: 98%;
    height: 100px;
`;

export const MicroCoverContainer = styled.div`
    position: relative;
    width: 70px;
`;

export const MicroCoverPriority = styled.div`
    position: absolute;
    top: 5px;
    left: 5px;
    font-size: 1.5em;
    font-weight: bold;
`;

export const MicroCover = styled.img`
    width: 120px;
    object-fit: cover; 
    border-radius: 10px;
`;

export const CourseMicroInfo = styled.div`
    padding: 5px 5px 5px 15px;
`;

export const NameText = styled.div`
    font-weight: 500;
    line-height: 20px;
    font-size: 1.2em;
`;

export const TeacherText = styled.div`
   color: ${({theme}) => theme.elements.textSecondary};
   font-weight: 500;
   font-size: 0.9em;
   margin: 3px 0;
`;

export const StatusText = styled.span`
    font-size: 12px;
`;
