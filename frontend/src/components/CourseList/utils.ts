import { theme } from '../../styles';
import { RequestStatus } from '../../consts/enums';

export const getStatusColor = (status: RequestStatus): string => {
    switch (status) {
        case RequestStatus.Accepted:
            return theme.colors.green;
        case RequestStatus.Rejected:
            return theme.colors.red;
        default:
            return theme.colors.yellow;
    }
};
