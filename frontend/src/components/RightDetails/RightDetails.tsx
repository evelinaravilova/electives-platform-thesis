import React, { FC } from 'react';
import { StyledBlock } from './styles';

export const RightDetails: FC = ({ children }) => {

    return (
        <StyledBlock>
            {children}
        </StyledBlock>
    );
};
