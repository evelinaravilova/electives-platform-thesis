import styled from 'styled-components';

export const StyledBlock = styled.div`
    border-radius: 10px 0 0 10px;
    background-color: white;
    width: 100%;
    min-width: 570px;
    height: 100%;
    max-height: 100vh;
    padding: 5%;
    position: relative;
`;
