import React, { FC, useMemo } from 'react';

import { chartBorderColors, chartColors } from '../../consts/colors';
import { Line } from 'react-chartjs-2';

export type RequestsIntensityChartProps = {
    data: {days: number[];
        values: number[];};
};

export const RequestsIntensityChart: FC<RequestsIntensityChartProps> = ({ data }) => {
    const mappedData = useMemo(() => {
        return {
            labels: data.days,
            datasets: [
                {
                    label: "Интенсивность подачи заявок в течение волны",
                    data: data.values,
                    backgroundColor: chartColors.blue,
                    borderColor: chartBorderColors.blue,
                    borderWidth: 2,
                },
            ],
        };
    }, [data]);

    return (
        <>
            <Line
                data={mappedData}
                type="line"
                height={50}
                options={{
                    responsive: true,
                    scales: {
                        yAxes: [
                            {
                                ticks: {
                                    autoSkip: true,
                                    maxTicksLimit: 5,
                                    beginAtZero: true
                                },
                            }
                        ],
                    }
                }}
            />
        </>
    );
};
