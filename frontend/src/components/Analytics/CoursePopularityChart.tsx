import React, { FC, useMemo } from 'react';

import { Pie } from 'react-chartjs-2';
import { chartBorderColors, chartColors } from '../../consts/colors';

export type CoursePopularityChartProps = {
    data: { courses: string[]; values: number[] };
};

export const CoursePopularityChart: FC<CoursePopularityChartProps> = ({ data }) => {
    const mappedData = useMemo(() => {
        const allBackgroundColors = Object.values(chartColors);
        const allBorderColors = Object.values(chartBorderColors);
        const backgroundColors = [];
        const borderColors = [];
        let j = 0;

        for (let i = 0; i < data.values.length; i++) {
            if (j < allBackgroundColors.length) {
                backgroundColors.push(allBackgroundColors[j]);
                borderColors.push(allBorderColors[j]);
                j++;
            } else j = 0;
        }

        return {
            labels: data.courses,
            datasets: [
                {
                    data: data.values,
                    backgroundColor: backgroundColors,
                    borderColor: borderColors,
                    borderWidth: 1,
                },
            ],
        };
    }, [data]);

    return (
        <>
            <Pie
                data={mappedData}
                type="pie"
                options={{
                    responsive: true,
                    title: { text: 'Популярность курсов', display: true },
                }}
            />
        </>
    );
};
