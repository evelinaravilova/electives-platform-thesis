import React, { FC, useMemo } from 'react';

import { chartBorderColors, chartColors } from '../../consts/colors';
import { Bar } from 'react-chartjs-2';

export type RatingChartProps = {
    data: { courses: string[]; values: number[] };
};

export const RatingChart: FC<RatingChartProps> = ({ data }) => {
    const mappedData = useMemo(() => {
        const allBackgroundColors = Object.values(chartColors);
        const allBorderColors = Object.values(chartBorderColors);
        const backgroundColors = [];
        const borderColors = [];
        let j = 0;

        for (let i = 0; i < data.values.length; i++) {
            if (j < allBackgroundColors.length) {
                backgroundColors.push(allBackgroundColors[j]);
                borderColors.push(allBorderColors[j]);
                j++;
            } else j = 0;
        }

        return {
            labels: data.courses,
            datasets: [
                {
                    label: 'Проходной балл',
                    data: data.values,
                    backgroundColor: backgroundColors,
                    borderColor: borderColors,
                    borderWidth: 1,
                },
            ],
        };
    }, [data]);

    return (
        <>
            <Bar
                data={mappedData}
                type="pie"
                options={{
                    responsive: true,
                    scales: {
                        yAxes: [{
                            display: true,
                            ticks: {
                                max: 100,
                                beginAtZero: true
                            }
                        }]
                    }
                }}
            />
        </>
    );
};
