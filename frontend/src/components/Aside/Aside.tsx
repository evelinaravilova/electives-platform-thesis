import React, { FC, useCallback, useMemo } from 'react';
import { Avatar, Layout } from 'antd';
import { UserOutlined } from '@ant-design/icons/lib';
import Title from 'antd/es/typography/Title';
import { useDispatch, useSelector } from 'react-redux';
import * as selectors from '../../redux-store/selectors';
import { LogoutButton } from './styles';
import { redirectToLogin } from '../../utils/requester';
import { setCurrentUser } from '../../redux-store/userReducer/slice';
import { emptyUser } from '../../consts/emptyUser';
import { UserRole } from '../../consts/enums';
import { StudentItems } from './menu-items/StudentItems';
import { DeaneryItems } from './menu-items/DeaneryItems';
import { TeacherItems } from './menu-items/TeacherItems';


export const Aside: FC = () => {
    const dispatch = useDispatch();

    const collapsed = window.innerWidth < 1200;

    const user = useSelector(selectors.user.user);

    const logout = useCallback(() => {
        dispatch(setCurrentUser({ token: '', user: emptyUser }));
        redirectToLogin('');
    }, [dispatch]);

    const menuItems = useMemo(() => {
        switch (user.role) {
            case UserRole.STUDENT:
                return <StudentItems />;
            case UserRole.TEACHER:
                return <TeacherItems />;
            case UserRole.DEANERY:
                return <DeaneryItems />;
            default:
                return;
        }
    }, [user.role]);

    return (
        <Layout.Sider
            theme="light"
            breakpoint="lg"
            style={{ zIndex: 10, position: 'relative', boxShadow: '0 0 5px rgba(0,0,0,0.1)' }}
            collapsed={collapsed}
        >
            {!collapsed ? (
                <Title level={3} style={{ margin: 15 }}>
                    Элективы
                </Title>
            ) : (
                <Title level={5} style={{ margin: 15 }}>
                    Элект.
                </Title>
            )}
            <Avatar icon={<UserOutlined />} style={{ margin: '10px 5px 15px 20px' }} src={user.photo} />{' '}
            {!collapsed && `${user.first_name} ${user.last_name}`}
            {menuItems}
            <LogoutButton onClick={logout} />
        </Layout.Sider>
    );
};
