import styled from 'styled-components';
import React, { FC } from 'react';
import { LogoutOutlined } from '@ant-design/icons/lib';

export const LogoutButtonWrapper = styled.div`
    cursor: pointer;
    position: absolute;
    bottom: 15px;
    left: 15px;
`;

export const LogoutIcon = styled(LogoutOutlined)`
    color: ${({theme}) => theme.colors.darkGrey};
    svg {
        width: 20px;
        height: 20px;
    }
`;

export const LogoutButton: FC<{ onClick: () => void }> = ({ onClick }) => (
    <LogoutButtonWrapper onClick={onClick}>
       <LogoutIcon />
    </LogoutButtonWrapper>
);
