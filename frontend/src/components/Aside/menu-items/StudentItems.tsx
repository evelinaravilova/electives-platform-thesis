import React, { FC, useState } from 'react';
import { Menu } from 'antd';
import {
    OrderedListOutlined,
    TrophyOutlined,
} from '@ant-design/icons/lib';
import { useHistory } from 'react-router';
import { Routes } from '../../../consts';
import { MenuInfo } from 'rc-menu/lib/interface';

enum MenuItems {
    ELECTIVES = 'courses',
    RESULTS = 'results',
}

export const StudentItems: FC = () => {
    const history = useHistory();
    const [activeOption, setActiveOption] = useState(history.location.pathname.split('/')[1]);

    const handleClick = (info: MenuInfo) => {
        let pathName = '';
        if (info.key === MenuItems.ELECTIVES) {
            pathName = Routes.COURSES;
            setActiveOption(MenuItems.ELECTIVES);
        }
        if (info.key === MenuItems.RESULTS) {
            pathName = Routes.STUDENT_RESULTS;
            setActiveOption(MenuItems.RESULTS);
        }
        history.push(pathName);
    };

    return (
        <Menu theme="light" mode="inline" selectedKeys={[activeOption]} onClick={handleClick}>
            <Menu.Item key={MenuItems.ELECTIVES} icon={<OrderedListOutlined/>}>
                Элективы
            </Menu.Item>
            <Menu.Item key={MenuItems.RESULTS} icon={<TrophyOutlined />}>
                Результаты
            </Menu.Item>
        </Menu>
    );
};
