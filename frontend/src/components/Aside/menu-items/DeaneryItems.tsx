import React, { FC, useState } from 'react';
import { Menu } from 'antd';
import { BarChartOutlined, OrderedListOutlined, UsergroupAddOutlined } from '@ant-design/icons/lib';
import { useHistory } from 'react-router';
import { Routes } from '../../../consts';
import { MenuInfo } from 'rc-menu/lib/interface';

enum MenuItems {
    DISTRIBUTIONS = 'distributions',
    ANALYTICS = 'analytics',
    STUDENT_LIST = 'student_list',
}

export const DeaneryItems: FC = () => {
    const history = useHistory();
    const [activeOption, setActiveOption] = useState(history.location.pathname.split('/')[1]);

    const handleClick = (info: MenuInfo) => {
        let pathName = '';
        if (info.key === MenuItems.DISTRIBUTIONS) {
            pathName = Routes.DISTRIBUTIONS;
            setActiveOption(MenuItems.DISTRIBUTIONS);
        }
        if (info.key === MenuItems.ANALYTICS) {
            pathName = Routes.ANALYTICS;
            setActiveOption(MenuItems.ANALYTICS);
        }
        if (info.key === MenuItems.STUDENT_LIST) {
            pathName = '';
            setActiveOption(MenuItems.STUDENT_LIST);
        }
        history.push(pathName);
    };

    return (
        <Menu theme="light" mode="inline" selectedKeys={[activeOption]} onClick={handleClick}>
            <Menu.Item key={MenuItems.DISTRIBUTIONS} icon={<OrderedListOutlined />}>
                Распределения
            </Menu.Item>
            <Menu.Item key={MenuItems.ANALYTICS} icon={<BarChartOutlined />}>
                Аналитика
            </Menu.Item>
            <Menu.Item key={MenuItems.STUDENT_LIST} icon={<UsergroupAddOutlined />}>
                Студенты (рейтинг)
            </Menu.Item>
        </Menu>
    );
};
