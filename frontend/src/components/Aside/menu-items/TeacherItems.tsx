import React, { FC, useState } from 'react';
import { Menu } from 'antd';
import { OrderedListOutlined, VideoCameraOutlined } from '@ant-design/icons/lib';
import { Routes } from '../../../consts';
import { useHistory } from 'react-router';
import { MenuInfo } from 'rc-menu/lib/interface';

enum MenuItems {
    WAVES = 'wave_courses',
    MY_COURSES = 'my_courses',
}

export const TeacherItems: FC = () => {
    const history = useHistory();
    const [activeOption, setActiveOption] = useState(MenuItems.WAVES);

    const handleClick = (info: MenuInfo) => {
        let pathName = '';
        if (info.key === MenuItems.WAVES) {
            pathName = Routes.WAVE_COURSES;
            setActiveOption(MenuItems.WAVES);
        }
        if (info.key === MenuItems.MY_COURSES) {
            pathName = '';
            setActiveOption(MenuItems.MY_COURSES);
        }
        history.push(pathName);
    };

    return (
        <Menu theme="light" mode="inline" selectedKeys={[activeOption]} onClick={handleClick}>
            <Menu.Item key={MenuItems.WAVES} icon={<OrderedListOutlined/>}>
                Волны
            </Menu.Item>
            <Menu.Item key={MenuItems.MY_COURSES} icon={<VideoCameraOutlined/>} disabled>
                Мои курсы
            </Menu.Item>
        </Menu>
    );
};
