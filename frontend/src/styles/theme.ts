const colors = {
    green: '#00C48C',
    red: '#ff7875',
    yellow: '#ffc53d',
    blue: '#4EAEFF',
    darkBlue: '#109CF1',
    darkGrey: '#151522',
    pink: '#FF647C',
    grey: '#595959',

};

const elements = {
    bodyBackgroundColor: '#F4F5F7',
    textColor: colors.darkGrey,
    fontSizeBase: '12px',
    textSecondary: '#999999'
};

export type StyleTheme = {
    colors: { [key in keyof typeof colors]: string };
    elements: { [key in keyof typeof elements]: string };
};

const theme: StyleTheme = {
    colors,
    elements,
};

export type StyleColors = { [key in keyof typeof colors]: string };

export { theme };
