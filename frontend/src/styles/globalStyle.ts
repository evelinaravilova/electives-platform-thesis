import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
    &::-webkit-scrollbar {
        background-color: #fff;
        width: 12px;
    }
    
    &::-webkit-scrollbar-track {
        background-color: #fff;
    }
    
    &::-webkit-scrollbar-thumb {
        background-color: #ccc;
        border-radius: 16px;
        border: 4px solid #fff;
    }
    
    &::-webkit-scrollbar-button {
        display:none;
    }
`;